# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'faker'

# user = User.new
# user.email = 'admin@mail.com'
# user.username = 'admin'
# user.opera_module = 'ADMIN'
# user.password = 'admin123'
# user.save!


suffering_list = [
    'COLUMNA',
    'HOMBRO',
    'RODILLA',
    'CADERA',
    'NEUROLOGICO',
    'TOBILLO',
    'PEQUEÑAS ARTICULACIONES'
]

suffering_list.each do |description| 
    Suffering.create(description: description)
end

Procedure.create(
    name: 'CAMBIO DE COLUMNA',
    procedure_type: 'COLUMNA'
)
Procedure.create(
    name: 'CAMBIO DE HOMBRO',
    procedure_type: 'HOMBRO'
)
Procedure.create(
    name: 'CAMBIO DE RODILLA',
    procedure_type: 'RODILLA'
)
Procedure.create(
    name: 'CAMBIO DE CADERA',
    procedure_type: 'CADERA'
)
Procedure.create(
    name: 'CAMBIO DE NEURONAS',
    procedure_type: 'NEUROLOGICO'
)
Procedure.create(
    name: 'CAMBIO DE TOBILLO',
    procedure_type: 'TOBILLO'
)
Procedure.create(
    name: 'CAMBIO DE PEQUEÑAS ARTICULACIONES',
    procedure_type: 'PEQUEÑAS ARTICULACIONES'
)


10.times do |i|
    Doctor.create(
        name: Faker::Name.name 
    )
end
    
10.times do |i|
    hospital = Hospital.create(
        name: Faker::Company.name,
        description: Faker::Lorem.sentence,
        address: Faker::Address.street_address,
        city: Faker::Address.city,
        contact: Faker::Name.name,
        phone: Faker::Number.number(digits: 10),
        coinsurance: Faker::Boolean.boolean
    )
   
    Treatment.create(
        hospital_id: hospital.id,
        suffering_id: Faker::Number.between(from: 1, to: 7)
    )
     ProcedureOffered.create(
        procedure_id: Faker::Number.between(from: 1, to: 7),
        hospital_id: hospital.id,
        cost: Faker::Number.between(from: 1500, to: 10000),
        material_cost: Faker::Number.between(from: 1500, to: 10000),
        hospital_cost: Faker::Number.between(from: 1500, to: 10000)
    )
end


# user_provider = User.new
# user_provider.email = 'provider@mail.com'
# user_provider.username = 'provider'
# user_provider.opera_module = 'PROVEEDOR'
# user_provider.password = 'provider'
# user_provider.save!


# provider = Provider.new
# provider.name = 'Gasas y Bendas'
# provider.status = 'ok'
# provider.coinsurance = true
# provider.save!
