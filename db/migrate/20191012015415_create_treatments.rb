# frozen_string_literal: true

class CreateTreatments < ActiveRecord::Migration[6.0]
  def change
    create_table :treatments do |t|
      t.references :hospital, null: false, foreign_key: true
      t.references :suffering, null: false, foreign_key: true

      t.timestamps
    end
  end
end
