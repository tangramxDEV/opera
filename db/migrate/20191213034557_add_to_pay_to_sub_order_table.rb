# frozen_string_literal: true

class AddToPayToSubOrderTable < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_orders, :to_pay, :integer, default: 0
  end
end
