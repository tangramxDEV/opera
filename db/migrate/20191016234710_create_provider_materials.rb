# frozen_string_literal: true

class CreateProviderMaterials < ActiveRecord::Migration[6.0]
  def change
    create_table :provider_materials do |t|
      t.references :provider, null: false, foreign_key: true
      t.references :general_material, null: false, foreign_key: true
      t.string :price
      t.string :brand

      t.timestamps
    end
  end
end
