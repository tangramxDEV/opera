# frozen_string_literal: true

class RemoveUserReferencesFromProviders < ActiveRecord::Migration[6.0]
  def change
    remove_reference :providers, :user, index: true, foreign_key: true
  end
end
