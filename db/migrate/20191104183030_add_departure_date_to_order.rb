# frozen_string_literal: true

class AddDepartureDateToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :departure_date, :datetime
  end
end
