# frozen_string_literal: true

class AddOrderReferencesToSubOrder < ActiveRecord::Migration[6.0]
  def change
    add_reference :sub_orders, :order, null: false, foreign_key: true
  end
end
