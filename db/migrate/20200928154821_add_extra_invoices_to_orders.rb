class AddExtraInvoicesToOrders < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :evo_invoice, :string
    add_column :orders, :wf_invoice, :string
  end
end
