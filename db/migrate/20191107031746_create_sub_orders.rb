# frozen_string_literal: true

class CreateSubOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :sub_orders do |t|
      t.string :invoice
      t.references :provider, null: false, foreign_key: true

      t.timestamps
    end
  end
end
