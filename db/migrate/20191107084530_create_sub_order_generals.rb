# frozen_string_literal: true

class CreateSubOrderGenerals < ActiveRecord::Migration[6.0]
  def change
    create_table :sub_order_generals do |t|
      t.references :sub_order_generic, null: false, foreign_key: true
      t.references :provider_material, null: false, foreign_key: true
      t.string :quantity
      t.string :cost

      t.timestamps
    end
  end
end
