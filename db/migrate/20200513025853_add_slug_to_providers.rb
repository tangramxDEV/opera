# frozen_string_literal: true

class AddSlugToProviders < ActiveRecord::Migration[6.0]
  def change
    add_column :providers, :slug, :string, null: false
    add_index :providers, :slug, unique: true
  end
end
