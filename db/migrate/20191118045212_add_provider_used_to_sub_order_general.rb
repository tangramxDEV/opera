# frozen_string_literal: true

class AddProviderUsedToSubOrderGeneral < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_order_generals, :provider_used, :string
  end
end
