# frozen_string_literal: true

class AddEpisodeAndHospitalExpenseToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :episode, :string
    add_column :orders, :hospital_expense, :string
  end
end
