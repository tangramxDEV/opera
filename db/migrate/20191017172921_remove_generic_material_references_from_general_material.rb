# frozen_string_literal: true

class RemoveGenericMaterialReferencesFromGeneralMaterial < ActiveRecord::Migration[6.0]
  def change
    remove_reference :general_materials, :generic_material, null: false, foreign_key: true
  end
end
