# frozen_string_literal: true

class AddNewDateToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :surgical_event_date, :date
  end
end
