# frozen_string_literal: true

class AddNegotiatedAmountToSubOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_orders, :negotiated_amount, :integer
  end
end
