class AddSkipOverloadToSubOrders < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_orders, :skip_overload, :boolean, default: false
  end
end
