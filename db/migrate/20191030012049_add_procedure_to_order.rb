# frozen_string_literal: true

class AddProcedureToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :procedure, :string
  end
end
