# frozen_string_literal: true

class CreateCpts < ActiveRecord::Migration[6.0]
  def change
    create_table :cpts do |t|
      t.string :description

      t.timestamps
    end
  end
end
