# frozen_string_literal: true

class AddAffiliateCodeToProvider < ActiveRecord::Migration[6.0]
  def change
    add_column :providers, :affiliate_code, :string
  end
end
