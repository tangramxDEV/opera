# frozen_string_literal: true

class AddHospitalCoinsuranceToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :hospital_coinsurance, :string
  end
end
