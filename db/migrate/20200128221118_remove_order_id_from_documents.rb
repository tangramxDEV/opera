# frozen_string_literal: true

class RemoveOrderIdFromDocuments < ActiveRecord::Migration[6.0]
  def change
    remove_reference :documents, :order, null: false, foreign_key: true
  end
end
