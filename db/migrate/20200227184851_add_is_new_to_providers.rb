# frozen_string_literal: true

class AddIsNewToProviders < ActiveRecord::Migration[6.0]
  def change
    add_column :providers, :is_new, :boolean, default: false
  end
end
