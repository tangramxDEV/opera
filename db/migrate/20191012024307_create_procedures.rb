# frozen_string_literal: true

class CreateProcedures < ActiveRecord::Migration[6.0]
  def change
    create_table :procedures do |t|
      t.string :name
      t.string :procedure_type

      t.timestamps
    end
  end
end
