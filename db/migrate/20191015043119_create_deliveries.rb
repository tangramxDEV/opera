# frozen_string_literal: true

class CreateDeliveries < ActiveRecord::Migration[6.0]
  def change
    create_table :deliveries do |t|
      t.references :order, null: false, foreign_key: true
      t.references :provider, null: false, foreign_key: true
      t.string :quantity
      t.references :generic_material, null: false, foreign_key: true

      t.timestamps
    end
  end
end
