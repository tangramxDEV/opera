# frozen_string_literal: true

class AddCostToProcedureOffered < ActiveRecord::Migration[6.0]
  def change
    add_column :procedure_offereds, :cost, :string
  end
end
