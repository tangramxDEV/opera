# frozen_string_literal: true

class AddUserReferenceToOrders < ActiveRecord::Migration[6.0]
  def change
    add_reference :orders, :user, null: false, foreign_key: true, default: 4
  end
end
