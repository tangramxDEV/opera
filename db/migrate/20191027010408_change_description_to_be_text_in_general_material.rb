# frozen_string_literal: true

class ChangeDescriptionToBeTextInGeneralMaterial < ActiveRecord::Migration[6.0]
  def change
    change_column :general_materials, :description, :text
  end
end
