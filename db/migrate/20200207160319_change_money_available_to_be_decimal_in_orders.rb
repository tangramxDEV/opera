# frozen_string_literal: true

class ChangeMoneyAvailableToBeDecimalInOrders < ActiveRecord::Migration[6.0]
  def up
    change_column :orders, :money_available, :decimal
  end

  def down
    change_column :orders, :money_available, :integer
  end
end
