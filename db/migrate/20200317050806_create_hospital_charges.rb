# frozen_string_literal: true

class CreateHospitalCharges < ActiveRecord::Migration[6.0]
  def change
    create_table :hospital_charges do |t|
      t.references :user, null: false, foreign_key: true
      t.references :hospital, null: false, foreign_key: true

      t.timestamps
    end
  end
end
