# frozen_string_literal: true

class AddUsedToSubOrderGeneral < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_order_generals, :used, :string
  end
end
