# frozen_string_literal: true

class AddContainNewElemetToSubOrders < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_orders, :contain_new_element, :boolean, default: false
  end
end
