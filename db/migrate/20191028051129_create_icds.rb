# frozen_string_literal: true

class CreateIcds < ActiveRecord::Migration[6.0]
  def change
    create_table :icds do |t|
      t.string :description

      t.timestamps
    end
  end
end
