class AddCancelationMotiveToSubOrders < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_orders, :cancelation_motive, :string
  end
end
