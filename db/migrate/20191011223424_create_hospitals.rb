# frozen_string_literal: true

class CreateHospitals < ActiveRecord::Migration[6.0]
  def change
    create_table :hospitals do |t|
      t.string :name
      t.string :description
      t.string :address
      t.string :city
      t.string :contact
      t.string :contact_area
      t.string :phone
      t.string :extension
      t.string :mobile
      t.string :email
      t.string :status
      t.string :zone
      t.string :state
      t.string :rfc
      t.string :suburb
      t.string :business_name
      t.string :business_reason
      t.string :postal_code

      t.timestamps
    end
  end
end
