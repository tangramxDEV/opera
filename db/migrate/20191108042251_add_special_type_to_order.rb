# frozen_string_literal: true

class AddSpecialTypeToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :special_type, :string, default: 'Normal'
  end
end
