# frozen_string_literal: true

class AddSelectedProvidersToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :selected_providers, :string, array: true, default: []
  end
end
