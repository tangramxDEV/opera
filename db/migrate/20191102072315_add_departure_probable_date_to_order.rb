# frozen_string_literal: true

class AddDepartureProbableDateToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :departure_probable_date, :date
  end
end
