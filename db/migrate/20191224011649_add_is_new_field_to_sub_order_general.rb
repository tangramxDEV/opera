# frozen_string_literal: true

class AddIsNewFieldToSubOrderGeneral < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_order_generals, :is_new, :boolean, default: false
  end
end
