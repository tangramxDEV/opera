# frozen_string_literal: true

class ChangeStoredAtToBeDatetimeInDocument < ActiveRecord::Migration[6.0]
  def up
    change_column :documents, :stored_at, :string
  end

  def down
    change_column :documents, :stored_at, :date
  end
end
