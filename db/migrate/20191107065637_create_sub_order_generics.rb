# frozen_string_literal: true

class CreateSubOrderGenerics < ActiveRecord::Migration[6.0]
  def change
    create_table :sub_order_generics do |t|
      t.string :generic
      t.references :sub_order, null: false, foreign_key: true
      t.string :authorized
      t.string :motive
      t.string :quantity

      t.timestamps
    end
  end
end
