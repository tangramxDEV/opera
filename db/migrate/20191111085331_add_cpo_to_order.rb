# frozen_string_literal: true

class AddCpoToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :cpo, :string
  end
end
