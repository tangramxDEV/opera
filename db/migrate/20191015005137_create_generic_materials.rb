# frozen_string_literal: true

class CreateGenericMaterials < ActiveRecord::Migration[6.0]
  def change
    create_table :generic_materials do |t|
      t.string :generic
      t.string :suffering_type

      t.timestamps
    end
  end
end
