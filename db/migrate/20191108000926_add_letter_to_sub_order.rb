# frozen_string_literal: true

class AddLetterToSubOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_orders, :letter, :string
  end
end
