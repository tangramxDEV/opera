# frozen_string_literal: true

class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.references :doctor, null: false, foreign_key: true
      t.references :hospital, null: false, foreign_key: true
      t.string :suffering
      t.date :surgery_date
      t.integer :days_of_stay
      t.string :policy
      t.string :claim
      t.string :gnp_invoice
      t.string :coinsurance_percentage
      t.string :coinsurance_money
      t.string :insured_name
      t.string :insured_last_name
      t.string :insured_mother_last_name
      t.string :insured_gender
      t.string :insured_email
      t.string :insured_phone

      t.timestamps
    end
  end
end
