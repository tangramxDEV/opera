# frozen_string_literal: true

class CreateFileUploaders < ActiveRecord::Migration[6.0]
  def change
    create_table :file_uploaders, &:timestamps
  end
end
