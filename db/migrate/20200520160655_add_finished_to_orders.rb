# frozen_string_literal: true

class AddFinishedToOrders < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :finished, :boolean, default: false
  end
end
