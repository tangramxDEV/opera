# frozen_string_literal: true

class RemoveOrderIdFromComments < ActiveRecord::Migration[6.0]
  def change
    remove_reference :comments, :order, null: false, foreign_key: true
  end
end
