# frozen_string_literal: true

class AddProbabbleDateToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :entry_probable_date, :date
  end
end
