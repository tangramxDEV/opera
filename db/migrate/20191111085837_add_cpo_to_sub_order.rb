# frozen_string_literal: true

class AddCpoToSubOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_orders, :cpo, :string
  end
end
