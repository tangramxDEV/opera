# frozen_string_literal: true

class AddSlugToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :slug, :string, null: false
    add_index :orders, :slug, unique: true
  end
end
