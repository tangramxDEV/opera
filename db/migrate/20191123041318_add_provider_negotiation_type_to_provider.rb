# frozen_string_literal: true

class AddProviderNegotiationTypeToProvider < ActiveRecord::Migration[6.0]
  def change
    add_column :providers, :provider_negotiation_type, :string
  end
end
