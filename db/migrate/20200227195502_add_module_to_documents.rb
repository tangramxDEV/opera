# frozen_string_literal: true

class AddModuleToDocuments < ActiveRecord::Migration[6.0]
  def change
    add_column :documents, :module, :string
  end
end
