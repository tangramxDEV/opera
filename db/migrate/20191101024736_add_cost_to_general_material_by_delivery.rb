# frozen_string_literal: true

class AddCostToGeneralMaterialByDelivery < ActiveRecord::Migration[6.0]
  def change
    add_column :general_material_by_deliveries, :cost, :string
  end
end
