# frozen_string_literal: true

class AddSlugToSubOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_orders, :slug, :string, null: false
    add_index :sub_orders, :slug, unique: true
  end
end
