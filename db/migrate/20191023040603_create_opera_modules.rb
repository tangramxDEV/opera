# frozen_string_literal: true

class CreateOperaModules < ActiveRecord::Migration[6.0]
  def change
    create_table :opera_modules do |t|
      t.string :module_name

      t.timestamps
    end
  end
end
