# frozen_string_literal: true

class AddCoinsuranceToHospital < ActiveRecord::Migration[6.0]
  def change
    add_column :hospitals, :coinsurance, :boolean
  end
end
