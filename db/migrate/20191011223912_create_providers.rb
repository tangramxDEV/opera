# frozen_string_literal: true

class CreateProviders < ActiveRecord::Migration[6.0]
  def change
    create_table :providers do |t|
      t.string :name
      t.string :rfc
      t.string :responsable
      t.string :status
      t.string :zone
      t.string :contact_person
      t.boolean :coinsurance
      t.string :contact_number
      t.string :email

      t.timestamps
    end
  end
end
