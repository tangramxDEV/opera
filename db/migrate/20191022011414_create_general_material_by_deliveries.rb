# frozen_string_literal: true

class CreateGeneralMaterialByDeliveries < ActiveRecord::Migration[6.0]
  def change
    create_table :general_material_by_deliveries do |t|
      t.references :general_material, null: false, foreign_key: true
      t.references :delivery, null: false, foreign_key: true
      t.integer :quantity

      t.timestamps
    end
  end
end
