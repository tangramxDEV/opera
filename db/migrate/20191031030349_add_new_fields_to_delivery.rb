# frozen_string_literal: true

class AddNewFieldsToDelivery < ActiveRecord::Migration[6.0]
  def change
    add_column :deliveries, :motive, :string
    add_column :deliveries, :authorized, :string
  end
end
