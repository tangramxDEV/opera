# frozen_string_literal: true

class AddDeductibleAndMedicalFeesToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :deductible, :string
    add_column :orders, :medical_fees, :string
  end
end
