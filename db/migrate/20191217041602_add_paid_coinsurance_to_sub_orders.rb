# frozen_string_literal: true

class AddPaidCoinsuranceToSubOrders < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_orders, :paid_coinsurance, :integer
  end
end
