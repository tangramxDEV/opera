# frozen_string_literal: true

class CreateOrderComments < ActiveRecord::Migration[6.0]
  def change
    create_table :order_comments do |t|
      t.references :order, null: false, foreign_key: true
      t.string :comment
      t.string :user
      t.string :module
      t.string :date

      t.timestamps
    end
  end
end
