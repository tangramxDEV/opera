# frozen_string_literal: true

class AddUserToDocument < ActiveRecord::Migration[6.0]
  def change
    add_column :documents, :user, :string
  end
end
