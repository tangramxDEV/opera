# frozen_string_literal: true

class AddPreviewToDocument < ActiveRecord::Migration[6.0]
  def change
    add_column :documents, :preview, :string
  end
end
