# frozen_string_literal: true

class AddMaterialCostAndHospitalCostToProcedureOffered < ActiveRecord::Migration[6.0]
  def change
    add_column :procedure_offereds, :material_cost, :float
    add_column :procedure_offereds, :hospital_cost, :float
  end
end
