# frozen_string_literal: true

class AddCodedToSubOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_orders, :coded, :boolean, default: false
  end
end
