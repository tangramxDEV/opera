class AddGeneratedAtAndUserReferencesToStatusHistory < ActiveRecord::Migration[6.0]
  def change
    add_column :status_histories, :generated_at, :string
    add_reference :status_histories, :user, null: false, foreign_key: true, default: 200
  end
end
