# frozen_string_literal: true

class CreateStatusHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :status_histories do |t|
      t.references :sub_order, null: false, foreign_key: true
      t.string :status, null: false
      t.timestamps
    end
  end
end
