# frozen_string_literal: true

class AddAuthorizedAndObservationsToSubOrderGeneral < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_order_generals, :authorized, :string
    add_column :sub_order_generals, :obvservations, :string
  end
end
