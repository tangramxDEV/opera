# frozen_string_literal: true

class AddCptsToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :icd, :string
    add_column :orders, :cpt, :string
    add_column :orders, :second_cpt, :string
  end
end
