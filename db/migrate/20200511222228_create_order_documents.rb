# frozen_string_literal: true

class CreateOrderDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :order_documents do |t|
      t.references :order, null: false, foreign_key: true
      t.date :stored_at
      t.string :document_type
      t.string :preview
      t.string :user
      t.string :module
      t.timestamps
    end
  end
end
