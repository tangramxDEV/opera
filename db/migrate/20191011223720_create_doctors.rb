# frozen_string_literal: true

class CreateDoctors < ActiveRecord::Migration[6.0]
  def change
    create_table :doctors do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :professional_id
      t.string :rfc
      t.string :speciality
      t.string :medical_circle
      t.string :status
      t.string :zone
      t.string :state
      t.string :city
      t.string :address
      t.string :postal_code
      t.date :contract_date

      t.timestamps
    end
  end
end
