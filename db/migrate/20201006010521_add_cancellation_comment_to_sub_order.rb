class AddCancellationCommentToSubOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_orders, :cancellation_comment, :string
  end
end
