class AddNegotiatedAmountToSubOrderGenerals < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_order_generals, :negotiated_amount, :float, default: 0
  end
end
