# frozen_string_literal: true

class CreateProcedureOffereds < ActiveRecord::Migration[6.0]
  def change
    create_table :procedure_offereds do |t|
      t.references :procedure, null: false, foreign_key: true
      t.references :hospital, null: false, foreign_key: true

      t.timestamps
    end
  end
end
