# frozen_string_literal: true

class AddNewDataToGeneralMaterialByDelivery < ActiveRecord::Migration[6.0]
  def change
    add_column :general_material_by_deliveries, :authorization, :string
    add_column :general_material_by_deliveries, :observations, :string
  end
end
