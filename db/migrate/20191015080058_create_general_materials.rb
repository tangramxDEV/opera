# frozen_string_literal: true

class CreateGeneralMaterials < ActiveRecord::Migration[6.0]
  def change
    create_table :general_materials do |t|
      t.string :description
      t.string :code
      t.references :generic_material, null: false, foreign_key: true

      t.timestamps
    end
  end
end
