# frozen_string_literal: true

class AddSufferingTypeToIcd < ActiveRecord::Migration[6.0]
  def change
    add_column :icds, :suffering_type, :string
  end
end
