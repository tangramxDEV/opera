# frozen_string_literal: true

class AddSufferingTypeToCpt < ActiveRecord::Migration[6.0]
  def change
    add_column :cpts, :suffering_type, :string
  end
end
