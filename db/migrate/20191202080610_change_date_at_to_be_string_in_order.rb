# frozen_string_literal: true

class ChangeDateAtToBeStringInOrder < ActiveRecord::Migration[6.0]
  def up
    change_column :orders, :surgical_event_date, :string
    change_column :orders, :departure_probable_date, :string
  end

  def down
    change_column :orders, :surgical_event_date, :date
    change_column :orders, :departure_probable_date, :date
  end
end
