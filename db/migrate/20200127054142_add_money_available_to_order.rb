# frozen_string_literal: true

class AddMoneyAvailableToOrder < ActiveRecord::Migration[6.0]
  def change
    add_column :orders, :money_available, :float
  end
end
