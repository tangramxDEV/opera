# frozen_string_literal: true

class CreateSufferings < ActiveRecord::Migration[6.0]
  def change
    create_table :sufferings do |t|
      t.string :description

      t.timestamps
    end
  end
end
