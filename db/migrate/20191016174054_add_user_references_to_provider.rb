# frozen_string_literal: true

class AddUserReferencesToProvider < ActiveRecord::Migration[6.0]
  def change
    add_reference :providers, :user, null: false, foreign_key: true
  end
end
