# frozen_string_literal: true

class CreateProvidersUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :providers_users do |t|
      t.belongs_to :provider
      t.belongs_to :user
      t.timestamps
    end
  end
end
