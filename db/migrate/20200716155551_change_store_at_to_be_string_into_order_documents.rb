# frozen_string_literal: true

class ChangeStoreAtToBeStringIntoOrderDocuments < ActiveRecord::Migration[6.0]
  def up
    change_column :order_documents, :stored_at, :string
  end

  def down
    change_column :order_documents, :stored_at, :date
  end
end
