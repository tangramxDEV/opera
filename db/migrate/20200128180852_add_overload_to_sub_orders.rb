# frozen_string_literal: true

class AddOverloadToSubOrders < ActiveRecord::Migration[6.0]
  def change
    add_column :sub_orders, :overload, :boolean, default: false
  end
end
