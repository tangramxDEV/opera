# frozen_string_literal: true

class ChangeToPayToBeFloatInSubOrders < ActiveRecord::Migration[6.0]
  def up
    change_column :sub_orders, :to_pay, :decimal
  end

  def down
    change_column :sub_orders, :to_pay, :integer
  end
end
