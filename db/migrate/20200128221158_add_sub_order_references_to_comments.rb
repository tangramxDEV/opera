# frozen_string_literal: true

class AddSubOrderReferencesToComments < ActiveRecord::Migration[6.0]
  def change
    add_reference :comments, :sub_order, null: false, foreign_key: true
  end
end
