# frozen_string_literal: true

class AddNewDataToComment < ActiveRecord::Migration[6.0]
  def change
    add_column :comments, :user, :string
    add_column :comments, :module, :string
    add_column :comments, :date, :string
  end
end
