# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_10_06_010521) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "comments", force: :cascade do |t|
    t.string "comment"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "user"
    t.string "module"
    t.string "date"
    t.bigint "sub_order_id", null: false
    t.index ["sub_order_id"], name: "index_comments_on_sub_order_id"
  end

  create_table "cpts", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "suffering_type"
  end

  create_table "deliveries", force: :cascade do |t|
    t.bigint "order_id", null: false
    t.bigint "provider_id", null: false
    t.string "quantity"
    t.bigint "generic_material_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "motive"
    t.string "authorized"
    t.index ["generic_material_id"], name: "index_deliveries_on_generic_material_id"
    t.index ["order_id"], name: "index_deliveries_on_order_id"
    t.index ["provider_id"], name: "index_deliveries_on_provider_id"
  end

  create_table "doctors", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "phone"
    t.string "professional_id"
    t.string "rfc"
    t.string "speciality"
    t.string "medical_circle"
    t.string "status"
    t.string "zone"
    t.string "state"
    t.string "city"
    t.string "address"
    t.string "postal_code"
    t.date "contract_date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "documents", force: :cascade do |t|
    t.string "stored_at"
    t.string "document_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "preview"
    t.string "user"
    t.bigint "sub_order_id", null: false
    t.string "module"
    t.index ["sub_order_id"], name: "index_documents_on_sub_order_id"
  end

  create_table "file_uploaders", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "general_material_by_deliveries", force: :cascade do |t|
    t.bigint "general_material_id", null: false
    t.bigint "delivery_id", null: false
    t.integer "quantity"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "authorization"
    t.string "observations"
    t.string "cost"
    t.index ["delivery_id"], name: "index_general_material_by_deliveries_on_delivery_id"
    t.index ["general_material_id"], name: "index_general_material_by_deliveries_on_general_material_id"
  end

  create_table "general_materials", force: :cascade do |t|
    t.text "description"
    t.string "code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "generic_materials", force: :cascade do |t|
    t.string "generic"
    t.string "suffering_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "hospital_charges", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "hospital_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["hospital_id"], name: "index_hospital_charges_on_hospital_id"
    t.index ["user_id"], name: "index_hospital_charges_on_user_id"
  end

  create_table "hospitals", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "address"
    t.string "city"
    t.string "contact"
    t.string "contact_area"
    t.string "phone"
    t.string "extension"
    t.string "mobile"
    t.string "email"
    t.string "status"
    t.string "zone"
    t.string "state"
    t.string "rfc"
    t.string "suburb"
    t.string "business_name"
    t.string "business_reason"
    t.string "postal_code"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "coinsurance"
  end

  create_table "icds", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "suffering_type"
  end

  create_table "opera_modules", force: :cascade do |t|
    t.string "module_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "order_comments", force: :cascade do |t|
    t.bigint "order_id", null: false
    t.string "comment"
    t.string "user"
    t.string "module"
    t.string "date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["order_id"], name: "index_order_comments_on_order_id"
  end

  create_table "order_documents", force: :cascade do |t|
    t.bigint "order_id", null: false
    t.string "stored_at"
    t.string "document_type"
    t.string "preview"
    t.string "user"
    t.string "module"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["order_id"], name: "index_order_documents_on_order_id"
  end

  create_table "orders", force: :cascade do |t|
    t.bigint "doctor_id", null: false
    t.bigint "hospital_id", null: false
    t.string "suffering"
    t.date "surgery_date"
    t.integer "days_of_stay"
    t.string "policy"
    t.string "claim"
    t.string "gnp_invoice"
    t.string "coinsurance_percentage"
    t.string "coinsurance_money"
    t.string "insured_name"
    t.string "insured_last_name"
    t.string "insured_mother_last_name"
    t.string "insured_gender"
    t.string "insured_email"
    t.string "insured_phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "status"
    t.string "procedure"
    t.string "icd"
    t.string "cpt"
    t.string "second_cpt"
    t.date "entry_probable_date"
    t.string "surgical_event_date"
    t.string "deductible"
    t.string "medical_fees"
    t.string "departure_probable_date"
    t.string "selected_providers", default: [], array: true
    t.string "order_type"
    t.datetime "departure_date"
    t.string "special_type", default: "Normal"
    t.string "cpo"
    t.string "episode"
    t.string "hospital_expense"
    t.string "hospital_coinsurance"
    t.decimal "money_available"
    t.bigint "user_id", default: 4, null: false
    t.string "slug", null: false
    t.boolean "finished", default: false
    t.string "evo_invoice"
    t.string "wf_invoice"
    t.index ["doctor_id"], name: "index_orders_on_doctor_id"
    t.index ["hospital_id"], name: "index_orders_on_hospital_id"
    t.index ["slug"], name: "index_orders_on_slug", unique: true
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "procedure_offereds", force: :cascade do |t|
    t.bigint "procedure_id", null: false
    t.bigint "hospital_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "cost"
    t.float "material_cost"
    t.float "hospital_cost"
    t.index ["hospital_id"], name: "index_procedure_offereds_on_hospital_id"
    t.index ["procedure_id"], name: "index_procedure_offereds_on_procedure_id"
  end

  create_table "procedures", force: :cascade do |t|
    t.string "name"
    t.string "procedure_type"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "provider_materials", force: :cascade do |t|
    t.bigint "provider_id", null: false
    t.bigint "general_material_id", null: false
    t.string "price"
    t.string "brand"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["general_material_id"], name: "index_provider_materials_on_general_material_id"
    t.index ["provider_id"], name: "index_provider_materials_on_provider_id"
  end

  create_table "providers", force: :cascade do |t|
    t.string "name"
    t.string "rfc"
    t.string "responsable"
    t.string "status"
    t.string "zone"
    t.string "contact_person"
    t.boolean "coinsurance"
    t.string "contact_number"
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "affiliate_code"
    t.string "provider_negotiation_type"
    t.boolean "is_new", default: false
    t.string "slug", null: false
    t.index ["slug"], name: "index_providers_on_slug", unique: true
  end

  create_table "providers_users", force: :cascade do |t|
    t.bigint "provider_id"
    t.bigint "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["provider_id"], name: "index_providers_users_on_provider_id"
    t.index ["user_id"], name: "index_providers_users_on_user_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "status_histories", force: :cascade do |t|
    t.bigint "sub_order_id", null: false
    t.string "status", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "generated_at"
    t.bigint "user_id", default: 1, null: false
    t.index ["sub_order_id"], name: "index_status_histories_on_sub_order_id"
    t.index ["user_id"], name: "index_status_histories_on_user_id"
  end

  create_table "sub_order_generals", force: :cascade do |t|
    t.bigint "sub_order_generic_id", null: false
    t.bigint "provider_material_id", null: false
    t.string "quantity"
    t.string "cost"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "authorized"
    t.string "obvservations"
    t.string "used"
    t.string "provider_used"
    t.boolean "is_new", default: false
    t.float "negotiated_amount", default: 0.0
    t.index ["provider_material_id"], name: "index_sub_order_generals_on_provider_material_id"
    t.index ["sub_order_generic_id"], name: "index_sub_order_generals_on_sub_order_generic_id"
  end

  create_table "sub_order_generics", force: :cascade do |t|
    t.string "generic"
    t.bigint "sub_order_id", null: false
    t.string "authorized"
    t.string "motive"
    t.string "quantity"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["sub_order_id"], name: "index_sub_order_generics_on_sub_order_id"
  end

  create_table "sub_orders", force: :cascade do |t|
    t.string "invoice"
    t.bigint "provider_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "order_id", null: false
    t.boolean "coded", default: false
    t.string "letter"
    t.string "cpo"
    t.string "status", default: "ok"
    t.integer "negotiated_amount"
    t.decimal "to_pay", default: "0.0"
    t.integer "paid_coinsurance"
    t.boolean "overload", default: false
    t.boolean "contain_new_element", default: false
    t.string "slug", null: false
    t.boolean "extemporaneous", default: false
    t.boolean "skip_overload", default: false
    t.string "cancelation_motive"
    t.datetime "cancelation_date"
    t.string "cancellation_comment"
    t.index ["order_id"], name: "index_sub_orders_on_order_id"
    t.index ["provider_id"], name: "index_sub_orders_on_provider_id"
    t.index ["slug"], name: "index_sub_orders_on_slug", unique: true
  end

  create_table "sufferings", force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "treatments", force: :cascade do |t|
    t.bigint "hospital_id", null: false
    t.bigint "suffering_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["hospital_id"], name: "index_treatments_on_hospital_id"
    t.index ["suffering_id"], name: "index_treatments_on_suffering_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "opera_module"
    t.string "username"
    t.string "login_token"
    t.string "unique_session_id"
    t.boolean "active", default: true
    t.integer "failed_attempts", default: 0, null: false
    t.datetime "locked_at"
    t.string "unlock_token"
    t.string "slug", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["slug"], name: "index_users_on_slug", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "comments", "sub_orders"
  add_foreign_key "deliveries", "generic_materials"
  add_foreign_key "deliveries", "orders"
  add_foreign_key "deliveries", "providers"
  add_foreign_key "documents", "sub_orders"
  add_foreign_key "general_material_by_deliveries", "deliveries"
  add_foreign_key "general_material_by_deliveries", "general_materials"
  add_foreign_key "hospital_charges", "hospitals"
  add_foreign_key "hospital_charges", "users"
  add_foreign_key "order_comments", "orders"
  add_foreign_key "order_documents", "orders"
  add_foreign_key "orders", "doctors"
  add_foreign_key "orders", "hospitals"
  add_foreign_key "orders", "users"
  add_foreign_key "procedure_offereds", "hospitals"
  add_foreign_key "procedure_offereds", "procedures"
  add_foreign_key "provider_materials", "general_materials"
  add_foreign_key "provider_materials", "providers"
  add_foreign_key "status_histories", "sub_orders"
  add_foreign_key "status_histories", "users"
  add_foreign_key "sub_order_generals", "provider_materials"
  add_foreign_key "sub_order_generals", "sub_order_generics"
  add_foreign_key "sub_order_generics", "sub_orders"
  add_foreign_key "sub_orders", "orders"
  add_foreign_key "sub_orders", "providers"
  add_foreign_key "treatments", "hospitals"
  add_foreign_key "treatments", "sufferings"
end
