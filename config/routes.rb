# frozen_string_literal: true

Rails.application.routes.draw do
  get 'sub_order_generals/destroy'
  devise_for :users,
             path: '', # optional namespace or empty string for no space
             path_names: {
               sign_in: 'login',
               sign_out: 'logout',
               password: 'secret',
               confirmation: 'verification',
               registration: 'register',
               sign_up: 'signup'
             }

  get 'active'  => 'sessions#active'
  get 'timeout' => 'sessions#timeout'

  get 'nosuboarchivos' => 'secrets#new'
  get 'global_report' => 'reports#global'

  devise_scope :user do
    authenticated :user do
      root 'dashboard#index', as: :authenticated_root
    end

    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

  resources :orders, param: :slug do
    resources :sub_orders, param: :slug
  end

  post '/hospital_charges', to: 'hospital_charges#create'
  get '/hospital_charges/:user_id', to: 'hospital_charges#index'
  delete '/hospital_charges/:id', to: 'hospital_charges#destroy'

  resources :orders, param: :slug
  get '/pending/orders', to: 'orders#pending'

  resources :report_orders
  resources :sub_orders, param: :slug
  get '/sub_orders/one/:id', to: 'sub_orders#get_sub_order'
  resources :doctors
  resources :hospitals do
    resources :sufferings, only: %i[index]
    resources :procedures, only: %i[index]
  end
  resources :providers, param: :slug
  post '/providers/hold', to: 'providers#on_hold'
  resources :users, param: :slug
  resources :generic_materials
  resources :general_materials, only: %i[index]
  resources :provider_materials
  resources :hospital_supervisions, only: %i[index show update]
  resources :hospital_supervision_provider_orders, only: %i[index show update]

  get '/cancelations/last_ten', to: 'cancelations#last_ten'
  resources :cancelations, only: %i[index show update]
  resources :intelligence_centers, only: %i[index show update]
  resources :dictums, only: %i[index show]
  resources :icds, only: %i[index create]
  resources :cpts, only: %i[index create]

  resources :file_uploaders
  resources :provider_payments
  resources :provider_deliveries
  resources :provider_qualities, only: %i[index show update]
  resources :negotiations
  resources :provider_negotiations, only: %i[index show update]
  resources :material_negotiations, only: %i[index show update]
  resources :case_managements
  resources :new_supplies, only: %i[index show update]
  
  resources :sub_order_generals, only: %i[destroy]
  resources :qualities, only: %i[index show update]
  resources :extemporaneous, only: %i[index show update]
  resources :operational_consultings, only: %i[index show]
  get '/operational_consulting/last_ten', to: 'operational_consultings#last_ten'
  resources :operational_consulting_providers, only: %i[index show]
  get '/operational_consulting_provider/last_ten', to: 'operational_consulting_providers#last_ten'
  resources :documents, only: %i[destroy]
  resources :overuse
  get '/overuse/:id/sub_orders', to: 'overuse#sub_orders'
end
