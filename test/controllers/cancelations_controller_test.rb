require "test_helper"

describe CancelationsController do
  it "must get index" do
    get cancelations_index_url
    must_respond_with :success
  end

  it "must get show" do
    get cancelations_show_url
    must_respond_with :success
  end

  it "must get update" do
    get cancelations_update_url
    must_respond_with :success
  end

end
