# frozen_string_literal: true

require 'test_helper'

class SubOrderGeneralsControllerTest < ActionDispatch::IntegrationTest
  test 'should get destroy' do
    get sub_order_generals_destroy_url
    assert_response :success
  end
end
