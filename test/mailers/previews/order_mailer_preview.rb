# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/order_mailer
class OrderMailerPreview < ActionMailer::Preview
  def new_provider_order
    OrderMailer.new_provider_order Delivery.new(
      order_id: 1,
      provider_id: 1,
      generic_material_id: 1,
      quantity: 10
    )
  end
end
