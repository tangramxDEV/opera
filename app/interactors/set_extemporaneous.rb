class SetExtemporaneous
  include Interactor

  delegate :sub_order, to: :context

  def call
    begin
      sub_order.update(extemporaneous: true)
    rescue => error
      context.fail!(message: error)
    end
  end
end
