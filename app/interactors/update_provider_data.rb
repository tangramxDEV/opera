class UpdateProviderData
  include Interactor
  delegate :provider, :provider_params, to: :context

  def call
    begin
      provider.update provider_params
    rescue => err
      context.fail!(message: err)
    end
  end
  
end