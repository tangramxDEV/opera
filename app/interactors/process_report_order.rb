# frozen_string_literal: true

class ProcessReportOrder
  include Interactor::Organizer

  organize FindOrCreateSubOrder, UpdateStatusHistory, CalculateExtemporaneity, StoreComment, StoreDocument, StoreEmptySubOrderGeneric, StoreReportMaterials, ValidateIfNegotiable
end