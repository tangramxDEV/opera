class StoreReportMaterials
  include Interactor

  delegate :materials, :sub_order_generic, :sub_order, to: :context

  def call
    begin
      materials.map do |material|
        SubOrderGeneral.create(
          sub_order_generic_id: sub_order_generic.id,
          provider_material_id: material[:material_id],
          provider_used: material[:provider_used],
          quantity: material[:quantity],
          cost: material[:cost],
          is_new: material[:is_new]
        )
        next unless material[:is_new] == true
  
        sub_order.update!(
          contain_new_element: true
        )
      end
    rescue => exception
      context.fail!(message: exception)
    end
  end
end