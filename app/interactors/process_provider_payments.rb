# frozen_string_literal: true

class ProcessProviderPayments
  include Interactor::Organizer

  organize StoreComment, UpdateStatusHistory
end
