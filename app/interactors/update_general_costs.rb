class UpdateGeneralCosts
  include Interactor

  delegate :sub_order, :cost_list, to: :context

  def call
    begin
      cost_list.try(:each) do |cost|
        sub_order_general = SubOrderGeneral.find(cost.last['sub_order_general_id'])
        
        old_cost = sub_order_general.cost.to_f
        new_to_pay = (sub_order.to_pay.to_f - old_cost) + cost.last['cost'].to_f
        sub_order.update(
          to_pay: new_to_pay
        )
        sub_order_general.update(
          cost: cost.last['cost'],
          negotiated_amount: cost.last['cost']
        )
      end
      if sub_order.provider.coinsurance
        sub_order.calculate_coinsurance(sub_order.order)
      end
    rescue => error
      context.fail!(message: error)
    end
  end
end