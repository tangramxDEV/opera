class StoreOrderDocuments
  include Interactor

  delegate :order, :documents, to: :context

  def call
    begin
      documents.try(:each) do |document|
        order.order_documents.create(
          stored_at: (Time.now - 5*60*60).strftime('%d-%m-%Y %I:%M:%S %p'),
          document_type: document[:document_type],
          preview: document[:preview],
          user: document[:user],
          module: document[:module]
        )
      end
    rescue => exception
      context.fail!(message: exception)
    end
  end
end