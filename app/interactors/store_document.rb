# frozen_string_literal: true

class StoreDocument
  include Interactor

  delegate :sub_order, :documents, to: :context

  def call
    if !documents.nil? && documents.present?
      documents.map do |document|
        new_document = sub_order.documents.build(
          stored_at: (Time.now - 5*60*60).strftime('%d-%m-%Y %I:%M:%S %p'),
          document_type: document[:document_type],
          preview: document[:preview],
          user: document[:user],
          module: document[:module]
        )

        context.fail!(errors: new_document.errors) unless new_document.save
      end
    end
  end
end
