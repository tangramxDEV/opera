class StoreEmptySubOrderGeneric
  include Interactor
  
  delegate :sub_order, to: :context

  def call
    begin
      sub_order_generic = SubOrderGeneric.create(
        generic: '',
        sub_order_id: sub_order.id
      )

      context.sub_order_generic = sub_order_generic
    rescue => error
      context.fail!(message: error)
    end
  end
end