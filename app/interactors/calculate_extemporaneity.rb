# frozen_string_literal: true

class CalculateExtemporaneity
  include Interactor

  delegate :current_user, :sub_order, to: :context

  def call
    begin
      order = sub_order.order
      if order.departure_date && sub_order.coded && sub_order.status != 'pending'
        # current_date_time = DateTime.now - (6.0 / 24)
        #   if current_date_time > ActiveSupport::TimeZone['Central Time (US & Canada)'].parse(@order.departure_date.to_s)
        if DateTime.now > ActiveSupport::TimeZone['UTC'].parse(order.departure_date.to_s)
          status = 'extemporaneous'
          sub_order.update!(status: status)
          sub_order.status_histories.create(
            status: status,
            generated_at: (Time.now - 5*60*60).strftime('%d-%m-%Y %I:%M:%S %p'),
            user_id: current_user.id
          )
          OrderMailer.with(sub_order: sub_order).notify_extemporaneous_sub_order.deliver_later
        end
      end
    rescue => exception
      context.fail!(message: exception)
    end
  end
end
