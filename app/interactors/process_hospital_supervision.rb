# frozen_string_literal: true

class ProcessHospitalSupervision
  include Interactor::Organizer

  organize StoreComment, StoreDocument, UpdateStatusHistory, Supervision, ValidateIfNegotiable
end
