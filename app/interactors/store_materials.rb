# frozen_string_literal: true

class StoreMaterials
  include Interactor

  delegate :sub_order, :sub_order_params, :materials, to: :context

  def call
    begin
      sub_order.update!(sub_order_params)
  
      if sub_order.coded
        # Email 4 - Notify to Hospital Supervision that order is coded
        OrderMailer.with(sub_order: sub_order).sub_order_coded.deliver_later
      end
      if !materials.nil? && materials.present?
        materials.each do |material|
          new_material = SubOrderGeneral.new(
            sub_order_generic_id: material[:sub_order_generic_id],
            provider_material_id: material[:id],
            provider_used: material[:used],
            quantity: material[:quantity],
            cost: material[:price],
            is_new: material[:is_new]
          )
          sub_order.update!(contain_new_element: true) if material[:is_new]
  
          context.fail!(errors: new_material.errors) unless new_material.save
        end
      end
    rescue => exception
      context.fail!(message: exception)
    end

  end
end
