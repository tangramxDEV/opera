class ProcessNewSupply
  include Interactor::Organizer

  organize StoreComment, StoreDocument, UpdateStatusHistory, UpdateProviderData, UpdateGeneralCosts
end