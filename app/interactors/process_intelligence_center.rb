# frozen_string_literal: true

class ProcessIntelligenceCenter
  include Interactor::Organizer

  organize StoreComment, StoreDocument, UpdateStatusHistory
end
