class StoreOrderComment
  include Interactor

  delegate :order, :comment, to: :context

  def call
    begin
      if !comment.nil? && comment.present?
        new_comment = order.order_comments.build(
          comment: comment[:comment],
          user: comment[:user],
          module: comment[:module],
          date: (Time.now - 5*60*60).strftime('%d-%m-%Y %I:%M:%S %p')
        )
  
        context.fail!(errors: new_comment.errors) unless new_comment.save
      end
    rescue => exception
      context.fail!(message: exception)
    end
  end
end