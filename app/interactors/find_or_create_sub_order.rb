# frozen_string_literal: true

class FindOrCreateSubOrder
  include Interactor

  delegate :provider, :order, to: :context
  
  def call
    begin
      new_sub_order = SubOrder.find_or_create_by(
        provider_id: provider.id,
        order_id: order.id
      ) do |sub_order|
        sub_order.cpo = "#{order.cpo}-#{order.sub_orders.count + 1}"
        sub_order.coded = true
      end

      context.sub_order = new_sub_order
    rescue => exception
      context.fail!(message: exception)  
    end
  end
end
