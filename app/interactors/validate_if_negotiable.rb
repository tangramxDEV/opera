# frozen_string_literal: true

class ValidateIfNegotiable
  include Interactor
  delegate :sub_order, :sub_order_params, :materials, to: :context

  def call
    begin
      if sub_order.coded && sub_order.contain_new_element && sub_order.status != 'pending'
        status = 'negotiable-matter'
        sub_order.update(status: status)
        sub_order.status_histories.create(
          status: status
        )
      end
    rescue => exception
      context.fail!(message: exception)  
    end
  end
end
