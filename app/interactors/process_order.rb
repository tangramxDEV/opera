class ProcessOrder
  include Interactor::Organizer

  organize StoreSubOrders, StoreOrderDocuments, StoreOrderComment, FinishOrder
end