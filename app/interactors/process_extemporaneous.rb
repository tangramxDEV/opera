# frozen_string_literal: true

class ProcessExtemporaneous
  include Interactor::Organizer

  organize StoreComment, StoreDocument, SetExtemporaneous, UpdateStatusHistory
end
