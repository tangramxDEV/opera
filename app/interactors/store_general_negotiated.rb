class StoreGeneralNegotiated
  include Interactor

  delegate :provider, :materials, to: :context

  def call
    begin
      materials.try(:each) do |material|
        gral = GeneralMaterial.create(
          description: material[:description],
          code: material[:code]
        )
  
        material_provider = ProviderMaterial.create(
          provider_id: provider.id,
          general_material_id: gral.id,
          price: material[:cost],
          brand: material[:brand]
        )
  
        order_material = SubOrderGeneral.create(
          sub_order_generic_id: material[:sub_order_generic_id],
          provider_material_id: material_provider.id,
          cost: material[:cost],
          quantity: material[:quantity],
          provider_used: material[:provider_used]
        )
      end
    rescue => error
      context.fail!(message: error)
    end
  end
end