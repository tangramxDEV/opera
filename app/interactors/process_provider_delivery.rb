# frozen_string_literal: true

class ProcessProviderDelivery
  include Interactor::Organizer

  organize StoreComment, UpdateStatusHistory
end
