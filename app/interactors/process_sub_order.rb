# frozen_string_literal: true

class ProcessSubOrder
  include Interactor::Organizer

  organize StoreComment, StoreDocument, UpdateStatusHistory
end