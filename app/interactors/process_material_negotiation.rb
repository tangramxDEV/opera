class ProcessMaterialNegotiation
  include Interactor::Organizer

  organize StoreComment, StoreDocument, UpdateStatusHistory
end