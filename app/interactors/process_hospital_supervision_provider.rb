# frozen_string_literal: true

class ProcessHospitalSupervisionProvider
  include Interactor::Organizer

  organize StoreComment, StoreDocument, StoreMaterials, CalculateExtemporaneity, ValidateIfNegotiable
end
