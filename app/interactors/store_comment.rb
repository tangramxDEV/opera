# frozen_string_literal: true

class StoreComment
  include Interactor

  delegate :sub_order, :comment, to: :context

  def call
    if !comment.nil? && comment.present?
      new_comment = sub_order.comments.build(
        comment: comment[:comment],
        user: comment[:user],
        module: comment[:module],
        date: (Time.now - 5*60*60).strftime('%d-%m-%Y %I:%M:%S %p')
      )

      context.fail!(errors: new_comment.errors) unless new_comment.save
    end
  end
end
