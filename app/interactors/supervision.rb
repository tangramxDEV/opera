# frozen_string_literal: true

class Supervision
  include Interactor

  delegate :sub_order, :sub_order_params, :generals, to: :context

  def call
    begin
      if !generals.nil? and generals.present?
        generals.each do |general|
          sub_order_general = SubOrderGeneral.find(general[:general_material_id])
          sub_order_general.update(
            authorized: general[:authorization],
            obvservations: general[:observation],
            used: general[:used]
          )
          if general[:authorization] != 'FUERA DE COBERTURA' and sub_order.status == 'cdi'
            to_pay = sub_order.to_pay + (sub_order_general.cost.to_f * sub_order_general.quantity.to_i)
            sub_order.update(
              to_pay: to_pay.to_f
            )
          else
            sub_order.update(overload: true)
            
            # Email 3
            OrderMailer.with(sub_order: sub_order).notify_sub_order_overloaded.deliver_later
          end
        end
        
        if sub_order.provider.coinsurance and sub_order.status == 'cdi'
          # If provider have coinsurance, proced operation to pay
          sub_order.calculate_coinsurance(sub_order.order) 
        end

      end
      
    rescue => e
      context.fail!(message: e)
    end
  end
end
