# frozen_string_literal: true

class ProcessCaseManagement
  include Interactor::Organizer

  organize StoreComment, StoreDocument, UpdateStatusHistory, Supervision
end