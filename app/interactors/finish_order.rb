class FinishOrder
  include Interactor

  delegate :current_user, :order, to: :context

  def call
    begin
      # Email 2 - Notify to Hospital Supervision of new case
      OrderMailer.with(order: order).notify_new_case_to_medic.deliver_later
      order.sub_orders.map do |sub_order|
        next unless sub_order.coded
        sub_order.status_histories.create(
          status: 'delivery',
          generated_at: (Time.now - 5*60*60).strftime('%d-%m-%Y %I:%M:%S %p'),
          user_id: current_user.id
        )
        sub_order.update(
          status: 'delivery'
        )
      end
    rescue => exception
      context.fail!(message: exception)
    end
  end
end