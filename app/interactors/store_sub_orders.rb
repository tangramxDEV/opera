class StoreSubOrders
  include Interactor
  
  delegate :current_user, :sub_orders, :order, to: :context
  
  def call
    begin
      sub_orders.map.with_index do |sub_order, index|
        sub_order_status = 'hospital_supervision'
        provider = Provider.find(sub_order[:provider].to_i)
  
        sub_order_status = 'supplier_management' if provider.status == 'pending'
  
        new_sub_order = SubOrder.create(
          order_id: order.id,
          provider_id: sub_order[:provider].to_i,
          cpo: "#{order.cpo}-#{index + 1}",
          status: sub_order_status
        )
        new_sub_order.status_histories.create(
          status: sub_order_status,
          generated_at: (Time.now - 5*60*60).strftime('%d-%m-%Y %I:%M:%S %p'),
          user_id: current_user.id
        )
  
        sub_order[:materials].map do |material|
          SubOrderGeneric.create(
            sub_order_id: new_sub_order.id,
            generic: material[:generic],
            authorized: material[:authorized],
            motive: material[:motive],
            quantity: material[:quantity]
          )
        end
        
        SubOrderGeneric.create(
          sub_order_id: new_sub_order.id,
          generic: 'otro adicional',
          authorized: 'SI',
          motive: '',
          quantity: 1
        )
        
        # Email 1 Notify to provider of new order
        OrderMailer.with(sub_order: new_sub_order).new_provider_order.deliver_later
      end # end map
      
    rescue StandardError => error
      context.fail!(message: error.message)  
    end #end begin

  end # end call
end #end Class