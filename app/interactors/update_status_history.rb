# frozen_string_literal: true

class UpdateStatusHistory
  include Interactor

  delegate :current_user, :sub_order, :sub_order_params, to: :context

  def call
    begin
      # sub_order.update(status: sub_order_params[:status])
      sub_order.update(sub_order_params)
      if sub_order_params[:status].present?
        sub_order.status_histories.create(
          status: sub_order_params[:status],
          generated_at: (Time.now - 5*60*60).strftime('%d-%m-%Y %I:%M:%S %p'),
          user_id: current_user.id
        )
      end
    rescue => error
      context.fail!(message: error)
    end
  end
end
