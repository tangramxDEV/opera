# frozen_string_literal: true

class ProcessProviderNegotiation
  include Interactor::Organizer

  organize StoreComment, StoreDocument, UpdateStatusHistory, UpdateProviderData, StoreGeneralNegotiated
end