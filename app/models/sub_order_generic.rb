# frozen_string_literal: true

class SubOrderGeneric < ApplicationRecord
  belongs_to :sub_order

  has_many :sub_order_generals
end
