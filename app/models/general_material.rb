# frozen_string_literal: true

class GeneralMaterial < ApplicationRecord
  # belongs_to :generic_materials

  has_many :provider_materials
  has_many :providers, through: :provider_materials

  # has_many :general_material_by_deliveries
  # has_many :deliveries, through: :general_material_by_deliveries
end
