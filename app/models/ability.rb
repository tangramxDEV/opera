# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    if user.has_role?(:users)
      # ADMINISTRACION
      can :manage, :all
      can :show, ActiveStorage::Blob::Analyzable
    end
    if user.has_role?(:medical_opinion)
      # DICTAMEN
      can :show, ActiveStorage::Blob
      can %i[new edit update], Order
      can %i[show update], Order, finished: false
      can %i[show update], SubOrder, status: 'payment_provider'
    end
    if user.has_role?(:hospital_supervision)
      # SUPERVISION HOSPITALARIA
      can :show, ActiveStorage::Blob
      can :manage, SubOrder, status: 'hospital_supervision'
      can :manage, SubOrder, status: 'case_managment'
    end
    if user.has_role?(:power_center)
      # CENTRO DE INTELIGENCIA
      can :manage, SubOrder, status: 'cdi'
      can :show, ActiveStorage::Blob
    end
    if user.has_role?(:providers)
      # PROVEEDOR
      can :show, ActiveStorage::Blob
      can %i[read show update], SubOrder, status: 'quality'
      can %i[read show update], SubOrder, status: 'hospital_supervision'
      can :manage, SubOrder, status: 'finished'
      can :read, Order, order_type: 'REPORTE HOSPITALARIO'
      can %i[read show update], SubOrder
    end
    if user.has_role?(:providers_managment)
      # NEGOCIACION
      can :show, ActiveStorage::Blob
      can :manage, SubOrder, status: 'supplier_management'
      can :manage, SubOrder, status: 'negotiable-matter'
      can :manage, SubOrder, status: 'extemporaneous'
      can :read, Order
      can :manage, SubOrder, overload: true
    end
    if user.has_role?(:negotiation)
      # NEGOCIACION
      can :show, ActiveStorage::Blob
      can :manage, SubOrder, status: 'supplier_management'
      can :manage, SubOrder, status: 'negotiable-matter'
      can :manage, SubOrder, status: 'extemporaneous'
      can :read, Order
      can :manage, SubOrder, overload: true
    end
  end
end
