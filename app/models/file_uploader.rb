# frozen_string_literal: true

class FileUploader < ApplicationRecord
  has_one_attached :file

  validates :file,
            attached: true,
            content_type: { in: 'application/pdf', message: 'El archivo no es formato PDF' },
            size: { less_than: 5.megabytes, message: 'El archivo es muy grande' }
end
