# frozen_string_literal: true

class ProviderMaterial < ApplicationRecord
  belongs_to :provider
  belongs_to :general_material
end
