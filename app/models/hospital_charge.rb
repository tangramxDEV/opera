# frozen_string_literal: true

class HospitalCharge < ApplicationRecord
  belongs_to :user
  belongs_to :hospital
end
