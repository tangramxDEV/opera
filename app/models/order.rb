# frozen_string_literal: true

class Order < ApplicationRecord
  include Filterable
  scope :claim, ->(claim) { where claim: claim }
  scope :hospital, ->(hospital) { where hospital_id: hospital }
  scope :doctor, ->(doctor) { where doctor_id: doctor }
  scope :invoice, ->(invoice) { where 'lower(gnp_invoice) = ?', invoice }
  scope :surgery, ->(surgery) { where surgery_date: surgery }
  scope :type, ->(type) { where order_type: type }
  scope :status, ->(status) { where status: status }
  scope :cpo, ->(cpo) { where cpo: cpo }
  scope :like_cpo, ->(cpo) { where('orders.cpo LIKE ?', "%#{cpo}%") }
  scope :insured, lambda { |insured|
    where(
      "LOWER(insured_name || \' \' || insured_last_name || \' \' || insured_mother_last_name) LIKE ?",
      "%#{insured.downcase}%"
    )
  }

  belongs_to :doctor
  belongs_to :hospital
  belongs_to :user
  has_many :sub_orders
  has_many :order_comments
  has_many :order_documents
  before_create :set_slug

  def to_param
    slug
  end

  def insured
    "#{insured_name} #{insured_last_name} #{insured_mother_last_name}"
  end

  private

  def self.insured_full_name
    insured_name.to_s
  end

  def set_slug
    loop do
      self.slug = SecureRandom.uuid
      break unless Order.where(slug: slug).exists?
    end
  end
end
