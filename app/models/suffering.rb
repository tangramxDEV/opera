# frozen_string_literal: true

class Suffering < ApplicationRecord
  has_many :treatments
  has_many :hospitals, through: :treatments
end
