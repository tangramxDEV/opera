# frozen_string_literal: true

class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable, :timeoutable,
         :recoverable, :rememberable, :session_limitable, :lockable

  has_many :hospital_charges
  has_many :hospitals, through: :hospital_charges

  has_and_belongs_to_many :providers
  # before_update :password, if: :property_changed?
  before_create :set_slug

  def self.search(search)
    if search
      where('email LIKE ?', "%#{search}%")
    else
      all
    end
  end

  def to_param
    slug
  end

  def self.find_for_database_authentication(conditions = {})
    find_by(username: conditions[:email]) || find_by(email: conditions[:email])
  end

  def active_for_authentication?
    super && active
  end

  def inactive_message
    'Lo sentimos, parece que tu cuenta ha sido desactivada'
  end

  private

  def set_slug
    loop do
      self.slug = SecureRandom.uuid
      break unless User.where(slug: slug).exists?
    end
  end
end
