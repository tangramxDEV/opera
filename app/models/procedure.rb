# frozen_string_literal: true

class Procedure < ApplicationRecord
  has_many :procedure_offereds
  has_many :hospitals, through: :procedure_offereds
end
