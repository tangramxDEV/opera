# frozen_string_literal: true

class GeneralMaterialByDelivery < ApplicationRecord
  belongs_to :general_material
  belongs_to :delivery
end
