# frozen_string_literal: true

class Provider < ApplicationRecord
  has_many :deliveries
  has_many :orders, through: :deliveries

  has_many :sub_orders

  has_many :provider_materials
  has_many :general_materials, through: :deliveries

  has_and_belongs_to_many :users

  before_create :set_slug
  validates :name, presence: true

  def to_param
    slug
  end

  private

  def set_slug
    loop do
      self.slug = SecureRandom.uuid
      break unless Provider.where(slug: slug).exists?
    end
  end
end
