# frozen_string_literal: true

class SubOrder < ApplicationRecord
  belongs_to :provider
  belongs_to :order

  has_many :comments
  has_many :documents
  has_many :status_histories

  has_many :sub_order_generics

  before_create :set_slug

  def self.search(search)
    if search
      joins(:order).merge(
        Order.insured(search)
        .or(Order.like_cpo(search))
      )
    else
      all
    end
  end

  def to_param
    slug
  end

  def calculate_coinsurance(order)
    pay = 0

    if order.money_available.blank?
      amount_coinsurance_hospital = (order.hospital_expense.to_i - order.deductible.to_i) * order.hospital_coinsurance.to_i / 100
      amout_coinsurance_medic = order.medical_fees.to_i * order.coinsurance_percentage.to_i / 100
      money = order.coinsurance_money.to_i - (amount_coinsurance_hospital + amout_coinsurance_medic) # Total money for pay
      order.update!(money_available: money)
    end

    if order.money_available > order.hospital_coinsurance.to_f
      # The money Rule
      pay = self.to_pay * order.hospital_coinsurance.to_i / 100

      if order.money_available >= pay
        update(paid_coinsurance: pay)
        order.update!(money_available: order.money_available.to_i - pay)
      else
        update(paid_coinsurance: order.money_available)
        order.update!(money_available: 0)
      end
    end
  end

  private

  def set_slug
    loop do
      self.slug = SecureRandom.uuid
      break unless SubOrder.where(slug: slug).exists?
    end
  end
end
