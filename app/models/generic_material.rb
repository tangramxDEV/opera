# frozen_string_literal: true

class GenericMaterial < ApplicationRecord
  has_many :general_materials
end
