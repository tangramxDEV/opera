# frozen_string_literal: true

class Document < ApplicationRecord
  belongs_to :sub_order
end
