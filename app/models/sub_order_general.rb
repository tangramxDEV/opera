# frozen_string_literal: true

class SubOrderGeneral < ApplicationRecord
  belongs_to :sub_order_generic
  belongs_to :provider_material
end
