# frozen_string_literal: true

class Hospital < ApplicationRecord
  has_many :treatments
  has_many :sufferings, through: :treatments

  has_many :procedure_offereds
  has_many :procedures, through: :procedure_offereds

  has_many :hospital_charges
  has_many :users, through: :hospital_charges
end
