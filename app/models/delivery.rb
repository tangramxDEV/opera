# frozen_string_literal: true

class Delivery < ApplicationRecord
  belongs_to :order
  belongs_to :provider

  belongs_to :generic_material

  has_many :general_material_by_deliveries
  has_many :general_materials, through: :general_material_by_deliveries
end
