# frozen_string_literal: true

class ProcedureOffered < ApplicationRecord
  belongs_to :procedure
  belongs_to :hospital
end
