# frozen_string_literal: true

class OrderComment < ApplicationRecord
  belongs_to :order
end
