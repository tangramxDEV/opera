# frozen_string_literal: true

class OrderMailer < ApplicationMailer
  # default from: 'pablo.montes@admsalud.com.mx'

  # MENSAJE 1: Dictamen Medico genera una nueva solicitud.
  def new_provider_order
    @sub_order = params[:sub_order]

    # change to
    # sub_order.provider.email
    mail to: @sub_order.provider.email, subject: "Pedido Asignado, Folio: #{@sub_order.cpo}"
  end

  # MENSAJE 1.1: Dictamen Médico genera una nueva solicitud.
  def notify_new_case_to_medic
    @order = params[:order]
    @hospital = @order.hospital
    users = HospitalCharge.where(
      hospital_id: hospital.id
    ).map(&:user_id)
    hospital_supervision_team = User.find(users)

    hospital_supervision_team.each do |user|
      mail to: user.email, subject: "Ingreso a ortopedia, Folio: #{@order.cpo}"
    end
  end

  # MENSAJE 2: Supervision Hospitalaria ingresa fecha de alta
  #  y monto gasto hospitalario, ya sea que guarde o envie solicitud
  def notify_change_to_provider
    @sub_order = params[:sub_order]

    mail to: @sub_order.provider.email, subject: "Próxima alta #{@sub_order.cpo}"
  end

  # MENSAJE 3: Supervision Hospitalaria coloca Insumos con etiqueta Sin Justificación
  def notify_sub_order_overloaded
    @sub_order = params[:sub_order]
    @hospital = @sub_order.order.hospital
    users = HospitalCharge.where(
      hospital_id: @hospital.id
    ).map(&:user_id)
    cdi_team = User.find(users)

    cdi_team.each do |user|
      mail to: user.email, subject: "Insumos a negociación ticket #{@sub_order.cpo}"
    end
  end

  def sub_order_coded
    # @sub_order = params[:sub_order]
    # mail to: 'brenda.lopez@gnp.com.mx', subject: "Realizar visado de ortopedia #{@sub_order.cpo}"
  end

  # MENSAJE 4: Dictamen pago aproveedor envia solicitud con carta de autorizacion
  # al proveedor
  def notify_new_letter_to_provider
    @sub_order = params[:sub_order]

    mail to: @sub_order.provider.email, subject: "Carta GNP disponible #{@sub_order.cpo}"
  end

  # MENSAJE 4: Correo cuando una Solicitud es Extemporánea y
  # llega al Modulo de Negociacion
  def notify_extemporaneous_sub_order
    @sub_order = params[:sub_order]
    @hospital = @sub_order.order.hospital
    users = HospitalCharge.where(
      hospital_id: @hospital.id
    ).map(&:user_id)
    nego_team = User.find(users)

    nego_team.each do |user|
      mail to: user.email, subject: "Caso extemporaneo #{@sub_order.cpo}"
    end
  end
end
