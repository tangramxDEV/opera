# frozen_string_literal: true

class GeneralMaterialByDeliveriesSerializer < ActiveModel::Serializer
  attributes :id,
             :quantity,
             :cost,
             :delivery_id,
             :authorization,
             :observations
end
