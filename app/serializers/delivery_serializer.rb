# frozen_string_literal: true

class DeliverySerializer < ActiveModel::Serializer
  attributes :id,
             :provider_id,
             :quantity,
             :generic_material,
             :provider,
             :general_materials

  def generic_material
    ActiveModel::SerializableResource.new(object.generic_material, each_serializer: GenericMaterialSerializer)
  end

  def general_materials
    ActiveModel::SerializableResource.new(object.general_materials, each_serializer: GeneralMaterialSerializer)
  end
end
