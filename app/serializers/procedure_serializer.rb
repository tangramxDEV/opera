# frozen_string_literal: true

class ProcedureSerializer < ActiveModel::Serializer
  attributes :id, :name, :procedure_type, :procedure_offereds
end
