# frozen_string_literal: true

class ProviderSerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :responsable,
             :contact_number,
             :contact_person,
             :provider_negotiation_type,
             :affiliate_code,
             :coinsurance,
             :status

  has_many :deliveries
end
