# frozen_string_literal: true

class HospitalSerializer < ActiveModel::Serializer
  attributes :id, :name, :coinsurance, :city, :state

  # has_many :sufferings, each_serializer: SufferingSerializer
  # has_many :procedures, each_serializer: ProcedureSerializer
end
