# frozen_string_literal: true

class GenericMaterialSerializer < ActiveModel::Serializer
  attributes :id, :generic, :suffering_type

  # def general_materials
  #   ActiveModel::SerializableResource.new(object.general_materials, each_serializer: GeneralMaterialSerializer)
  # end
end
