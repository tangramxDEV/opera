# frozen_string_literal: true

class SubOrderGenericSerializer < ActiveModel::Serializer
  attributes :id,
             :authorized,
             :generic,
             :motive,
             :quantity
  :provider_used

  has_many :sub_order_generals
  # def sub_order_generals
  #   ActiveModel::SerializableResource.new(object.sub_order_generals, each_serializer: SubOrderGeneralSerializer)
  # end
end
