# frozen_string_literal: true

class SubOrderGeneralSerializer < ActiveModel::Serializer
  attributes :id,
             :quantity,
             :cost,
             :negotiated_amount,
             :authorized,
             :used,
             :obvservations,
             :provider_material,
             :provider_used,
             :is_new

  def provider_material
    ActiveModel::SerializableResource.new(object.provider_material, serializer: ProviderMaterialSerializer)
  end
end
