# frozen_string_literal: true

class SufferingSerializer < ActiveModel::Serializer
  attributes :id, :description
end
