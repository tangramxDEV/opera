# frozen_string_literal: true

class GeneralMaterialSerializer < ActiveModel::Serializer
  attributes :id,
             :description,
             :code
  # :provider_materials,
  # :general_material_by_deliveries

  # def general_material_by_deliveries
  #   ActiveModel::SerializableResource.new(object.general_material_by_deliveries, each_serializer: GeneralMaterialByDeliveriesSerializer)
  # end
end
