# frozen_string_literal: true

class ProcedureOfferedSerializer < ActiveModel::Serializer
  attributes :id, :cost, :hospital_cost
end
