# frozen_string_literal: true

class SubOrderSerializer < ActiveModel::Serializer
  attributes :id,
             :slug,
             :order,
             :sub_order_generics,
             :coded,
             :invoice,
             :letter,
             :cpo,
             :negotiated_amount,
             :status,
             :to_pay,
             :paid_coinsurance,
             :total,
             :comments,
             :documents

  belongs_to :provider

  # has_many :documents
  # has_many :comments

  def documents
    object.documents.order(id: :desc)
  end

  def comments
    sub_order_comments = []
    sub_order_comments.push(*object.comments.order(id: :desc))
    sub_order_comments.push(*object.order.order_comments)

    sub_order_comments
  end

  def total
    TotalSubOrderCalculatorService.calculate(object)
  end

  def sub_order_generics
    ActiveModel::SerializableResource.new(object.sub_order_generics, each_serializer: SubOrderGenericSerializer)
  end

  def order
    ActiveModel::SerializableResource.new(object.order, serializer: OrderSerializer)
  end
end
