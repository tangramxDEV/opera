# frozen_string_literal: true

class DoctorSerializer < ActiveModel::Serializer
  attributes :id, :name
end
