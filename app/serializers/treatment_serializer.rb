# frozen_string_literal: true

class TreatmentSerializer < ActiveModel::Serializer
  attributes :id, :description
end
