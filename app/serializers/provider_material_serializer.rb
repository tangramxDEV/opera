# frozen_string_literal: true

class ProviderMaterialSerializer < ActiveModel::Serializer
  attributes :id, :brand, :price, :description, :code

  has_one :general_material

  def code
    object.general_material.code.to_s
  end

  def description
    object.general_material.description.to_s
  end
end
