# frozen_string_literal: true

class OrderSerializer < ActiveModel::Serializer
  attributes :id,
             :slug,
             :order_type,
             :medical_fees,
             :deductible,
             :entry_probable_date,
             :surgical_event_date,
             :coinsurance_money,
             :coinsurance_percentage,
             :procedure,
             :icd,
             :cpt,
             :second_cpt,
             :insured,
             :claim,
             :gnp_invoice,
             :surgery_date,
             :suffering,
             :departure_date,
             :departure_probable_date,
             :cpo,
             :episode,
             :hospital_expense,
             :hospital_coinsurance,
             :include_negotiations,
             :wf_invoice,
             :evo_invoice

  has_one :doctor
  has_one :hospital
  has_many :order_comments
  has_many :order_documents

  def insured
    "#{object.insured_name} #{object.insured_last_name} #{object.insured_mother_last_name}"
  end

  ## Esto es un boloncheo que no deberia existir =(
  def include_negotiations
    negotiable = []
    object.sub_orders.each do |sub|
      negotiable.push(true) if sub.status == 'negotiable-matter'
    end
    object.sub_orders.length == negotiable.length
  end
end
