# frozen_string_literal: true

class ProceduresController < ApplicationController
  protect_from_forgery except: %i[index]
  before_action :authenticate_user!
  before_action :set_hospital, only: %i[index]

  def index
    @procedures = @hospital.procedures
    @procedures = @hospital.procedures.where(procedure_type: params[:type]) if params[:type]
    respond_to do |format|
      format.js { render json: @procedures, status: :ok }
      format.html
    end
  end

  private

  def set_hospital
    @hospital = Hospital.find params[:hospital_id]
  end
end
