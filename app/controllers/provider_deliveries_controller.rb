# frozen_string_literal: true

class ProviderDeliveriesController < ApplicationController
  protect_from_forgery except: %i[show]
  before_action :authenticate_user!
  before_action :set_sub_order, only: %i[show update]

  def index
    provider = current_user.providers.first
    @sub_orders = provider.sub_orders.includes(
      { order: %i[
        hospital
        doctor
      ] }
    )
                          .where(status: 'finished')
                          .order(order_id: :desc)
    respond_to do |format|
      format.js { render json: @sub_orders, status: :ok }
      format.html
    end
  end

  def show
    authorize! :show, @sub_order
    respond_to do |format|
      format.js { render json: @sub_order, status: :ok }
      format.html
    end
  end

  def update
    authorize! :update, @sub_order

    result = ProcessProviderDelivery.call(
      sub_order: @sub_order,
      sub_order_params: sub_order_params,
      comment: params[:comment],
      current_user: current_user
    )

    if result.success?
      flash[:success] = params[:message] || 'Guardado Exitoso'
      render json: { status: :ok }
    else
      render json: { status: :unprocessable_entity }
    end
  end

  private

  def set_sub_order
    @sub_order = SubOrder.find_by slug: params[:id]
  end

  def sub_order_params
    params.require(:sub_order).permit(:status)
  end
end
