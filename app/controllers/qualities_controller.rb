# frozen_string_literal: true

class QualitiesController < ApplicationController
  protect_from_forgery except: %i[index show]
  before_action :authenticate_user!
  before_action :set_sub_order, only: %i[show]

  def index
    @sub_orders = SubOrder.where(
      provider_id: current_user.providers.first.id,
      status: 'quality'
    )
                          .order(order_id: :desc)

    respond_to do |format|
      format.js { render json: @sub_orders, status: :ok }
      format.html
    end
  end

  def show
    authorize! :show, @sub_order
    respond_to do |format|
      format.js { render json: @sub_order, status: :ok }
      format.html
    end
  end

  # def update
  #   @sub_order.update sub_order_params
  #   CommentService.comment(@sub_order, params[:comment]) if !params[:comment].nil? && params[:comment].present?

  #   render json: { status: :ok }
  # end

  private

  def set_sub_order
    @sub_order = SubOrder.find_by slug: params[:id]
  end

  # def sub_order_params
  #   params.require(:sub_order).permit(:status)
  # end
end
