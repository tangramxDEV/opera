# frozen_string_literal: true

class DoctorsController < ApplicationController
  protect_from_forgery except: %i[index]
  before_action :authenticate_user!

  def index
    @doctors = Doctor.order(name: :asc)

    respond_to do |format|
      format.js { render json: @doctors, status: :ok }
      format.html
    end
  end

  def create
    current_doctor = Doctor.find_by(name: params[:doctor][:name])
    if current_doctor.nil?
      @doctor = Doctor.new doctor_params
      if @doctor.save
        render json: { doctor: @doctor, status: :ok }
      else
        render json: { status: :unprocessable_entity }
      end
    else
      render json: { 
        error: "Parece que ese doctor ya esta registrado", 
        status: :unprocessable_entity 
      }, status: :unprocessable_entity 
    end
  end

  private

  def doctor_params
    params.require(:doctor).permit!
  end
end
