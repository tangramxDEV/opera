# frozen_string_literal: true

class FileUploadersController < ApplicationController
  before_action :authenticate_user!

  def create
    @uploader = FileUploader.new file_uploader_params

    if @uploader.save
      render json: {
        uploader: @uploader,
        url: url_for(@uploader.file),
        status: :ok
      }
    else
      render json: {
        error: @uploader.errors
      }, status: :unprocessable_entity
      end
  end

  private

  def file_uploader_params
    params.require(:file_uploader).permit(:file)
  end
end
