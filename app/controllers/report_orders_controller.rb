# frozen_string_literal: true

class ReportOrdersController < ApplicationController
  protect_from_forgery except: %i[index]
  before_action :authenticate_user!
  before_action :set_report_order, only: %i[show edit update]

  def index
    @doctors = Doctor.all
    @hospitals = Hospital.where(status: 1).order(name: :asc)
    @orders = Order.filter(
      params.slice(:hospital, :doctor, :surgery, :type, :status, :insured)
    ).order(id: :desc)
    # can? :read, @orders
    respond_to do |format|
      format.js { render json: @orders, status: :ok }
      format.html
    end
  end

  def show; end

  def update
    result = ProcessReportOrder.call(
      provider: current_user.providers.first,
      order: @order,
      comment: params[:comment],
      documents: params[:documents],
      materials: params[:materials],
      sub_order_params: sub_order_params,
      current_user: current_user
    )

    if result.success?
      flash[:success] = params[:message] || 'Guardado Exitoso'
      render json: { status: :ok }
    else
      render json: { status: :unprocessable_entity }
    end
  end

  private

  def set_report_order
    @order = Order.find_by slug: params[:id]
  end

  def sub_order_params
    params.require(:sub_order).permit(:status, :coded)
  end
end
