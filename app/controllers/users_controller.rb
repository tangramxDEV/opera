# frozen_string_literal: true

class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: %i[edit update]

  def index
    @users = User.search(params[:search]).includes(%i[roles roles_users]).paginate(page: params[:page])

    authorize! :read, @users
  end

  def new
    @user = User.new
    @providers = Provider.where is_new: false
  end

  def create
    @user = User.new user_params
    @user.save

    if params[:provider]
      ProvidersUser.create(
        user_id: @user.id,
        provider_id: params[:provider][:id]
      )
    end

    params[:permissions]&.map do |permission|
      @user.add_role permission.to_sym
    end
    return redirect_to users_url if @user.save

    render :new
  end

  def edit
    authorize! :edit, @user
  end

  def update
    @user.roles.map do |role|
      @user.remove_role role.name.to_sym
    end

    params[:permissions]&.map do |permission|
      @user.add_role permission.to_sym
    end

    return redirect_to users_url if @user.update user_params

    render :edit
  end

  private

  def set_user
    @user = User.find_by slug: params[:slug]
  end

  def user_params
    params.require(:user).permit!
  end
end
