# frozen_string_literal: true

class OperationalConsultingProvidersController < ApplicationController
  protect_from_forgery except: %i[index show last_ten]
  before_action :authenticate_user!
  before_action :set_sub_order, only: %i[show]
  def index
    # @doctors = Doctor.pluck(:id, :name)
    # @hospitals = Hospital.where(status: 1).order(name: :asc).pluck(:id, :name)

    dictionary = {}
    dictionary['hospital_id'] = params[:hospital] if params[:hospital].present?
    dictionary['doctor_id'] = params[:doctor] if params[:doctor].present?
    dictionary['gnp_invoice'] = params[:invoice] if params[:invoice].present?

    provider = current_user.providers.first
    @sub_orders = provider.sub_orders
                          .includes([
                                      # :documents,
                                      { order: [
                                        # :order_documents,
                                        :hospital,
                                        :doctor
                                        # :sub_orders,
                                        # :order_comments
                                      ] }
                                      # {sub_order_generics: [
                                      #     {sub_order_generals: [
                                      #         {provider_material:[
                                      #             :general_material
                                      #         ]}
                                      #     ]}
                                      # ]}
                                    ])
                          .joins(:order)
                          .where(orders: dictionary)
                          .order(order_id: :desc)

    @sub_orders = @sub_orders.where(created_at: params[:start].to_time..params[:end].to_time) if params[:start].present?

    respond_to do |format|
      format.js { render json: @sub_orders, status: :ok }
      format.html
    end
  end

  def last_ten
    provider = current_user.providers.first
    @orders = provider.sub_orders
                      .includes([
                                  :documents,
                                  { order: %i[
                                    order_documents
                                    hospital
                                    doctor
                                    sub_orders
                                    order_comments
                                  ] },
                                  { sub_order_generics: [
                                    { sub_order_generals: [
                                      { provider_material: [
                                        :general_material
                                      ] }
                                    ] }
                                  ] }
                                ])
                      .joins(:order)
                      .order(id: :desc)

    respond_to do |format|
      format.js { render json: @orders, status: :ok }
      format.html
    end
  end

  def show
    respond_to do |format|
      format.js { render json: @sub_order, status: :ok }
      format.html
    end
  end

  private

  def set_sub_order
    @sub_order = SubOrder.find_by slug: params[:id]
  end
end
