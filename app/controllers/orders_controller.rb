# frozen_string_literal: true

class OrdersController < ApplicationController
  protect_from_forgery except: %i[index show pending]
  before_action :authenticate_user!
  before_action :set_order, only: %i[show edit update]

  def index
    @orders = Order.all

    respond_to do |format|
      format.js { render json: @orders.order(id: :desc), status: :ok }
      format.html
    end
  end

  def pending
    @orders = Order
              .includes(%i[doctor hospital])
              .where(finished: false)
              .paginate(page: params[:page])
              .order(id: :desc)
  end

  def new
    @order = Order.new
    authorize! :new, @order
  end

  def show
    respond_to do |format|
      format.js { render json: @order, status: :ok }
      format.html
    end
  end

  def create
    @order = Order.new order_params
    @order.user = current_user
    @document = @order.documents.new documents_params if params[:document]

    if @order.save
      @order.update(
        cpo: "CPO00000#{@order.id}"
      )
      flash[:success] = 'Solicitud Generada, continúe la captura'
      render json: { order: @order, status: :ok }
    else
      render json: { status: :unprocessable_entity }
    end
  end

  def edit
    authorize! :edit, @order
  end

  def update
    authorize! :update, @order
    @order.update order_params

    result = ProcessOrder.call(
      sub_orders: params[:sub_orders],
      order: @order,
      documents: params[:documents],
      comment: params[:order_comment],
      finished: params[:finished],
      current_user: current_user
    )

    if result.success?
      flash[:success] = params[:message] || 'Solicitud generada correctamente'
      render json: { status: :ok }
    else
      render json: { status: :unprocessable_entity }
    end
  end

  private

  def order_params
    params.require(:order).permit!
  end

  def order_comment_params
    params.require(:order_comment).permit!
  end

  def set_order
    @order = Order.find_by slug: params[:slug]
  end
end
