# frozen_string_literal: true

class NewSuppliesController < ApplicationController
  before_action :authenticate_user!
  protect_from_forgery except: %i[show]
  before_action :set_sub_order, only: %i[show update]
  before_action :set_provider, only: %i[update]

  def index
    @sub_orders = SubOrder.where(status: 'new_supplies')
                          .paginate(page: params[:page])
                          .order(order_id: :desc)
  end

  def show
    respond_to do |format|
      format.js { render json: @sub_order, status: :ok }
      format.html
    end
  end

  def update
    result = ProcessNewSupply.call(
      sub_order: @sub_order,
      sub_order_params: sub_order_params,
      comment: params[:comment],
      documents: params[:documents],
      provider: @provider,
      provider_params: provider_params,
      cost_list: params[:cost_list],
      current_user: current_user
    )

    if result.success?
      flash[:success] = params[:message] || 'Guardado Exitoso'
      render json: { status: :ok }
    else
      render json: { status: :unprocessable_entity }
    end
  end

  private

  def sub_order_params
    params.require(:sub_order).permit(:status, :negotiated_amount)
  end

  def set_sub_order
    @sub_order = SubOrder.find_by slug: params[:id]
  end

  def set_provider
    @provider = Provider.find params[:provider][:provider_id]
  end

  def provider_params
    params.require(:provider).permit(:responsable, :contact_number, :provider_negotiation_type, :affiliate_code)
  end
end
