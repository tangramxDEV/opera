# frozen_string_literal: true

class ProviderMaterialsController < ApplicationController
  protect_from_forgery except: %i[index]
  before_action :authenticate_user!

  def index
    @materials = if params[:provider_id]
                   ProviderMaterial.where(provider_id: params[:provider_id])
                                   .where('created_at < ?', DateTime.new(2019, 11, 30))
                 else
                   ProviderMaterial.all
                 end
    respond_to do |format|
      format.js { render json: @materials, status: :ok }
      format.html
    end
  end

  def create
    @gral = GeneralMaterial.create!(
      description: params[:provider_material][:description],
      code: params[:provider_material][:code]
    )

    @material = ProviderMaterial.create!(
      general_material_id: @gral.id,
      provider_id: current_user.providers.first.id,
      price: params[:provider_material][:price],
      brand: params[:provider_material][:brand]
    )
    render json: @material, serializer: ProviderMaterialSerializer
  end

  private

  def provider_material_params
    params.require(:provider_material).permit!
  end
end
