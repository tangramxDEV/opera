# frozen_string_literal: true

class CptsController < ApplicationController
  protect_from_forgery except: %i[index]
  before_action :authenticate_user!

  def index
    @cpts = Cpt.all
    @cpts = @cpts.where(suffering_type: params[:suffering]) if params[:suffering].present?
    respond_to do |format|
      format.js { render json: @cpts, status: :ok }
      format.html
    end
  end

  def create
    @cpt = Cpt.new cpt_params
    if @cpt.save
      render json: { cpt: @cpt, status: :ok }
    else
      render json: { status: :unprocessable_entity }
    end
  end

  private

  def cpt_params
    params.require(:cpt).permit!
  end
end
