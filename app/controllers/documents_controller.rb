# frozen_string_literal: true

class DocumentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_document, only: %i[destroy]
  def destroy
    @document.destroy
    render json: { status: :ok }
  end

  private

  def set_document
    @document = Document.find params[:id]
  end
end
