# frozen_string_literal: true

class IntelligenceCentersController < ApplicationController
  protect_from_forgery except: %i[index show]
  before_action :authenticate_user!
  before_action :set_sub_order, only: %i[show update]

  def index
    @sub_orders = SubOrder.search(params[:search])
                          .where(status: 'cdi')
                          .includes([{ order: [:doctor] }, :provider])
                          .paginate(page: params[:page])
                          .order(order_id: :desc)
  end

  def show
    authorize! :show, @sub_order
    respond_to do |format|
      format.js { render json: @sub_order, status: :ok }
      format.html
    end
  end

  def update
    authorize! :update, @sub_order

    result = ProcessIntelligenceCenter.call(
      sub_order: @sub_order,
      sub_order_params: sub_order_params,
      comment: params[:comment],
      documents: params[:documents],
      current_user: current_user
    )

    if result.success?
      flash[:success] = params[:message] || 'Guardado Exitoso'
      render json: { status: :ok }
    else
      render json: { status: :unprocessable_entity }
    end
  end

  private

  def set_sub_order
    @sub_order = SubOrder.find_by slug: params[:id]
  end

  def sub_order_params
    params.require(:sub_order).permit(:status)
  end
end
