class CancelationsController < ApplicationController
  protect_from_forgery except: %i[index last_ten]
  before_action :authenticate_user!
  before_action :set_sub_order, only: %i[show update]

  def index
    @sub_orders = SubOrder.search(params[:search])
                    .includes([{ order: %i[doctor hospital] }])
                    .where(status: [
                      'case_management', 'cdi', 'hospital_supervision', 'negotiable-matter','pending', 'supplier_management'
                    ])
                    .paginate(page: params[:page])
                    .order(order_id: :desc)

    respond_to do |format|
      format.js { render json: @sub_orders, status: :ok }
      format.html
    end
  end

  def show
    respond_to do |format|
      format.js { render json: @sub_order, status: :ok }
      format.html
    end
  end

  def update
    @sub_order.update(
      status: params[:sub_order][:status],
      cancelation_motive: params[:sub_order][:cancelation_motive],
      cancellation_comment: params[:sub_order][:cancellation_comment],
      cancelation_date: Time.now
    )

    @sub_order.comments.create(
      comment: params[:comment][:comment],
      user: params[:comment][:user],
      module: params[:comment][:module],
      date: (Time.now - 5*60*60).strftime('%d-%m-%Y %I:%M:%S %p')
    )

    flash[:success] = params[:message]
    render json: { status: :ok }
  end

  private

  def set_sub_order
    @sub_order = SubOrder.find_by slug: params[:id]
  end

  def sub_order_params
    params.require(:sub_order).permit(:status, :cancelation_motive, :cancellation_comment)
  end

end
