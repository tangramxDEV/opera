# frozen_string_literal: true

class GeneralMaterialsController < ApplicationController
  protect_from_forgery except: %i[index]
  before_action :authenticate_user!

  def index
    @general_materials = GeneralMaterial.all
    respond_to do |format|
      format.js { render json: @general_materials, status: :ok }
      format.html
    end
  end
end
