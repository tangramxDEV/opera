# frozen_string_literal: true

class DictumsController < ApplicationController
  before_action :authenticate_user!
  def index; end

  def show
    @order = Order.find params[:id]
  end
end
