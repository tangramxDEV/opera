# frozen_string_literal: true

class MaterialNegotiationsController < ApplicationController
  protect_from_forgery except: %i[show index]
  before_action :authenticate_user!
  before_action :set_sub_order, only: %i[show update]

  def index
    @sub_orders = SubOrder.includes([{order: [
                            :hospital,
                            :doctor
                          ]}])
                          .where(status: 'negotiable-matter')
                          .paginate(page: params[:page])
                          .order(order_id: :desc)
  end

  def show
    respond_to do |format|
      format.js { render json: @sub_order, status: :ok }
      format.html
    end
  end

  def update
    result = ProcessMaterialNegotiation.call(
      sub_order: @sub_order,
      sub_order_params: sub_order_params,
      comment: params[:comment],
      documents: params[:documents],
      current_user: current_user
    )

    if result.success?
      flash[:success] = params[:message] || 'Guardado Exitoso'
      render json: { status: :ok }
    else
      render json: { status: :unprocessable_entity }
    end
  end

  private

  def set_order
    @order = Order.find params[:id]
  end

  def set_sub_order
    @sub_order = SubOrder.includes([
                                     { sub_order_generics: [{
                                       sub_order_generals: [{
                                         provider_material: [:general_material]
                                       }]
                                     }] }
                                   ]).find_by slug: params[:id]
  end

  def sub_order_params
    params.require(:sub_order).permit(:status, :coded)
  end
end
