# frozen_string_literal: true

class HospitalsController < ApplicationController
  protect_from_forgery except: %i[index]
  before_action :authenticate_user!
  def index
    @hospitals = Hospital.where(status: 1).order(name: :asc)

    respond_to do |format|
      format.js { render json: @hospitals, status: :ok }
      format.html
    end
  end
end
