# frozen_string_literal: true

class HospitalSupervisionsController < ApplicationController
  protect_from_forgery except: %i[index show]
  before_action :authenticate_user!
  before_action :set_sub_order, only: %i[show update]

  def index
    hospital_charges = current_user.hospital_charges.map(&:hospital_id)
    hospitals = Hospital.find hospital_charges
    orders = Order.where(hospital_id: hospitals).map(&:id)

    @sub_orders = SubOrder.search(params[:search])
                  .includes(%i[order provider])
                  .where(status: 'hospital_supervision')
                  .where(order_id: orders)
                  .paginate(page: params[:page])
                  .order(order_id: :desc)

    respond_to do |format|
      format.js 
      format.html
    end
    # { render json: {html: render_to_string(partial: 'hospital_supervisions/hospital_supervision', collection: @sub_orders)}, status: :ok }
  end

  def show
    # authorize! :show, @sub_order
    respond_to do |format|
      format.js { render json: @sub_order, status: :ok }
      format.html
    end
  end

  def update
    authorize! :update, @sub_order
    # Update before for save hospital cost
    @sub_order.order.update order_params

    result = ProcessHospitalSupervision.call(
      sub_order: @sub_order,
      sub_order_params: sub_order_params,
      comment: params[:comment],
      documents: params[:documents],
      generals: params[:supervision],
      current_user: current_user
    )

    if result.success?
      flash[:success] = params[:message] || 'Guardado exitoso'
      render json: { status: :ok }
    else
      render json: { status: :unprocessable_entity }
    end
  end

  private

  def set_sub_order
    @sub_order = SubOrder.find_by slug: params[:id]
  end

  def sub_order_params
    params.require(:sub_order).permit(:status)
  end

  def order_params
    params.require(:order).permit(:departure_date, :deductible, :hospital_coinsurance, :coinsurance_money, :coinsurance_percentage, :medical_fees, :episode, :hospital_expense, :wf_invoice, :evo_invoice)
  end
end
