# frozen_string_literal: true

class ProvidersController < ApplicationController
  protect_from_forgery except: %i[index]
  before_action :authenticate_user!
  before_action :set_provider, only: %i[edit update]

  def index
    @providers = if params[:selected]
                   Provider.where(id: params[:selected].split(',').collect(&:to_i))
                 else
                   Provider.where(is_new: false)
                 end
    respond_to do |format|
      format.js { render json: @providers, status: :ok }
      format.html
    end
  end

  def new
    @provider = Provider.new
    authorize! :new, @provider
  end

  def create
    # @provider = current_user.create_provider provider_params
    @provider = Provider.new provider_params

    if @provider.save
      redirect_to authenticated_root_url
    else
      render :new
    end
  end

  def edit
    authorize! :edit, @provider
  end

  def update
    authorize! :update, @provider
    if @provider.update provider_params
      redirect_to authenticated_root_url
    else
      render :edit
    end
  end

  def on_hold
    @provider = Provider.new provider_params
    if @provider.save
      render json: { provider: @provider, status: :ok }
    else
      render json: { status: :unprocessable_entity, error: @provider.errors }
    end
  end

  private

  def set_provider
    @provider = Provider.find_by slug: params[:slug]
  end

  def provider_params
    params.require(:provider).permit!
  end
end
