# frozen_string_literal: true

class SufferingsController < ApplicationController
  protect_from_forgery except: %i[index]
  before_action :authenticate_user!
  before_action :set_hospital, only: %i[index]

  def index
    @sufferings = @hospital.sufferings
    respond_to do |format|
      format.js { render json: @sufferings, status: :ok }
      format.html
    end
  end

  private

  def set_hospital
    @hospital = Hospital.find params[:hospital_id]
  end
end
