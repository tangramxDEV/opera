# frozen_string_literal: true

class HospitalChargesController < ApplicationController
  protect_from_forgery except: %i[index]
  before_action :authenticate_user!
  before_action :set_user, only: %i[index]
  before_action :set_hospital, only: %i[destroy]

  def index
    hospital_charges = @user.hospital_charges.map(&:hospital_id)
    @hospitals = Hospital.find hospital_charges
    render json: {
      hospitals: @hospitals || [],
      status: :ok
    }
  end

  def create
    @charge = HospitalCharge.new hospital_charge_params
    if @charge.save
      render json: { charge: @charge, status: :ok }
    else
      render json: { status: :unprocessable_entity }
    end
  end

  def destroy
    if params[:user]
      @hospital_charge = HospitalCharge.where(
        user_id: params[:user][:user_id],
        hospital_id: @hospital.id
      ).first
      @hospital_charge.destroy
    end

    render json: { status: :ok }
  end

  private

  def set_user
    @user = User.find params[:user_id]
  end

  def set_hospital
    @hospital = Hospital.find params[:id]
  end

  def hospital_charge_params
    params.require(:hospital_charge).permit!
  end
end
