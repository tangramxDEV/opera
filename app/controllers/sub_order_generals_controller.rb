# frozen_string_literal: true

class SubOrderGeneralsController < ApplicationController
  before_action :authenticate_user!

  def destroy
    @sub_order_general = SubOrderGeneral.find params[:id]
    @sub_order_general.destroy
    render json: { status: :ok }
  end
end
