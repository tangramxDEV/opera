# frozen_string_literal: true

class OveruseController < ApplicationController
  protect_from_forgery except: %i[index show sub_orders]
  before_action :authenticate_user!
  before_action :set_order, only: %i[sub_orders]
  before_action :set_sub_order, only: %i[show update]

  def index
    @sub_orders = SubOrder.where(overload: true)
                          .where(skip_overload: false)
                          .paginate(page: params[:page])
                          .order(order_id: :desc)
  end

  def show
    authorize! :show, @sub_order
    respond_to do |format|
      format.js { render json: @sub_order, status: :ok }
      format.html
    end
  end

  def update
    @sub_order.update(skip_overload: true)
    flash[:success] = 'Revisado exitoso'
    render json: { status: :ok }
  end

  def sub_orders
    respond_to do |format|
      format.js { render json: @order.sub_orders, status: :ok }
      format.html
    end
  end

  private

  def set_order
    @order = Order.find params[:id]
  end

  def set_sub_order
    @sub_order = SubOrder.find_by slug: params[:id]
  end
end
