# frozen_string_literal: true

class CaseManagementsController < ApplicationController
  protect_from_forgery except: %i[index show]
  before_action :authenticate_user!
  before_action :set_sub_order, only: %i[show update]

  def index
    @sub_orders = SubOrder.search(params[:search])
                  .includes([{ order: %i[
                              doctor
                              hospital
                            ] }])
                  .where(status: 'case_managment')
                  .paginate(page: params[:page])
                  .order(order_id: :desc)
  end

  def show
    authorize! :show, @sub_order
    respond_to do |format|
      format.js { render json: @sub_order, status: :ok }
      format.html
    end
  end

  def update
    authorize! :update, @sub_order
    @sub_order.order.update order_params

    result = ProcessCaseManagement.call(
      sub_order: @sub_order,
      sub_order_params: sub_order_params,
      comment: params[:comment],
      documents: params[:documents],
      generals: params[:supervision],
      current_user: current_user
    )

    if result.success?
      flash[:success] = params[:message] || 'Guardado Exitoso'
      render json: { status: :ok }
    else
      render json: { errors: result.message, status: :unprocessable_entity }
    end
  end

  private

  def set_sub_order
    @sub_order = SubOrder.find_by slug: params[:id]
  end

  def order_params
    params.require(:order).permit(:departure_date, :deductible, :hospital_coinsurance, :coinsurance_money, :coinsurance_percentage, :medical_fees, :episode, :hospital_expense, :wf_invoice, :evo_invoice)
  end

  def sub_order_params
    params.require(:sub_order).permit(:status)
  end
end
