# frozen_string_literal: true

class ProviderQualitiesController < ApplicationController
  before_action :set_sub_order, only: %i[show]
  def index
    provider = current_user.providers.first
    @sub_orders = provider.sub_orders
                          .includes([
                                      { order: %i[hospital doctor] }
                                    ])
                          .where(status: 'quality')
                          .order(order_id: :desc)
    respond_to do |format|
      format.js { render json: @sub_orders, status: :ok }
      format.html
    end
  end

  def show; end

  private

  def set_sub_order
    @sub_order = SubOrder.find_by slug: params[:id]
  end
end
