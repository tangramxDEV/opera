# frozen_string_literal: true

class IcdsController < ApplicationController
  protect_from_forgery except: %i[index]
  before_action :authenticate_user!

  def index
    @icds = Icd.all
    @icds = @icds.where(suffering_type: params[:suffering]) if params[:suffering].present?
    respond_to do |format|
      format.js { render json: @icds, status: :ok }
      format.html
    end
  end

  def create
    @icd = Icd.new icd_params
    if @icd.save
      render json: { icd: @icd, status: :ok }
    else
      render json: { status: :unprocessable_entity }
    end
  end

  private

  def icd_params
    params.require(:icd).permit!
  end
end
