# frozen_string_literal: true

class ActiveStorage::BlobsController < ActiveStorage::BaseController
  authorize_resource
  include ActiveStorage::SetBlob

  def show
    authorize! :show, @blob
    redirect_to @blob.service_url(disposition: params[:disposition])
  end
end
