# frozen_string_literal: true

class ReportsController < ApplicationController
  before_action :authenticate_user!

  def global
    @sub_orders = SubOrder.includes([
                                      { order: %i[
                                        doctor
                                        hospital
                                      ] },
                                      :provider
                                    ])
    authorize! :read, @sub_orders
    respond_to do |format|
      format.xlsx do
        response.header[
          'Content-Disposition'
          ] = 'attachment; filename="solicitudes.xlsx"'
      end
      format.html
    end
  end
end
