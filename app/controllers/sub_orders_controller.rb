# frozen_string_literal: true

class SubOrdersController < ApplicationController
  protect_from_forgery except: %i[index show get_sub_order]
  before_action :authenticate_user!
  before_action :set_sub_order, only: %i[show edit update]

  def index
    if params[:order_slug].present?
      order = Order.find_by slug: params[:order_slug]
      @sub_orders = order.sub_orders
      respond_to do |format|
        format.js { render json: @sub_orders, status: :ok }
        format.html
      end
    end
  end

  def show
    respond_to do |format|
      format.js { render json: @sub_order, status: :ok }
      format.html
    end
  end

  def get_sub_order
    @sub_order = SubOrder.find params[:id]
    respond_to do |format|
      format.js { render json: @sub_order, status: :ok }
      format.html
    end
  end

  def update
    result = ProcessSubOrder.call(
      sub_order: @sub_order,
      sub_order_params: sub_order_params,
      comment: params[:comment],
      documents: params[:documents],
      current_user: current_user
    )

    if result.success?
      render json: {  sub_order: @sub_order, status: :ok }
    else
      render json: { status: :unprocessable_entity }
    end
  end

  private

  def sub_order_params
    params.require(:sub_order).permit(:status, :invoice, :letter)
  end

  def set_sub_order
    @sub_order = SubOrder.find_by slug: params[:slug]
  end
end
