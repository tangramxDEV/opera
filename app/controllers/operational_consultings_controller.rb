# frozen_string_literal: true

class OperationalConsultingsController < ApplicationController
  protect_from_forgery except: %i[index last_ten]
  before_action :authenticate_user!
  before_action :set_order, only: %i[show]
  def index
    @orders = Order.filter(
                      params.slice(:hospital, :doctor, :invoice, :claim, :insured, :cpo)
                  )
                  .where(finished: true)
                  .order(id: :desc)
    
    @orders = @orders.where(created_at: params[:start].to_time..params[:end].to_time) if params[:start].present?

    respond_to do |format|
      format.js { render json: @orders, status: :ok }
      format.html
    end
  end

  def last_ten
    @orders = Order
              .includes(%i[
                          sub_orders
                          order_documents
                          order_comments
                          hospital
                          doctor
                        ])
              .where(finished: true)
              .order(id: :desc)

    respond_to do |format|
      format.js { render json: @orders, status: :ok }
      format.html
    end
  end

  def show; end

  private

  def set_order
    @order = Order.find_by slug: params[:id]
  end
end
