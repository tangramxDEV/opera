# frozen_string_literal: true

class GenericMaterialsController < ApplicationController
  protect_from_forgery except: %i[index]
  before_action :authenticate_user!

  def index
    @generic_materials = if params[:suffering]
                           GenericMaterial.where(suffering_type: params[:suffering])
                         else
                           GenericMaterial.all
                         end
    respond_to do |format|
      format.js { render json: @generic_materials, status: :ok }
      format.html
    end
  end
end
