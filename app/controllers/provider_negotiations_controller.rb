# frozen_string_literal: true

class ProviderNegotiationsController < ApplicationController
  protect_from_forgery except: %i[index show]
  before_action :authenticate_user!
  before_action :set_sub_order, only: %i[show update]
  before_action :set_provider, only: %i[update]

  def index
    @sub_orders = SubOrder.where(status: 'supplier_management')
                          .includes([{ order: %i[
                                      doctor
                                      hospital
                                    ] }])
                          .paginate(page: params[:page])
                          .order(order_id: :desc)
  end

  def show
    respond_to do |format|
      format.js { render json: @sub_order, status: :ok }
      format.html
    end
  end

  def update
    result = ProcessProviderNegotiation.call(
      sub_order: @sub_order,
      sub_order_params: sub_order_params,
      comment: params[:comment],
      documents: params[:documents],
      provider: @provider,
      provider_params: provider_params,
      materials: params[:materials],
      current_user: current_user
    )

    if result.success?
      flash[:success] = params[:message] || 'Guardado Exitoso'
      render json: { status: :ok }
    else
      render json: { status: :unprocessable_entity }
    end
  end

  private

  def set_order
    @order = Order.find params[:id]
  end

  def set_sub_order
    @sub_order = SubOrder.find_by slug: params[:id]
  end

  def set_provider
    @provider = Provider.find params[:provider][:provider_id]
  end

  def provider_params
    params.require(:provider).permit(:status, :responsable, :contact_number, :provider_negotiation_type, :affiliate_code)
  end

  def sub_order_params
    params.require(:sub_order).permit(:status, :coded)
  end
end
