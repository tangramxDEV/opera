# frozen_string_literal: true

class HospitalSupervisionProviderOrdersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_sub_order, only: %i[show update]

  def index
    provider = current_user.providers.first
    @sub_orders = provider.sub_orders
                          .includes([{ order: %i[doctor hospital] }])
                          .where(status: 'hospital_supervision')
                          .where(coded: false)
                          .paginate(page: params[:page])
                          .order(order_id: :desc)

    respond_to do |format|
      format.js { render json: @sub_orders, status: :ok }
      format.html
    end
  end

  def show; end

  def update
    @sub_order.order.update order_params

    result = ProcessHospitalSupervisionProvider.call(
      sub_order: @sub_order,
      sub_order_params: sub_order_params,
      comment: params[:comment],
      documents: params[:documents],
      materials: params[:materials],
      current_user: current_user
    )

    if result.success?
      flash[:success] = params[:message] || 'Guardado Exitoso'
      render json: { status: :ok }
    else
      render json: { errors: result.message, status: :unprocessable_entity }
    end
  end

  private

  def set_sub_order
    @sub_order = SubOrder.find_by slug: params[:id]
  end

  def sub_order_params
    params.require(:sub_order).permit(:status, :coded)
  end

  def order_params
    params.require(:order).permit(:surgical_event_date)
  end
end
