let date = new Date();

export const current_date_time = () => {
   return `
        ${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()} 
        ${date.toLocaleTimeString()}
    `
}

export const current_date = () => {
    return `
        ${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()} 
    `
}

export const add_days_from_YMD = (string_date, days = 1) => {
    let dividedDate = string_date.split('-')// [0] Year [1] Month [2] Day
    let dateResultant = new Date(dividedDate[0],dividedDate[1],dividedDate[2])
    
    dateResultant.setDate(dateResultant.getDate() + days)

    return `${dateResultant.getDate()}-${dateResultant.getMonth()}-${dateResultant.getFullYear()}`
}

export const YMD_to_DMY_format = (string_date) => {
    let dividedDate = string_date.split('-')// [0] Year [1] Month [2] Day
    return `${dividedDate[2]}-${dividedDate[1]}-${dividedDate[0]}`
}

