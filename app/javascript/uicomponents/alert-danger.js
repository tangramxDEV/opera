import React from 'react'

const AlertDanger = ({text, visibility}) => {
    return <div style={{'visibility': visibility}} className="alert alert-danger" role="alert">
        {text}
    </div>
}

export default AlertDanger