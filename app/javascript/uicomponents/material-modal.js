import React,{
    useEffect,
    useState
} from 'react'
import { useForm } from 'react-hook-form'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'

const MaterialModal = ({
    form, 
    handleChange, 
    onSubmit,
    toggle,
    modal
}) => {
    const { register, handleSubmit, errors } = useForm()
    return <React.Fragment>
    <Modal isOpen={modal} toggle={toggle} >
        <ModalHeader toggle={toggle}>
           Nuevo Insumo
        </ModalHeader>
        <ModalBody>
        <div className="form-group">
            <label className="col-form-label">Descripcion:</label>
            <input 
                type="text" 
                className="form-control"
                name="description"
                value={form.description}
                onChange={handleChange}
                className={errors.description ? "form-control is-invalid" : "form-control"}
                ref={register({required: true})}
            />
            {  errors.description && <div className="invalid-feedback">
                    La descripción es requerida.
                </div>
            }
        </div>
        <div className="form-group">
            <label className="col-form-label">Código:</label>
            <input 
                type="text" 
                className="form-control"
                name="code"
                value={form.code}
                onChange={handleChange}
                className={errors.code ? "form-control is-invalid" : "form-control"}
                ref={register({required: true})}
            />
            {  errors.code && <div className="invalid-feedback">
                    El código es requerido.
                </div>
            }
        </div>
        <div className="form-group">
            <label className="col-form-label">Precio:</label>
            <input 
                type="number" 
                className="form-control"
                name="price"
                step="any"
                value={form.price}
                onChange={ e => {
                    const re = /\d+(\.\d{1,2})?/
                    if (re.test(e.target.value) && e.target.value > 0) {
                        handleChange(e)
                    }
                }}
                className={errors.price ? "form-control is-invalid" : "form-control"}
                ref={register({required: true})}
            />
            {  errors.price && <div className="invalid-feedback">
                    El precio es requerido.
                </div>
            }
        </div>
        <div className="form-group">
            <label className="col-form-label">Marca:</label>
            <input 
                type="text" 
                className="form-control" 
                name="brand"
                value={form.brand}
                onChange={handleChange}
                className={errors.brand ? "form-control is-invalid" : "form-control"}
                ref={register({required: true})}
            />
            {  errors.brand && <div className="invalid-feedback">
                    La marca es requerida.
                </div>
            }
        </div>
        </ModalBody>
        <ModalFooter>
            <button 
                type="button" 
                className="btn btn-primary"
                onClick={handleSubmit(onSubmit)}
            >
                Registrar
            </button>
        </ModalFooter>
    </Modal>

        <button 
            type="button" 
            className="btn btn-primary btn-lg btn-block"
            onClick={toggle}
        >
            Agregar Insumo
        </button>
    </React.Fragment>
}

export default MaterialModal