import React from 'react'

const InputSearcher = ({placeholder, query, setQuery}) => (
    
        <input 
            type="text" 
            className="form-control"
            placeholder={placeholder}
            value={query}
            onChange={(e) => {
                setQuery(e.target.value)
            }}
        />
    
)

export default InputSearcher