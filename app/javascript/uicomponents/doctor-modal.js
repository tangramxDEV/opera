import React from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import { useForm } from 'react-hook-form'
const ProviderModal = ({
    form, 
    handleChange, 
    onSubmit, 
    toggle,
    modal
}) => {
    const { register, handleSubmit, errors } = useForm()
    return <React.Fragment>
        <Modal isOpen={modal} toggle={toggle} >
            <ModalHeader toggle={toggle}>
                Nuevo Medico
            </ModalHeader>
            <ModalBody>
                <form>
                     <div className="form-group">
                        <label>Apellido Paterno:</label>
                        <input 
                            type="text" 
                            className={errors.last_name ? "form-control is-invalid" : "form-control"}
                            name="last_name"
                            value={form.last_name}
                            onChange={handleChange}
                            ref={register({required: true})}
                        />
                        {  errors.last_name && <div class="invalid-feedback">
                            El apellido paterno es necesario.
                        </div>
                        }
                    </div>
                    <div className="form-group">
                        <label>Apellido Materno:</label>
                        <input 
                            type="text" 
                            className={errors.mother_last_name ? "form-control is-invalid" : "form-control"}
                            name="mother_last_name"
                            value={form.mother_last_name}
                            onChange={handleChange}
                            ref={register({required: true})}
                        />
                        {  errors.mother_last_name && <div class="invalid-feedback">
                            El apellido materno es necesario.
                        </div>
                        }
                    </div>
                    <div className="form-group">
                        <label>Nombre(s):</label>
                        <input 
                            type="text" 
                            className={errors.name ? "form-control is-invalid" : "form-control"}
                            name="name"
                            value={form.name}
                            onChange={handleChange}
                            ref={register({required: true})}
                        />
                        {  errors.name && <div class="invalid-feedback">
                            El nombre es necesario.
                        </div>
                        }
                    </div>

                    <div className="form-group">
                        <label>Epecialidad</label>
                        <input 
                            type="text" 
                            className={errors.speciality ? "form-control is-invalid" : "form-control"}
                            name="speciality"
                            value={form.speciality}
                            onChange={handleChange}
                            ref={register({required: true})}
                        />
                        {  errors.speciality && <div class="invalid-feedback">
                            La especialidad es necesaria.
                        </div>
                        }
                    </div>
                    <div className="form-group">
                        <label>Ciudad</label>
                        <input 
                            type="text" 
                            className={errors.city ? "form-control is-invalid" : "form-control"}                            name="city"
                            value={form.city}
                            onChange={handleChange}
                            ref={register({required: true})}
                        />
                        {  errors.city && <div class="invalid-feedback">
                            La ciudad es necesaria.
                        </div>
                        }
                    </div>
                    <div className="form-group">
                        <label>Dirección</label>
                        <input 
                            type="text" 
                            className="form-control"
                            name="address"
                            value={form.address}
                            onChange={handleChange}
                        />
                    </div>
                    <hr/>
                    <div className="form-group">
                        <label>Email</label>
                        <input 
                            type="text" 
                            className="form-control"
                            name="email"
                            value={form.email}
                            onChange={handleChange}
                        />
                    </div>

                    <div className="form-group">
                        <label>Telefono</label>
                        <input 
                            type="text" 
                            className="form-control"
                            name="phone"
                            value={form.phone}
                            onChange={handleChange}
                        />
                    </div>
                </form>
            </ModalBody>
            <ModalFooter>
                <button 
                    type="button" 
                    className="btn btn-primary"
                    onClick={handleSubmit(onSubmit)}
                >
                    Registrar
                </button>
            </ModalFooter>
        </Modal>

        <button 
            type="button" 
            className="btn btn-outline-primary float-right"
            onClick={toggle}
        >
        <i className="fas fa-plus"></i>
        </button>
    </React.Fragment>
}

export default ProviderModal