import React, {
    useState
} from 'react'

import useFormatter from '../hooks/useFormatter'
import MaskedInput from 'react-text-mask'
import createNumberMask from 'text-mask-addons/dist/createNumberMask'
import OrderForm from './order_resume/order-form'

const OrderResume = ({
    order, 
    hospital, 
    doctor, 
    title, 
    expense = [],
    invisibles = [], 
    editable = [], 
    form, 
    handleChange,
    provider = {},
    subOrder = {},
    legends = false,
    coinsurance = false,
    module = '',
    departureDate = '',
    handleDepartureDate,
    surgicalDate='',
    handleSurgicalDate
}) => {


    return <React.Fragment>
        <div className="jumbotron">
            <h1 className="display-6">{title}</h1>
            <h2 className="display-10">{subOrder.cpo}</h2>
            <hr className="my-4"/>
            <OrderForm
                subOrder={subOrder}
                coinsurance={coinsurance}
                order={order}
                hospital={hospital}
                doctor={doctor}
                legends={legends}
                form={form}
                editable={editable}
                expense={expense}
                onChange={handleChange}
                invisibles={invisibles}
                module={module}
                departureDate = {departureDate}
                handleDepartureDate = {handleDepartureDate}
                provider={provider}
                surgicalDate={surgicalDate}
                handleSurgicalDate={handleSurgicalDate}
            />
        </div>
    </React.Fragment>
}

export default OrderResume