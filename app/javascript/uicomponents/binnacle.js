import React from 'react'
import { current_date_time } from '../functionalities/dates'

const Binnacle = ({comment, handleComment, comments = [], username = '', module = '', only_view = false, provider = false }) => {
    let today = new Date();

    const validateOnlyView = () => {
        if(!only_view){
           return <input 
                type="text" 
                className="form-control" 
                name="comment"
                value={comment}
                onChange={handleComment}
            />
        }
    }

    const showCurrentWritedComment = () => {
        if(!only_view){
            return <tr>
            <td>{ username }</td>
            <td>{ module }</td>
            {/* <td>{ current_date_time() }</td> */}
            <td></td>
            <td>{comment}</td>
        </tr>
         }
    }

    const showCorrespondingComments = () => {
        if(provider){ 
            return showProviderComments()
        } else {
            return showAllComments()
        }
    }

    const showProviderComments = () => (
        comments.map((comment, index) => {
            if(username === comment.user){
                return <tr key={index}>
                    <td>{comment.user}</td>
                    <td>{comment.module}</td>
                    <td>{comment.date}</td>
                    <td>{comment.comment}</td>
                </tr>
            }
        }) 
    )

    const showAllComments = () => (
        comments.map((comment, index) => (
            <tr key={index}>
                <td>{comment.user}</td>
                <td>{comment.module}</td>
                <td>{comment.date}</td>
                <td>{comment.comment}</td>
            </tr>
        )) 
    )
    
    return <React.Fragment>
        <h3 className="text-center">Bitacora de eventos registrados</h3>
        <br/>
            { validateOnlyView() }
        <br/>
        <table className="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">USUARIO</th>
                    <th scope="col">MODULO</th>
                    <th scope="col">FECHA</th>
                    <th scope="col">COMENTARIOS</th>
                </tr>
            </thead>
            <tbody>
                { showCorrespondingComments() }
                { showCurrentWritedComment() }
            </tbody>
        </table>
        <br/>
    </React.Fragment>

}


export default Binnacle