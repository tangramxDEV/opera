import React from 'react'

const FilteredProviders = ({filteredProviders, handleClick}) => (
    <div style={{'maxHeight':'350px'}} className="pre-scrollable accordion" id="accordionExample">
        { filteredProviders.map((provider) => (
            <div className="card" key={provider.id}>
                <div className="card-header" id="headingOne">
                    <h2 className="mb-0">
                        <button className="btn btn-link" type="button">
                            {provider.name}
                        </button>
                        <button 
                            type="button" 
                            className="btn btn-outline-primary float-right"
                            onClick={()=>{handleClick(provider)}}
                        >
                            <i className="fas fa-plus"></i>
                        </button>
                    </h2>
                </div>
            </div>
        )) }
    </div>
)

export default FilteredProviders