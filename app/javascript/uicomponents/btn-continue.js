import React from 'react'

const BtnContinue = ({manageable, handleSubmit}) => {
    if(manageable){
        return <button onClick={handleSubmit} type="button" className="btn btn-success float-right space-r">
            Continuar
        </button>
    } else {
        return <React.Fragment></React.Fragment>
    }
}



export default BtnContinue