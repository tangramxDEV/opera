import React from 'react'

const loader = ({visibility}) => {
    let visible = 'hidden'

    if(visibility)
        visible = 'visible'

    return <React.Fragment>
        <div style={{visibility: visible}} className="d-flex justify-content-center">
            <div style={{width: '3rem', height: '3rem'}} className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    </React.Fragment>
}

export default loader