import React,{
    useState
} from 'react'
import { URL } from '../settings/configs'
import { current_date_time } from '../functionalities/dates'
import useHttp from '../hooks/useHttp'

const DocumentsContainer = ({
    selectDocument, 
    handleSelectDocumentChange, 
    uploadBtn, 
    loader, 
    setLoader, 
    files, 
    setFiles,
    newFiles, 
    setNewFiles, 
    username,
    options = [],
    authenticity_token,
    only_view = false,
    provider = false,
    module
}) => {
    const { post, destroy } = useHttp()
    const [isMaxSize, setIsMaxSize] = useState(false)
    const [error, setError] = useState(false)
    

    const handleUploadFile = async (e) => {
        let file = fileInput.files[0]
        setIsMaxSize(false)
        if(file.size/1024/1024 <= 5){
          const formData = new FormData()
          formData.append('file_uploader[file]', file)
          formData.append('authenticity_token', authenticity_token)
          setLoader('visible')
        
          try {
            let config = {
              method: 'POST',
              body: formData,
              contentType: false,
              processData: false
            }
            if(selectDocument){
                setError(false)
                await fetch(`${URL}/file_uploaders`, config).then(async (res) => {
                    let data = await res.json()
    
                    if(data.status != 'unprocessable_entity'){
                        let file = {
                            stored_at: current_date_time(),
                            preview: data.url,
                            module: module,
                            document_type: selectDocument,
                            user: username,
                            recently: true
                        }                
                        setLoader('hidden')
                        setFiles([
                            ...files,
                            file
                        ])
                        setNewFiles([
                            ...newFiles,
                            file
                        ])
                        document.getElementById("fileInput").value = "";
                    }
    
                    setLoader('hidden')
                })
            } else {
                setError(true)
                setLoader('hidden')
            }
          } catch (err) {
            setError(false)
            setLoader('hidden')
        }
      } else {
        setIsMaxSize(true)
      }
    }

    const removeFileFromCollection = (fileToRemove) => {
        setFiles(files.filter((file) => (
            file !== fileToRemove
        )))
    }
    const handleRemoveLocalFile = (fileToRemove) => {
        removeFileFromCollection(fileToRemove)
    }
    const handleRemoveCloudFile = async (fileToRemove) => {
        let data = { authenticity_token }
        let res = await destroy(`documents/${fileToRemove.id}`, data)
        res
          .json()
          .then(() => removeFileFromCollection(fileToRemove))
          .catch(err => err)
    }
    const isSafe = (url) => {
        if (url.protocol === 'javascript:'){
            return false
        } else {
            return true
        }
    }

    const validateFileEdition = (file) => {
        if(!file.module){
            file.module = 'Proveedor Reporte Hospitalario'
        }

        if(username == file.user && file.module == module){
            return <React.Fragment>
                <a
                    className="space-l"
                    style={{color: 'red'}}
                    onClick={(e) => {
                        e.preventDefault()
                        if(file.recently){
                            handleRemoveLocalFile(file)
                        } else {
                            handleRemoveCloudFile(file)
                        }
                    }}
                    href="#"
                >
                    <i className="fas fa-trash"></i>
                </a>
            </React.Fragment>
        }
    }

    const validateView = () => {
        if(!only_view){
            return <div className="row">
            <div className="col sm-4">
                <div className="form-group">
                    <label htmlFor="exampleFormControlSelect1">Tipo de documento</label>
                    <select 
                        className="form-control" 
                        id="exampleFormControlSelect1"
                        name={selectDocument}
                        onChange={handleSelectDocumentChange}
                    >
                        <option disabled selected value> -- selecciona una opción -- </option>
                        { options.map((option, index) => (
                            <option key={index}>{option}</option>
                        ))}
                        
                    </select>
                </div>
            </div>
            <div className="col sm-4">
                <div className="form-group">
                    <input
                        accept="application/pdf"
                        type="file" 
                        className="space-t form-control-file" 
                        id="fileInput" 
                    />
                </div>
            </div>
            <div className="col sm-4 space-t-x">
                <button 
                    className="btn btn-outline-primary"
                    onClick={handleUploadFile}
                    style={{visibility: uploadBtn}}
                >
                    Subir
                </button>

                <div 
                    className="space-l spinner-border text-primary" 
                    role="status"
                    style={{visibility: loader}}
                >
                    <span className="sr-only">Loading...</span>
                </div>
            </div>
        </div>
        }
    }

    return <React.Fragment>
    <div className="alert alert-danger" role="alert" style={{visibility: isMaxSize ? 'visible' : 'hidden'}}>
      'El peso del archivo excede el limite'
    </div>
    <div className="alert alert-danger" role="alert" style={{visibility: error ? 'visible' : 'hidden'}}>
      Debes seleccionar un tipo de documento
    </div>
    <h3 className="text-center">Documentos</h3>
    
        { validateView() }

        <table className="table table-bordered">
        <thead>
            <tr>
                <th scope="col">DOCUMENTO</th>
                <th scope="col">USUARIO</th>
                <th scope="col">FECHA</th>
                <th scope="col">OPCIONES</th>
            </tr>
        </thead>
        <tbody>
        { files.map((file, index) => {
            if(provider){
                if(file.user === username){
                    return <tr key={index}>
                        <td>{ file.document_type }</td>
                        <td>{ file.user }</td>
                        <td>{ file.stored_at }</td>
                        <td>
                            <a
                                onClick={(e) => {
                                    e.preventDefault()
                                    if(isSafe(file.preview)){
                                        window.open(`${file.preview}`)
                                    }
                                }}
                                href="#"
                            >
                                <i className="fas fa-eye"></i>
                                
                            </a>
                            { validateFileEdition(file) }
                        </td>
                    </tr>
                }
            } else {
                return <tr key={index}>
                    <td>{ file.document_type }</td>
                    <td>{ file.user }</td>
                    <td>{ file.stored_at }</td>
                    <td>
                        <a
                            onClick={(e) => {
                                e.preventDefault()
                                if(isSafe(file.preview)){
                                    window.open(`${file.preview}`)
                                }
                            }}
                            href="#"
                        >
                            <i className="fas fa-eye"></i>
                            
                        </a>
                        { validateFileEdition(file) }
                    </td>
                </tr>
            }

        })}
        </tbody>
    </table>
    </React.Fragment>
}

export default DocumentsContainer
