import React,{
  Fragment,
  useState
} from 'react'
import useLegends from '../../hooks/useLegends'
import MaskedInput from 'react-text-mask'
import createNumberMask from 'text-mask-addons/dist/createNumberMask'
import useFormatter from '../../hooks/useFormatter'
import DatePicker, { registerLocale } from 'react-datepicker'
import es from "date-fns/locale/es"
registerLocale("es", es)

const OrderForm = ({
  order, hospital, doctor, legends, form, editable, expense, onChange, invisibles, module,
  departureDate, handleDepartureDate, provider = {}, subOrder = {}, surgicalDate,
  handleSurgicalDate, coinsurance = false
}) => {
  const { buildLegend } = useLegends()
  const { formatMoney } = useFormatter()

  const containHospitalExpense = () => order.hospital_expense > 0
  
  var minDate = new Date()
  minDate.setHours(minDate.getHours() - 5)

  const showLegends = () => {
    if(legends){
        return <h5>
            COASEGURO: 
            <p className="badge badge-primary text-wrap">
                { buildLegend(provider, order, subOrder, hospital) }
            </p>
        </h5>
    }
  }

  const showDateByOrderType = () => {
    if(order.order_type == 'REPORTE HOSPITALARIO'){
      return !invisibles.includes('surgery_date') && <div className="form-group">
          <label>Fecha de cirugia</label>
          <input 
              name='surgery_date'
              onChange={onChange}
              placeholder={form['surgery_date'] || order['surgery_date']}
              disabled={!editable.includes('surgery_date') || containHospitalExpense() && expense.includes('surgery_date')}
              type='text' 
              className="form-control" 
          />
        </div>
        
    } else {
      return !invisibles.includes('surgical_event_date') && <div className="form-group">
        <label>Fecha de evento quirurgico</label>
        <DatePicker
          locale="es"
          disabled={!editable.includes('surgical_event_date') || containHospitalExpense() && expense.includes('surgical_event_date')}
          selected={ surgicalDate }
          onChange={date => {handleSurgicalDate(date)}}
          name="surgical_event_date"
          className="form-control"
          dateFormat="dd/MM/yyyy"
          minDate={minDate}
        />
        </div>
    }
  }
  const showInputsForModule = () => {
    let amount_coinsurance_hospital = (parseInt(order.hospital_expense) - parseInt(order.deductible)) * (parseInt(order.hospital_coinsurance) / 100 )
    let amout_coinsurance_medic = parseInt(order.medical_fees) * (parseInt(order.coinsurance_percentage) / 100 )
    let amount_coinsurance_provider = parseInt(order.coinsurance_money) - (amount_coinsurance_hospital + amout_coinsurance_medic)
    if(module === 'HospitalSupervision'){
        return <Fragment>
        <p>MONTO COASEGURO HOSPITAL: {
            formatMoney(amount_coinsurance_hospital > 0 ? amount_coinsurance_hospital : 0)
        }</p>
        <p>MONTO COASEGURO MEDICO: {
            formatMoney(amout_coinsurance_medic.toFixed(2))
        }</p>
        <p>MONTO COASEGURO PROVEEDOR: { 
            formatMoney(amount_coinsurance_provider > 0 ? amount_coinsurance_provider : 0)
        }</p>
        </Fragment>
    }
  }
  return <Fragment>
    <div className="row">
      <div className="col sm-6">
        {
          !invisibles.includes('order_type') && <div className="form-group">
            <label>Tipo de Solicitud</label>
            <input 
                name='order_type'
                onChange={onChange}
                placeholder={form['order_type'] || order['order_type']}
                disabled={!editable.includes('order_type') || containHospitalExpense() && expense.includes('order_type')}
                type='text' 
                className="form-control" 
            />
          </div>
        }
        
        <p>Penalización: NO</p>
        <p>Hospital: { hospital.name }</p>
        <p>Medico Tratante: { doctor.name }</p>
        <p>Asegurado: { order.insured ? order.insured : `${order.insured_name} ${order.insured_last_name} ${order.insured_mother_last_name}` }</p>
        <p>Padecimiento: { order.suffering }</p>
        <p>Procedimiento: { order.procedure }</p>
        { showLegends() }

        {
          !invisibles.includes('icd') && <div className="form-group">
            <label>ICD</label>
            <input 
                name='icd'
                onChange={onChange}
                placeholder={form['icd'] || order['icd']}
                // value={} 
                disabled={!editable.includes('icd') || containHospitalExpense() && expense.includes('icd')}
                type='text' 
                className="form-control" 
            />
            </div>
        }
        {

          !invisibles.includes('cpt') && <div className="form-group">
            <label>CPT</label>
            <input 
                name='cpt'
                onChange={onChange}
                placeholder={form['cpt'] || order['cpt']}
                // value={} 
                disabled={!editable.includes('cpt') || containHospitalExpense() && expense.includes('cpt')}
                type='text' 
                className="form-control" 
            />
          </div>
        }
        {
          !invisibles.includes('second_cpt') && <div className="form-group">
            <label>CPT</label>
            <input 
                name='second_cpt'
                onChange={onChange}
                placeholder={form['second_cpt'] || order['second_cpt']}
                disabled={!editable.includes('second_cpt') || containHospitalExpense() && expense.includes('second_cpt')}
                type='text' 
                className="form-control" 
            />
          </div>
        }
      </div>
      <div className="col sm-6">
      {
        !invisibles.includes('departure_date') && <div className="form-group">
          <label>Fecha de alta</label>
          <br/>
          <DatePicker
            locale="es"
            selected={departureDate}
            onChange={date => {handleDepartureDate(date)
            }}
            disabled={!editable.includes('departure_date') || containHospitalExpense() && expense.includes('departure_date')}
            name="departure_date"
            showTimeInput
            className="form-control"
            dateFormat="dd/MM/yyyy h:mm aa"
            minDate={minDate}
          />
        </div>
        }

        {
          !invisibles.includes('deductible') && <div className="form-group">
            <label>Deducible($)</label>
            <MaskedInput 
                mask={createNumberMask({
                    prefix: '',
                    allowDecimal: true
                })}
                name='deductible'
                onChange={onChange}
                placeholder={formatMoney(form['deductible'] || order['deductible'])}
                disabled={!editable.includes('deductible') || containHospitalExpense() && expense.includes('deductible')}
                className="form-control" 
            />
        </div>
        }

        {
          !invisibles.includes('hospital_coinsurance') && <div className="form-group">
                <label>Coaseguro hospital(%)</label>
                <input 
                    name='hospital_coinsurance'
                    onChange={ e => {
                      const re = /\d+(\.\d{1,2})?/
                      if (re.test(e.target.value) && e.target.value > 0 && e.target.value.length <= 2) {
                        onChange(e)
                      }
                  }}
                    value={form['hospital_coinsurance'] || order['hospital_coinsurance']}
                    placeholder={form['hospital_coinsurance'] || order['hospital_coinsurance']}
                    disabled={!editable.includes('hospital_coinsurance') || containHospitalExpense() && expense.includes('hospital_coinsurance')}
                    type='number' 
                    className="form-control" 
                />
          </div>
        }

        {
          !invisibles.includes('coinsurance_money') && <div className="form-group">
            <label>Remanente coaseguro($)</label>
            <MaskedInput 
                mask={createNumberMask({
                    prefix: '',
                    allowDecimal: true
                })}
                name='coinsurance_money'
                onChange={onChange}
                placeholder={formatMoney(form['coinsurance_money'] || order['coinsurance_money'])}
                disabled={!editable.includes('coinsurance_money') || containHospitalExpense() && expense.includes('coinsurance_money')}
                className="form-control" 
            />
        </div>
        }

        {
          !invisibles.includes('coinsurance_percentage') && <div className="form-group">
                <label>Coaseguro Honorarios(%)</label>
                <input 
                    name='coinsurance_percentage'
                    onChange={ e => {
                      const re = /\d+(\.\d{1,2})?/
                      if (re.test(e.target.value) && e.target.value > 0 && e.target.value.length <= 2) {
                        onChange(e)
                      }
                  }}
                    value={form['coinsurance_percentage'] || order['coinsurance_percentage']}
                    placeholder={form['coinsurance_percentage'] || order['coinsurance_percentage']}
                    disabled={!editable.includes('coinsurance_percentage') || containHospitalExpense() && expense.includes('coinsurance_percentage')}
                    type='text' 
                    className="form-control" 
                />
          </div>
        }

        {
          !invisibles.includes('medical_fees') && <div className="form-group">
            <label>Monto Honorarios Médicos($)</label>
            <MaskedInput 
                mask={createNumberMask({
                    prefix: '',
                    allowDecimal: true
                })}
                name='medical_fees'
                onChange={onChange}
                placeholder={formatMoney(form['medical_fees'] || order['medical_fees'])}
                disabled={!editable.includes('medical_fees') || containHospitalExpense() && expense.includes('medical_fees')}
                className="form-control" 
            />
        </div>
        }

        {
          !invisibles.includes('gnp_invoice') && <div className="form-group">
                <label>Folio GNP Hospitalización</label>
                <input 
                    name='gnp_invoice'
                    onChange={onChange}
                    placeholder={form['gnp_invoice'] || order['gnp_invoice']}
                    disabled={!editable.includes('gnp_invoice') || containHospitalExpense() && expense.includes('gnp_invoice')}
                    type='text' 
                    className="form-control" 
                />
          </div>
        }
        {
          !invisibles.includes('claim') && <div className="form-group">
                <label>Reclamación</label>
                <input 
                    name='claim'
                    onChange={onChange}
                    placeholder={form['claim'] || order['claim']}
                    disabled={!editable.includes('claim') || containHospitalExpense() && expense.includes('claim')}
                    type='text' 
                    className="form-control" 
                />
          </div>
        }
        {
          !invisibles.includes('entry_probable_date') && <div className="form-group">
                <label>Fecha probable de ingreso</label>
                <input 
                    name='entry_probable_date'
                    onChange={onChange}
                    placeholder={form['entry_probable_date'] || order['entry_probable_date']}
                    disabled={!editable.includes('entry_probable_date') || containHospitalExpense() && expense.includes('entry_probable_date')}
                    type='text' 
                    className="form-control" 
                />
          </div>
        }
        {
          !invisibles.includes('departure_probable_date') && <div className="form-group">
                <label>Fecha probable de alta</label>
                <input 
                    name='departure_probable_date'
                    onChange={onChange}
                    placeholder={form['departure_probable_date'] || order['departure_probable_date']}
                    disabled={!editable.includes('departure_probable_date') || containHospitalExpense() && expense.includes('departure_probable_date')}
                    type='text' 
                    className="form-control" 
                />
          </div>
        }

        { showDateByOrderType() }
        { showInputsForModule() }

        {
          !invisibles.includes('episode') && <div className="form-group">
                <label>Folio Hospital / Episodio</label>
                <input 
                    name='episode'
                    onChange={onChange}
                    placeholder={form['episode'] || order['episode']}
                    disabled={!editable.includes('episode') || containHospitalExpense() && expense.includes('episode')}
                    type='text' 
                    className="form-control" 
                />
          </div>
        }
        {
          !invisibles.includes('evo_invoice') && <div className="form-group">
                <label>Folio EVO</label>
                <input 
                    name='evo_invoice'
                    onChange={onChange}
                    placeholder={form['evo_invoice'] || order['evo_invoice']}
                    disabled={!editable.includes('evo_invoice') || containHospitalExpense() && expense.includes('evo_invoice')}
                    type='text' 
                    className="form-control" 
                />
          </div>
        }
        {
          !invisibles.includes('wf_invoice') && <div className="form-group">
                <label>Folio WF</label>
                <input 
                    name='wf_invoice'
                    onChange={onChange}
                    placeholder={form['wf_invoice'] || order['wf_invoice']}
                    disabled={!editable.includes('wf_invoice') || containHospitalExpense() && expense.includes('wf_invoice')}
                    type='text' 
                    className="form-control" 
                />
          </div>
        }
        {
          !invisibles.includes('hospital_expense') && <div className="form-group">
            <label>Gasto Hospitalario</label>
            <MaskedInput 
                mask={createNumberMask({
                    prefix: '',
                    allowDecimal: true
                })}
                name='hospital_expense'
                onChange={onChange}
                placeholder={formatMoney(form['hospital_expense'] || order['hospital_expense'])}
                disabled={!editable.includes('hospital_expense') || containHospitalExpense() && expense.includes('hospital_expense')}
                className="form-control" 
            />
        </div>
        }
      </div>
    </div>
  </Fragment>
}

export default OrderForm