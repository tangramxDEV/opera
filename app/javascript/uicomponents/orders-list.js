import React from 'react'

const OrdersList = ({orders,route}) => (
    <table className="table table-bordered">
            <thead>
            <tr>
                <th scope="col">Ticket CPO</th>
                <th scope="col">Tipo de Procedimiento</th>
                <th scope="col">Asegurado</th>
                <th scope="col">Médico Tratante</th>
                <th scope="col">Hospital</th>
                <th scope="col">Estado</th>
                <th scope="col">Procedimiento</th>
                <th scope="col">Fecha de cirugia</th>
                <th scope="col">Opciones</th>
            </tr>
            </thead>
            <tbody>
                { orders.map((order, index) => (
                    <tr key={index}>
                        <th scope="row">{ order.cpo }</th>
                        <td>{ order.order_type }</td>
                        <td>{ order.insured }</td>
                        <td>{ order.doctor.name }</td>
                        <td>{ order.hospital.name }</td>
                        <td>{ order.hospital.state }</td>
                        <td>{ order.procedure }</td>
                        <td>{ order.surgery_date }</td>
                        <td>
                            <a href={`${route}/${order.slug}`}>Ir</a>
                        </td>
                    </tr>
                )) }
            </tbody>
        </table>
)

export default OrdersList