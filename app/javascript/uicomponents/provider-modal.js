import React from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'

const ProviderModal = ({
    form, 
    handleChange, 
    handleSubmit, 
    toggle,
    modal
}) => {

    return <React.Fragment>
        <Modal isOpen={modal} toggle={toggle} >
            <ModalHeader toggle={toggle}>
                Nuevo Proveedor
            </ModalHeader>
            <ModalBody>
                <form>
                    <div className="form-group">
                        <label>Nombre:</label>
                        <input 
                            type="text" 
                            className="form-control"
                            name="name"
                            value={form.name}
                            onChange={handleChange}
                        />
                    </div>
                </form>
            </ModalBody>
            <ModalFooter>
                <button 
                    type="button" 
                    className="btn btn-primary"
                    onClick={handleSubmit}
                >
                    Registrar
                </button>
            </ModalFooter>
        </Modal>

        <button 
            type="button" 
            className="btn btn-outline-primary float-right"
            onClick={toggle}
        >
        <i className="fas fa-plus"></i>
        </button>
    </React.Fragment>
}

export default ProviderModal