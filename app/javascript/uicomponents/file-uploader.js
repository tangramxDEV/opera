import React,{
    useState
} from 'react'
import { URL } from '../settings/configs'
const FileUploader = ({
    uploadBtn,
    setFile,
    authenticity_token,
}) => {
    const [currentFile, setCurrentFile] = useState('')
    const [maxSize, setMaxSize] = useState(false)

    const handleUploadFile = async (e) => {

        const formData = new FormData()
        formData.append('file_uploader[file]', currentFile)
        formData.append('authenticity_token', authenticity_token)

        console.log(authenticity_token)
        try {
            let config = {
              method: 'POST',
              body: formData,
              contentType: false,
              processData: false
            }
        
            await fetch(`${URL}/file_uploaders`, config).then(async (res) => {
                let data = await res.json()
                
                setFile(data.url)
                console.log(data.url)
                
            })
          } catch (error) {
            console.log(error)
        }
    }

    const onChange = (e) => {
      setMaxSize(false)
      let file = e.target.files[0]
      if(file.size/1024/1024 <= 5){
        setCurrentFile(file)
      } else {
        setMaxSize(true)      
      }
    }

    return <React.Fragment>
        <div className="row">
            <div className="col sm-4">
                <div>
                { maxSize ? 'El peso del archivo excede el limite permitido, intenta con otro' : '' }
                </div>
                <div className="form-group">
                    <input 
                        accept="application/pdf"
                        type="file" 
                        className="space-t form-control-file" 
                        id="theFileInput" 
                        onChange={onChange}
                    />
                </div>
            </div>
            <div className="col sm-4 space-t-x">
                <button 
                    className="btn btn-outline-primary"
                    onClick={handleUploadFile}
                    style={{visibility: uploadBtn}}
                >
                <i class="fas fa-save"></i>
                </button>
            </div>
        </div>
        </React.Fragment>
}



export default FileUploader
