import React from 'react'

const ICDModal = ({form, handleChange, handleSubmit}) => {
    return <React.Fragment>
    <div className="modal fade" id="icdModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
            <div className="modal-content">
            <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">Nuevo ICD</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div className="modal-body">
                <form>
                <div className="form-group">
                    <label for="recipient-name" className="col-form-label">Código:</label>
                    <input 
                        type="text" 
                        className="form-control"
                        name="code"
                        value={form.code}
                        onChange={handleChange}
                    />
                </div>
                <div className="form-group">
                    <label for="recipient-name" className="col-form-label">Nombre:</label>
                    <input 
                        type="text" 
                        className="form-control"
                        name="description"
                        value={form.description}
                        onChange={handleChange}
                    />
                </div>
                </form>
            </div>
            <div className="modal-footer">
                <button 
                    type="button" 
                    className="btn btn-primary"
                    onClick={handleSubmit}
                >
                    Registrar
                </button>
            </div>
            </div>
        </div>
        </div>

        <button 
            type="button" 
            className="btn btn-outline-primary float-right"
            data-toggle="modal" 
            data-target="#icdModal" 
        >
        <i className="fas fa-plus"></i>
    </button>
    </React.Fragment>
}

export default ICDModal