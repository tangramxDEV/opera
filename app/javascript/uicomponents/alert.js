import React from 'react'


const Alert = ({manageable, visible, error}) => {
    if(visible){
        if(manageable){
            return <div className="alert alert-success" role="alert">
            SOLICITUD GESTIONABLE POR ESTE MODELO.
          </div>
        } else {
            return <div className="alert alert-danger" role="alert">
            {error}.
          </div>
        }
    } else {
        return <React.Fragment></React.Fragment>
    }
}
export default Alert