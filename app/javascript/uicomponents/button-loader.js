import React from 'react'

const buttonLoader = ({visibility}) => {
    let visible = 'hidden'

    if(visibility)
        visible = 'visible'

    return <React.Fragment style={{visibility: visible}}>
        <div className="spinner-border spinner-border-sm text-light" role="status">
            <span className="sr-only">Loading...</span>
        </div>
    </React.Fragment>
}

export default buttonLoader