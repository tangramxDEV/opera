import { Controller } from 'stimulus'
import Rails from '@rails/ujs'

export default class extends Controller {
    static targets = ['document_type', 'value', 'input']

    set_document_type(){
        console.log(this.document_typeTarget.value)
    }

    display(event){
        const fileName = event.target.value.split('\\').pop()
        if(this.valueTarget.nodeName == 'INPUT'){
            this.valueTarget.placeholder = fileName
        } else {
            this.valueTarget.innerHTML = fileName
        }
    }

    async upload(){
        console.log('uploading...')
        const formData = new FormData()

        formData.append('file_uploader[file]', this.inputTarget.files[0])
        formData.append('authenticity_token', Rails.csrfToken())

        let config = {
            method: 'POST',
            body: formData,
            contentType: false,
            processData: false
        }

        try {
            const uploader = await fetch(`${'http://localhost:3000'}/file_uploaders`, config)
            const data = await uploader.json()

            console.log(data)            
        } catch (error) {
            console.log(error)
        }
    }
}