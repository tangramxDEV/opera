import { Controller } from 'stimulus'

export default class extends Controller {
    connect() {
        
    }
    initialize(){
        this.getHospitals()
    }

    getHospitals(){
        fetch('http://localhost:3000/hospitals')
        .then(response => response.text())
        .then(html => {
            console.log(html)
        })
    }
}