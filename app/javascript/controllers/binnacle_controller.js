import { Controller } from 'stimulus'

export default class extends Controller {
    static targets = ['comment', 'writed_comment']

    write_comment(){
        this.writed_commentTarget.textContent = this.commentTarget.value
    }
}