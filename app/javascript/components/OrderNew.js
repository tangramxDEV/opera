import React, { useState, useEffect } from "react"
import useProviderSearcher from '../hooks/useProviderSearcher'
import { URL } from '../settings/configs'
import InputSearcher from '../uicomponents/input-searcher'
import FilteredProviders from '../uicomponents/filtered-providers'
import BtnContinue from '../uicomponents/btn-continue'
import Alert from '../uicomponents/alert'
import ProviderModal from '../uicomponents/provider-modal'
import DoctorModal from '../uicomponents/doctor-modal'
import MaskedInput from 'react-text-mask'
import createNumberMask from 'text-mask-addons/dist/createNumberMask'
import useFormatter from '../hooks/useFormatter'


const OrderNew = ({authenticity_token}) => {
  const { formatMoney } = useFormatter()
  const [cost, setCost] = useState(0)
  const [amount, setAmount] = useState(0)
  const [medicalFees, setMedicalFees] = useState(0)
  const [manageable, setManageable] = useState(false)
  const [searchProvider, setSearchProvider] = useState('')
  const [form, setForm] = useState({
    'sum_assured': '',
    'penalty': false,
    'coinsurance_percentage': '',
    'coinsurance_money': '',
    'hospital_coinsurance': '',
    'deductible': '',
    'medical_fees': ''
  })
  const [savedProvider, setSavedProvider] = useState(false)
  const [providerForm, setProviderForm] = useState({
    'status': 'pending',
    'name':'',
    'is_new': true
  })
  const [doctorForm, setDoctorForm] = useState({
    'name':'',
    'speciality':'',
    'city':'',
    'address':''
  })
  const [manageError, setManageError] = useState('')
  const [selectedProviders, setSelectedProviders] = useState([])
  const [providerCoinsurance, setProviderCoinsurance] = useState(false)
  const [providers, setProviders] = useState([])
  const [doctors, setDoctors] = useState([])
  const [hospitals, setHospitals] = useState([])
  const [hospital, setHospital] = useState({})
  const [procedures, setProcedures] = useState([])
  const [procedure, setProcedure] = useState('')
  const [sufferings, setSufferings] = useState([])
  const [hospitalCost, setHospitalCost] = useState(0)
  const [doctor, setDoctor] = useState(0)
  const [suffering, setSuffering] = useState({})

  const [alertVisible, setAlertVisible] = useState(false)

  const { query, setQuery, filteredProviders } = useProviderSearcher(providers) // Filter para Insumos Disponibles


  useEffect(() => {
    const headers = new Headers()
    headers.append('Content-Type','application/json')
    headers.append('X-CSRF-Token', authenticity_token)

    const payload = {
      method: 'GET',
      headers: headers,
      mode: 'cors',
      cache: 'default'
    }
    const fetchProviders = async () => {
        let res = await fetch(`${URL}/providers?not_new`, payload)
        res
          .json()
          .then(res => setProviders(res))
          .catch(err => console.log(err))
    }

    const fetchHospitals = async () => {
        let res = await fetch(`${URL}/hospitals`, payload)
        res 
          .json()
          .then(res => setHospitals(res))
          .catch(err => console.log(err))
    }

    fetchHospitals()
    fetchDoctors()
    fetchProviders()
}, [])

const fetchDoctors = async () => {
  let res = await fetch(`${URL}/doctors`)
  res
    .json()
    .then(res => setDoctors(res))
    .catch(err => console.log(err))
}

const fetchSufferings = async(id) => {
  let res = await fetch(`${URL}/hospitals/${id}/sufferings`)
  res
    .json()
    .then(res => setSufferings(res))
    .catch(err => console.log(err))
}

const fetchProcedures = async(id, type) => {
  let res = await fetch(`${URL}/hospitals/${id}/procedures?type=${type}`)
  res
    .json()
    .then(res => setProcedures(res))
    .catch(err => console.log(err))
}
const handleSufferingChange = e => {
  setSuffering(e.target.value)
  fetchProcedures(hospital.id, e.target.value)
}

const handleHospitalChange = e => {
  let filteredHospital = hospitals.filter((hospital) => (
    hospital.id == e.target.value  
  ))
  setHospital(filteredHospital[0])
  fetchSufferings(filteredHospital[0].id)
}

const handleProcedureChange = e => {
  let procedure = procedures.find((procedure) => (
    procedure.id == e.target.value  
  ))
  
  setProcedure(procedure.name)

  let procedure_offered = procedure.procedure_offereds.find((procedure_offered) => (
    procedure_offered.procedure_id == procedure.id && procedure_offered.hospital_id == hospital.id
  ))

  let cost = Number.parseFloat(procedure_offered.cost)
  let margin = Math.floor((cost + medicalFees) * (20))/100
  setCost(cost)
  
  setHospitalCost(Number.parseFloat(procedure_offered.hospital_cost))
  setAmount(cost + margin)
}

const handleCheckManagement = () => {
    setAlertVisible(true)
 
    if(Number.parseFloat(form.coinsurance_money) > 0 && Number.parseFloat(form.hospital_coinsurance) <= 0 || Number.parseFloat(form.coinsurance_money) > 0 && Number.parseFloat(form.coinsurance_percentage) <= 0){
      setManageable(false) // Hospital ó Proveedor no cobra coaseguro
      setManageError('FAVOR DE VALIDAR MONTOS Y % DE COASEGURO PARA CONTINUAR')
    } else {
      if(form.penalty == false) { // RULE 01 - Si no esta penalizado
        if(hospitalCost >= Number.parseFloat(form.deductible ? form.deductible.replace(/,/g, '') : 0)){
          if(Number.parseFloat(form.sum_assured.replace(/,/g, '')) > ((amount * 1.2) + (Number.parseFloat(form.medical_fees ? form.medical_fees.replace(/,/g, '') : 0) * 1.6))){
            if(searchProvider == 'REPORTE HOSPITALARIO'){
                setManageable(true)
            } else {
              if(selectedProviders.length > 0){
                if(hospital.coinsurance == true || providerCoinsurance == true){ // Si el hospital cobra coseguro
                  if(doctor !== 0){ // Si el doctor ya fue seleccionado
                    setManageable(true)
                  } else {
                    setManageable(false) // Hospital ó Proveedor no cobra coaseguro
                    setManageError('Debes seleccionar un médico antes de continuar')  
                  }
                } 
              } else {
                setManageable(false) // Hospital ó Proveedor no cobra coaseguro
                setManageError('SELECCIONA AL MENOS UN PROVEEDOR')
              }
            }
          } else {
            setManageable(false) // El seguro no cubre el costo
            setManageError('COMO EL MONTO TOTAL ESTIMADO ES MAYOR A LA SUMA ASEGURADA NO ES GESTIONABLE')
          }
        } else {
          setManageable(false)
          setManageError('COMO EL MONTO DEL DEDUCIBLE ES MAYOR AL MONTO DE LA CUENTA HOSPITALARIA, SE CONSIDERA NO GESTIONABLE')
        }
      } else { // END RULE 01
        setManageable(false) // Esta penalizado
        setManageError('COMO LA SOLICITUD CUENTA CON PENALIZACION, SE CONSIDERA NO GESTIONABLE.')
      }
    }
}
const handleDoctorChange = e => {
  setDoctor(e.target.value)
}

const handleChange = e => {
  setForm({
      ...form,
      [e.target.name]: e.target.value
  })
}

const handleProviderFormSubmit = async () => {
  let data = {
    provider: {
      ...providerForm
    },
    authenticity_token
  }

  console.log(data)
  try {
    let config = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }

    await fetch(`${URL}/providers/hold`, config).then(async (res) => {
      if(res.status == '200'){
        let data = await res.json()
        toggle()
        setProviders([
          ...providers,
          data.provider
        ])
        setProviderForm({
          'status': 'pending',
          'name':''
        })
      }
    })
  } catch (error) {
    console.log(error)
  }
}
const handleProviderFormChange = e => {
  setProviderForm({
    ...providerForm,
    [e.target.name]: e.target.value
  })
}

const handleDoctorFormChange = e => {
  setDoctorForm({
    ...doctorForm,
    [e.target.name]: e.target.value.toUpperCase()
  })
}
const handleDoctorFormSubmit = async () => {
  let data = {
    doctor: {
      name: `${doctorForm.last_name} ${doctorForm.mother_last_name} ${doctorForm.name}`
    },
    authenticity_token
  }
  try {
    let config = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }

    await fetch(`${URL}/doctors`, config).then(async (res) => {
      
      if(res.status == '200'){
        let data = await res.json()
        toggleDoctor()
        fetchDoctors()
        setDoctorForm({
          'name':'',
          'speciality':'',
          'city':'',
          'address':''
        })
        alert("MEDICO YA REGISTRADO");
      } else {
        alert("EL MEDICO PARECE YA ESTAR REGISTRADO");
      }
    })
  } catch (error) {
    console.log(error)
  }
}
const handleAddProvider = (provider) => {
  if(!selectedProviders.some(current_provider => current_provider.id === provider.id)){
    if(provider.coinsurance){
      setProviderCoinsurance(true)
    }
    setSelectedProviders([
      ...selectedProviders,
      provider
    ])  
  }
}

const handleOrderType = (e) => {
  setSearchProvider(e.target.value)
}

const [modal, setModal] = useState(false)
const toggle = () => setModal(!modal)

const [doctorModal, setDoctorModal] = useState(false)
const toggleDoctor = () => setDoctorModal(!doctorModal)

const handleRemoveSelectedProvider = (provider) => {
  setSelectedProviders(selectedProviders.filter((current) => (
    current !== provider
  )))
}

const providerSearcher = () => {
  if(searchProvider === 'PROGRAMACIÓN DE CIRUGÍA'){
    return <React.Fragment>
      <ul className="list-group">
        { selectedProviders.map((provider, index) => (
          <li 
            className="list-group-item" 
            key={index}
          >
            {provider.name}
            <button 
              className="btn btn-outline-danger float-right"
              onClick={() => handleRemoveSelectedProvider(provider)}
            >
              <i className="fas fa-trash"></i>
            </button>
          </li>
        )) }
      </ul>
      
      <div class="input-group">
        <div class="input-group-prepend">
          <ProviderModal 
            form={providerForm}
            handleChange={handleProviderFormChange}
            handleSubmit={handleProviderFormSubmit}
            toggle={toggle}
            modal={modal}
          />
        </div>
        <InputSearcher   
              placeholder={'BUSCAR PROVEEDOR'}
              query={query}
              setQuery={setQuery}
          />
      </div>
    
      <FilteredProviders
        filteredProviders={filteredProviders}
        handleClick={handleAddProvider}
      />
      
  </React.Fragment>
  }
}
const handleSubmit = async () => {
  let newSelectedProviders = []
  let specialCase = ''

  if (searchProvider === 'PROGRAMACIÓN DE CIRUGÍA'){
    selectedProviders.map((provider) => {
      if(provider.status == 'pending'){
        specialCase = 'Nuevo Proveedor'
      }
      newSelectedProviders.push(provider.id)
    })
  }
  
  let data = {
    order: {
      doctor_id: doctor,
      hospital_id: hospital.id,
      suffering: suffering,
      procedure: procedure,
      order_type: searchProvider,
      selected_providers: newSelectedProviders,
      // percentages
      coinsurance_percentage: form.coinsurance_percentage == '' ? 0 : form.coinsurance_percentage, 
      hospital_coinsurance: form.hospital_coinsurance == '' ? 0 : form.hospital_coinsurance,
      // floats
      medical_fees: form.medical_fees == '' ? 0 : Number.parseFloat(form.medical_fees.replace(/,/g, '')),
      deductible: Number.parseFloat(form.deductible.replace(/,/g, '')),
      coinsurance_money: form.coinsurance_money == '' ? 0 : Number.parseFloat(form.coinsurance_money.replace(/,/g, '')),

    },
    authenticity_token
  }
  
  if(specialCase == 'Nuevo Proveedor'){
    data['order']['special_type']  = specialCase
  }

  try {
    let config = {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }

    await fetch(`${URL}/orders`, config).then(async (res) => {
      if(res.status == '200'){
        let data = await res.json()
        window.location.href = `/orders/${data.order.slug}/edit`
      }
        
    })
  } catch (error) {
    console.log(error)
  }
}

  return <React.Fragment>
  <div className="form-group">
    <label htmlFor="exampleFormControlSelect1">TIPO DE SOLICITUD</label>
    <select 
      className="form-control"
      onChange={handleOrderType}
    >
      <option value="">Selecciona una opcion</option>
      <option>PROGRAMACIÓN DE CIRUGÍA</option>
      <option>REPORTE HOSPITALARIO</option>
    </select>
  </div>

  <fieldset className="form-group">
    <div className="row">
      <legend className="col-form-label col-sm-2 pt-0">PENALIZACION</legend>
      <div className="col-sm-10">
        <div className="form-check">
          <input 
            className="form-check-input" 
            type="radio" 
            name="penalty" 
            id="gridRadios1" 
            value="1" 
            checked={form.penalty == true}
            onChange={handleChange}
          />
          <label className="form-check-label" htmlFor="gridRadios1">
            SI
          </label>
        </div>
        <div className="form-check">
          <input 
            className="form-check-input"
            type="radio" 
            name="penalty" 
            id="gridRadios2" 
            checked={form.penalty == false}
            onChange={handleChange}
            value="0"
          />
          <label className="form-check-label" htmlFor="gridRadios2">
            NO
          </label>
        </div>
      </div>
    </div>
  </fieldset>

    <div className="form-group">
      <label htmlFor="exampleFormControlSelect1">HOSPITAL</label>
      <select onChange={handleHospitalChange} className="form-control" id="exampleFormControlSelect1">
      <option value="">Selecciona una opcion</option>
      { hospitals.map((hospital, index) => (
        <option value={hospital.id} key={index}>{ hospital.name }</option>
      )) }
      </select>
    </div>

    <label>MEDICO</label>
    <div class="input-group">
      <div class="input-group-prepend">
        <DoctorModal
            form={doctorForm}
            handleChange={handleDoctorFormChange}
            onSubmit={handleDoctorFormSubmit}
            toggle={toggleDoctor}
            modal={doctorModal}
        />
      </div>
        <select onChange={handleDoctorChange} className="form-control" id="exampleFormControlSelect1">
        <option value="">Selecciona una opcion</option>
        { doctors.map((doctor, index) => (
          <option value={doctor.id} key={index}>{ doctor.name }</option>
        )) }
        </select>
      </div>


    <div className="form-group">
      <label htmlFor="exampleFormControlSelect1">PADECIMIENTO</label>
      <select onChange={handleSufferingChange} className="form-control" id="exampleFormControlSelect1">
      <option value="">Selecciona una opcion</option>
      { sufferings.map((suffering, index) => (
        <option value={suffering.description} key={index}>{ suffering.description }</option>
      )) }
      </select>
    </div>

    <div className="form-group">
      <label htmlFor="exampleFormControlSelect1">PROCEDIMIENTO</label>
      <select  onChange={handleProcedureChange} className="form-control" id="exampleFormControlSelect1">
        <option value="">Selecciona una opcion</option>
        { procedures.map((procedure, index) => (
          <option value={procedure.id} key={index}>{ procedure.name }</option>
          )) }
      </select>
    </div>
        
    <div className="form-group">
      <label>HONORARIO MEDICO</label>
      <div className="input-group mb-2">
        <div className="input-group-prepend">
          <div className="input-group-text">$</div>
        </div>
        <MaskedInput
          mask={createNumberMask({
            prefix: '',
            allowDecimal: true
          })}
          className="form-control" 
          placeholder="0.00" 
          value={form.medical_fees}
          name="medical_fees"
          onChange={handleChange}
        />
      </div>
    </div>

    <div className="form-group">
      <label>MONTO ESTIMADO</label>
      <div className="input-group mb-2">
        <div className="input-group-prepend">
          <div className="input-group-text">$</div>
        </div>
        <input 
          type="text" 
          className="form-control" 
          disabled 
          value={`${formatMoney(cost)}`} />
      </div>
    </div>

    <div className="form-group">
    <label>DEDUCIBLE</label>
    <div className="input-group mb-2">
      <div className="input-group-prepend">
        <div className="input-group-text">$</div>
      </div>
      <MaskedInput
        mask={createNumberMask({
          prefix: '',
          allowDecimal: true
        })}
        className="form-control" 
        placeholder="0.00" 
        name="deductible"
        value={form.deductible}
        onChange={handleChange}
      />
    </div>
  </div>

    <div className="form-group">
      <label>SUMA ASEGURADA</label>
      <div className="input-group mb-2">
        <div className="input-group-prepend">
          <div className="input-group-text">$</div>
        </div>
          <MaskedInput
          mask={createNumberMask({
            prefix: '',
            allowDecimal: true
          })}
          className="form-control"
          name="sum_assured"
          placeholder="0.00"
          value={form.sum_assured}
          onChange={handleChange}
        />
      </div>
    </div>

    <div className="form-group">
      <label>COASEGURO HOSPITAL</label>
      <div className="input-group mb-2">
        <div className="input-group-prepend">
          <div className="input-group-text">%</div>
        </div>
        <input
          type="number" 
          className="form-control"
          name="hospital_coinsurance"
          value={form.hospital_coinsurance}
          onChange={ e => {
            const re = /^\d{1,2}$/
            if (re.test(e.target.value)) {
                handleChange(e)
            }
          }}
          placeholder="0"
          min="0"
          step="1"
          max="100"
        />
      </div>
    </div>
    <div className="form-group">
      <label>COASEGURO HONORARIOS</label>
      <div className="input-group mb-2">
        <div className="input-group-prepend">
          <div className="input-group-text">%</div>
        </div>
        <input 
          type="text" 
          className="form-control"
          name="coinsurance_percentage"
          value={form.coinsurance_percentage}
          onChange={ e => {
            const re = /^\d{1,2}$/
            if (re.test(e.target.value)) {
                handleChange(e)
            }
          }}
          placeholder="0"
          min="0"
          step="1"
          max="100"
        />
      </div>
    </div>
    <div className="form-group">
      <label>REMANENTE COASEGURO</label>
      <div className="input-group mb-2">
        <div className="input-group-prepend">
          <div className="input-group-text">$</div>
        </div>
        <MaskedInput
          mask={createNumberMask({
            prefix: '',
            allowDecimal: true
          })}
          className="form-control"
          name="coinsurance_money"
          value={form.coinsurance_money}
          onChange={handleChange}
          placeholder="0.00"
        />
      </div>
    </div>
    
    { providerSearcher() }

    <div style={{'marginTop': '50px'}}></div>
      <button 
        className="btn btn-primary float-right"
        onClick={handleCheckManagement}
      >
        Consultar
    </button>

    <BtnContinue 
        manageable={manageable}
        handleSubmit={handleSubmit}
    />
    <br/><br/>
    <Alert
        manageable={manageable}
        visible={alertVisible}
        error={manageError}
    />
    

    <div style={{'marginTop': '100px'}}></div>
  </React.Fragment>
}


export default OrderNew
