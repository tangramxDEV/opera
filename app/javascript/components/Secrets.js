import React,{
    useState
} from 'react'
import { URL } from '../settings/configs'

const Secrets = ({authenticity_token}) => {
    const [loader, setLoader] = useState('hidden')
    const [url, setUrl] = useState('')
    const [isMaxSize, setIsMaxSize] = useState(false)

    const handleUploadFile = async (e) => {
        let file = fileInput.files[0]
        setIsMaxSize(false)
        if(file.size/1024/1024 <= 5){
          const formData = new FormData()
          formData.append('file_uploader[file]', file)
          formData.append('authenticity_token', authenticity_token)
          setLoader('visible')
        
          try {
            let config = {
              method: 'POST',
              body: formData,
              contentType: false,
              processData: false
            }
        
            await fetch(`${URL}/file_uploaders`, config).then(async (res) => {
                let data = await res.json()

                if(data.status != 'unprocessable_entity'){
                    setUrl(data.url)
                    setLoader('hidden')
                    document.getElementById("fileInput").value = "";
                }

                setLoader('hidden')
            })
          } catch (error) {
            setLoader('hidden')
            console.log(error)
        }
      } else {
        setIsMaxSize(true)
      }
    }

    return <div className="container">
        <div className="col sm-4">
                <div className="form-group">
                    <input
                        accept="application/pdf"
                        type="file" 
                        className="space-t form-control-file" 
                        id="fileInput" 
                    />
                </div>
            </div>
            <div className="col sm-4 space-t-x">
                <button 
                    className="btn btn-outline-primary"
                    onClick={handleUploadFile}
                >
                    Subir
                </button>
                
                <div 
                    className="space-l spinner-border text-primary" 
                    role="status"
                    style={{visibility: loader}}
                >
                    <span className="sr-only">Loading...</span>
                </div>
            </div>

            <h3>URL: {url}</h3>
    </div>
}

export default Secrets