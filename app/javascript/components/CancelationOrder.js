import React, {
    useEffect,
    useState
} from 'react'
import Alert from '../uicomponents/alert'
import Binnacle from '../uicomponents/binnacle'
import DocumentsContainer from '../uicomponents/documents_container'
import OrderResume from '../uicomponents/order-resume'
import Notifications, { notify } from '../modules/Notifications'
import { current_date_time } from '../functionalities/dates'
import useHttp from '../hooks/useHttp'
import useOrderStatus from '../hooks/useOrderStatus'
import useValidations from '../hooks/useValidations'
import useButtonLoader from '../hooks/useButtonLoader'
import { useForm, FormContext } from "react-hook-form"
import useFormatter from '../hooks/useFormatter'

const CancelationOrder = ({sub_order, order, authenticity_token, username, module}) => {
    const methods = useForm()
    const { get, put } = useHttp()

    const [motive, setMotive] = useState('')
    const [comment, setComment] = useState('')
    const [subOrder, setSubOrder] = useState({provider: {}, sub_order_generics: []})
    const [orderData, setOrderData] = useState('')
    const [hospital, setHospital] = useState('')
    const [doctor, setDoctor] = useState('')

    const [alertVisible, setAlertVisible] = useState(false)

    // Resume Component
    const [departureDate, setDepartureDate] = useState( 
        order.departure_date ?  new Date(order.departure_date) : undefined
    )
    const [surgicalDate, setSurgicalDate] = useState( 
        order.surgical_event_date ?  new Date(order.surgical_event_date) : undefined
    )

    const [resumeForm, setResumeForm] = useState({
        coinsurance_percentage: order.coinsurance_percentage,
        coinsurance_money: order.coinsurance_money,
        deductible: order.deductible,
        medical_fees: order.medical_fees,
        departure_probable_date: order.departure_probable_date,
        // departure_date: order.departure_date ? order.departure_date.split(' ').join('T').substring(0, 16) : '',
        episode: order.episode,
        hospital_expense: order.hospital_expense,
        hospital_coinsurance: order.hospital_coinsurance,
        // surgical_event_date: order.surgical_event_date
    })

    const handleSendOrder = async () => {
        setAlertVisible(false)
        if(motive && comment){

            let data = {
                message: 'Solicitud cancelada.',
                sub_order: {
                    status: 'canceled',
                    cancelation_motive: motive,
                    cancellation_comment: comment,
                },
                authenticity_token
            }
            
            data['comment'] = {
                comment: `${motive}: ${comment}`,
                user: username,
                module: module,
                date: current_date_time()
            }
            
            let res = await put(`cancelations/${sub_order.slug}`, data)
            res
                .json()
                .then(data => window.location.href = `/`)
                .catch(err => err)
        } else {
            setAlertVisible(true)
        }
    }

    const handleSelectMotiveChange = (e) => {
        setMotive(e.target.value)
    }

    const handleCommentChange = (e) => {
        setComment(e.target.value)
    }

    const handleDepartureDate = (date) => {
        setDepartureDate(date)
    }
    
    useEffect(() => {
        const fetchSubOrder = async () => {
            let res = await get(`hospital_supervisions/${sub_order.slug}`)
            res 
                .json()
                .then(data => {
                    setSubOrder(data)
                    setOrderData(data.order)
                    setHospital(data.order.hospital)
                    setDoctor(data.order.doctor)
                })
                .catch(err => console.log(err))
        }
        fetchSubOrder()
    }, [])


    const handleResumeChange = (e) => {
        setResumeForm({
          ...resumeForm,
          [e.target.name]: e.target.value
        })
    }

    return <React.Fragment>
    <FormContext {...methods} >
    <OrderResume
        surgicalDate={surgicalDate}
        departureDate={departureDate}
        handleDepartureDate={handleDepartureDate}
        order={orderData}
        hospital={hospital}
        doctor={doctor}
        title={"Cancelación"}
        expense={[
            "hospital_expense",
            "medical_fees",
            "deductible",
            "hospital_coinsurance",
            "coinsurance_money",
            "coinsurance_percentage",
        ]}
        invisibles={[
            "entry_probable_date",
            "claim",
            "departure_probable_date",
            "insured"
        ]}
        editable={[]}
        validable={[
            "episode",
        ]}
        handleChange={handleResumeChange}
        form={resumeForm}
        module={'HospitalSupervision'}
    />
    <Alert
        manageable={false}
        visible={alertVisible}
        error={'Parece que no haz ingresado el comentario o seleccionado el motivo.'}
    />
    <div className="row">
        <div className="col-6">
            <div className="form-group">
                <label htmlFor="exampleFormControlSelect1">Motivo</label>
                <select 
                    className="form-control" 
                    id="exampleFormControlSelect1"
                    name={motive}
                    onChange={handleSelectMotiveChange}
                >
                    <option disabled selected value> -- selecciona una opción -- </option>
                    <option value="Folio Duplicado">Folio Duplicado</option>
                    <option value="Insumos a cuenta de hospital">Insumos a cuenta de hospital</option>
                    <option value="Error de captura">Error de captura</option>
                    <option value="Cancelacion(otro motivo)">Cancelacion(otro motivo)</option>
                </select>
            </div>
        </div>
        <div className="col-6">
            <div className="form-group">
                <label htmlFor="exampleFormControlSelect1">Comentario</label>
                <input 
                    className="form-control" 
                    type="text"
                    name={comment}
                    onChange={handleCommentChange}
                />
            </div>
        </div>
    </div>
    <button 
        onClick={() => {handleSendOrder()}}
        type="button" 
        className="btn btn-danger"
    >
        CANCELAR
    </button>

    <Notifications/>
    </FormContext>
    </React.Fragment>
}

export default React.memo(CancelationOrder)