import React,{
    useState,
    useEffect
} from 'react'
import Binnacle from '../uicomponents/binnacle'
import DocumentsContainer from '../uicomponents/documents_container'
import OrderResume from '../uicomponents/order-resume'
import useHttp from '../hooks/useHttp'
import { current_date_time } from '../functionalities/dates'
import useFormatter from '../hooks/useFormatter'
import useValidations from '../hooks/useValidations'
import Notifications, { notify } from '../modules/Notifications'

const ProviderNegotiation = ({sub_order, order, authenticity_token, username, module}) => {
    const { formatMoney } = useFormatter()
    const { get, put } = useHttp()
    // Comments Component
    const [comment, setComment] = useState('')
    const [comments, setComments] = useState([])
    // Documents Component
    const [selectDocument, setSelectDocument] = useState(null)
    const [uploadBtn, setUploadBtn] = useState('visible')
    const [loader, setLoader] = useState('hidden')
    const [files, setFiles] = useState([])
    const [newFiles, setNewFiles] = useState([])
    // Resume Component
    const [departureDate, setDepartureDate] = useState( 
        order.departure_date ?  new Date(order.departure_date) : undefined
    )
    const [resumeForm, setResumeForm] = useState({
        departure_date: order.departure_date ? order.departure_date.split(' ').join('T').substring(0, 16) : '',
    })
    // 
    const [orderData, setOrderData] = useState('')
    const [hospital, setHospital] = useState('')
    const [doctor, setDoctor] = useState('')
    const [providers, setProviders] = useState([])
    const [generics, setGenerics] = useState([])
    const [selectedMaterials, setSelectedMaterials] = useState([])
    const [generic, setGeneric] = useState('')
    const [subOrder, setSubOrder] = useState({
        provider: {},
        sub_order_generics: []

    })
    const [materials, setMaterials] = useState([])
    const [materialForm, setMaterialForm] = useState({
        'quantity': '',
        'code': '',
        'description': '',
        'cost': '',
        'brand': '',
        'generic': '',
        'sub_order_generic_id': '',
        'provider_used': 'NO'
    })
    const [providerForm, setProviderForm] = useState({
        'provider_id': '',
        'responsable': '',
        'contact_number': '',
        'provider_negotiation_type': '',
        'affiliate_code': ''
    })

    const { 
        validateIfCommentExist
    } = useValidations()

    const handleSelectDocumentChange = (e) => {
        setSelectDocument(e.target.value)
    }

    const handleMaterialFormChange = e => {
        setMaterialForm({
            ...materialForm,
            [e.target.name]: e.target.value
        })
    }
    const handleComment= e => {
        setComment(e.target.value)
    }
    const handleResumeChange = (e) => {
        setResumeForm({
          ...resumeForm,
          [e.target.name]: e.target.value
        })
      }
    useEffect(() => {
        const fetchOrder = async () => {
            let res = await get(`provider_negotiations/${sub_order.slug}`)
            res
              .json()
              .then(data => {
                setSubOrder(data)
                setProviders([
                    ...providers,
                    data.provider
                ])
                setGenerics(data.sub_order_generics)
                setComments(data.comments)
                setFiles(data.documents)
                setOrderData(data.order)
                setHospital(data.order.hospital)
                setDoctor(data.order.doctor)
                
                console.log(data)
              })
              .catch(err => console.log(err))
        }
        fetchOrder()
    }, [])

    const handleProviderFormChange = (e) => {
        setProviderForm({
            ...providerForm,
            [e.target.name]: e.target.value
        })
    }
    const handleAddMaterial = () => {
        materialForm['generic'] = generic.name
        materialForm['sub_order_generic_id'] = generic.id
        setSelectedMaterials([
            ...selectedMaterials,
            materialForm
        ])
        setMaterialForm({
            'quantity': '',
            'code': '',
            'description': '',
            'cost': '',
            'brand': '',
            'generic': '',
            'sub_order_generic_id': '',
            'provider_used': 'NO'
        })

        setMaterials([
            ...materials,
            materialForm
        ])
        
        console.log(materialForm)
    }
    const handleRemoveMaterial = (material) => {
        setSelectedMaterials(selectedMaterials.filter((current) => (
            current !== material
        )))
        
        setMaterials(materials.filter((currentMaterial) => (
            currentMaterial !== material
        )))
    }
    const handleGenericChange = (e) => {
        let values = e.target.value.split(",")
        setGeneric({
            id: values[1],
            name: values[0]
        })
    }
    const handleSendOrder = async () => {
        providerForm['provider_id'] = providers[0].id
        

        let data = {
            sub_order:{
                status: 'case_managment',
                coded: true,
            },
            provider: {
              ...providerForm
            },
            materials: selectedMaterials,
            authenticity_token
        }
        if(comment){
            data['comment'] = {
                comment,
                user: username,
                module: module,
                date: current_date_time()
            }
        }
        if(newFiles.length > 0){
            let documents = []
            newFiles.map((file) => {
                documents.push({
                    document_type: file.document_type ? file.document_type : '',
                    preview: file.preview ? file.preview : '',
                    stored_at: file.stored_at ? file.stored_at : '',
                    user: username,
                    module: module
                })
            })
            data['documents'] = documents
        }

        if(validateIfCommentExist(comment)){
            let res = await put(`provider_negotiations/${sub_order.slug}`, data)
            res
              .json()
              .then(data => window.location.href = `/`)
              .catch(err => err)
        } else {
            notify('Recuerda capturar información en la bitacora','danger')
        }
    
    }

    return <React.Fragment>
        <OrderResume
            departureDate={departureDate}
            order={orderData}
            hospital={hospital}
            doctor={doctor}
            title={"Modulo de Negociación"}
            invisibles={[
                "deductible",
                "medical_fees",
                "surgical_event_date",
                "surgery_date"
            ]}
            form={resumeForm}
            handleChange={handleResumeChange}
        />
        <div>
            <div className="form-group">
            <label>Proveedor</label>
                <select className="form-control">
                    <option>Selecciona</option>
                    { providers.map((provider) => (
                        <option>{provider.name}</option>
                    )) }
                </select>
            </div>
            <div class="form-group">
                <label>Representante</label>
                <input 
                    type="text" 
                    class="form-control"
                    name="responsable"
                    value={providerForm.responsable}
                    onChange={handleProviderFormChange}
                />
            </div>
            <div class="form-group">
                <label>Contacto</label>
                <input 
                    type="text" 
                    class="form-control"
                    name="contact_number"
                    value={providerForm.contact_number}
                    onChange={handleProviderFormChange}
                />
            </div>
            <div className="form-group">
                <label>Tipo de Negociación</label>
                <select 
                    className="form-control"
                    name="provider_negotiation_type"
                    value={providerForm.provider_negotiation_type}
                    onChange={handleProviderFormChange} 
                >
                    <option>Selecciona</option>
                    <option>Insumo Nuevo</option>
                    <option>Proveedor Nuevo</option>
                    <option>Solicitud Extemporánea</option>
                </select>
            </div>
            <div class="form-group">
                <label>Código de Afiliación</label>
                <input 
                    type="text" 
                    class="form-control"
                    name="affiliate_code"
                    value={providerForm.affiliate_code}
                    onChange={handleProviderFormChange}
                />
            </div>
        </div>
      
        <table className="table table-bordered">
            <thead>
                <tr>
                <th scope="col">Generico</th>
                <th scope="col">Cantidad</th>
                <th scope="col">Código</th>
                <th scope="col">Descripción</th>
                <th scope="col">Marca</th>
                <th scope="col">Costo</th>
                <th scope="col">Utilizado Proveedor</th>
                <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <td scope="row">
                <div className="form-group mb-0">
                    <select 
                        className="form-control"
                        onChange={handleGenericChange}
                    >
                    <option disabled selected value>Selecciona</option>
                    { generics.map((generic) => (
                        <option 
                            multiple={true}
                            value={[generic.generic, generic.id]}
                        >
                            {generic.generic}
                        </option>
                    )) }
                    </select>
                </div>
                </td>
                    <td>
                    <div class="form-group mb-0">
                        <input 
                        className="form-control"
                        type="number"
                        min="1"
                        step="1"
                        placeholder="Cantidad"
                        name="quantity"
                        value={materialForm.quantity}
                        onChange={handleMaterialFormChange}
                        />
                    </div>
                    </td>
                    <td>
                    <div class="form-group mb-0">
                        <input 
                        className="form-control"
                        type="text"
                        placeholder="Código"
                        name="code"
                        value={materialForm.code}
                        onChange={handleMaterialFormChange}
                        />
                    </div>
                    </td>
                    <td>
                    <div class="form-group mb-0">
                        <input 
                        className="form-control"
                        type="text"
                        placeholder="Descripción"
                        name="description"
                        value={materialForm.description}
                        onChange={handleMaterialFormChange}
                        />
                    </div>
                    </td>
                    <td>
                    <div class="form-group mb-0">
                        <input 
                            className="form-control"
                            type="text"
                            placeholder="Marca"
                            name="brand"
                            value={materialForm.brand}
                            onChange={handleMaterialFormChange}
                        />
                    </div>
                    </td>
                    <td>
                        <div class="form-group mb-0">
                            <input 
                                className="form-control"
                                type="number"
                                min="1"
                                step="1"
                                placeholder="Costo"
                                name="cost"
                                value={materialForm.cost}
                                onChange={handleMaterialFormChange}
                            />
                        </div>
                    </td>
                    <td>
                    <div className="form-group mb-0">
                        <select 
                            className="form-control"
                            name="provider_used"
                            value={materialForm.provider_used}
                            onChange={handleMaterialFormChange}
                        >
                            <option>SI</option>
                            <option>NO</option>
                        </select>
                    </div>
                    </td>
                    <td>
                        <center>
                            <button 
                                type="button" 
                                className="btn btn-primary"
                                onClick={handleAddMaterial}
                            >
                                <i className="fas fa-plus"></i>
                            </button>
                        </center>
                    </td>
                </tr>

                {materials.map((material) => (
                    
                        <tr>
                            <th scope="row">{material.generic}</th>
                            <th scope="row">{material.quantity}</th>
                            <th scope="row">{ material.code }</th>
                            <th scope="row">{ material.description }</th>
                            <th scope="row">{ material.brand }</th>
                            <th scope="row">{ formatMoney(material.cost) }</th>
                            <th scope="row">{ material.provider_used }</th>
                            <th scope="row">
                                <center>
                                    <button 
                                    type="button" 
                                    className="btn btn-danger"
                                    onClick={() => handleRemoveMaterial(material)}
                                    >
                                        <i className="fa fa-trash"></i>
                                    </button>
                                </center>
                            </th>
                        </tr>
                    
                ))}
            </tbody>
        </table>
        <br/><br/>
        <DocumentsContainer 
            selectDocument={selectDocument}
            handleSelectDocumentChange={handleSelectDocumentChange}
            uploadBtn={uploadBtn}
            loader={loader}
            setLoader={setLoader}
            setFiles={setFiles}
            files={files}
            setNewFiles={setNewFiles}
            newFiles={newFiles}
            username={username}
            options={["HOJA DE REMISIÓN SELLADA", "CARTA COMERCIAL", "EVIDENCIA DE NEGOCIACIÓN"]}
            authenticity_token={authenticity_token}
            module={module}
        />
        <br/><br/>
        <Binnacle 
            comment={comment}
            handleComment={handleComment}
            comments={comments}
            username={username}
            module={module}
        />

        <button 
            onClick={handleSendOrder}
            type="button" 
            className="btn btn-success float-right  space-r"
        >
            Enviar a administración de casos
        </button>
    <Notifications/>
    </React.Fragment>
}

export default ProviderNegotiation