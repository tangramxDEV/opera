import React, {
    useState,
    useEffect
} from 'react'
import useHttp from '../hooks/useHttp'
import Loader from '../uicomponents/loader'
import Alert from '../uicomponents/alert'
import DatePicker, { registerLocale } from 'react-datepicker'
import es from "date-fns/locale/es"
registerLocale("es", es)

const OperationalConsultingProvider = () => {
    const FIRST_DATE = new Date()

    const { get, put } = useHttp()    
    const [ranges, setRanges] = useState(false)
    const [rangeStart, setRangeStart] = useState(FIRST_DATE)
    const [rangeEnd, setRangeEnd] = useState(FIRST_DATE)
    // Loader
    const [isLoading, setIsLoading] = useState(false)
    //Datepicker Component
    const [startDate, setStartDate] = useState(new Date())
    
    const [subOrders, setSubOrders] = useState([])
    const [hospitals, setHospitals] = useState([])
    const [doctors, setDoctors] = useState([])
    const [query, setQuery] = useState({
        doctor: '',
        hospital: '',
        invoice: '',
        cpo: ''
    })

    const [manageError] = useState('No se encontro informacion')
    const [alertVisible, setAlertVisible] = useState(false)

    useEffect(() => {
        const fetchLastOrders = async () => {
            setIsLoading(true)
            let res = await get(`operational_consulting_provider/last_ten`)
            res
              .json()
              .then(res => {
                    setSubOrders(res)
                    setIsLoading(false)
                })
              .catch(err => console.log(err))
        }
        const fetchHospitals = async () => {
            let res = await get(`/hospitals`)
            res
                .json()
                .then(res => setHospitals(res))
                .catch(err => console.log(err))
        }
        const fetchDoctors = async () => {
            let res = await get(`/doctors`)
            res
                .json()
                .then(res => setDoctors(res))
                .catch(err => console.log(err))
        }
        fetchLastOrders()
        fetchDoctors()
        fetchHospitals()
    }, [])

    const handleLaunchQuery = async (e) => {
        e.preventDefault()
        if (query.doctor && query.hospital){
            setIsLoading(true)
            var dateRanges = ''
            if(ranges){
                dateRanges = `&start=${rangeStart}&end=${rangeEnd}`
            }
            let res = await get(`/operational_consulting_providers?doctor=${query.doctor}&hospital=${query.hospital}&invoice=${query.invoice}&cpo=${query.cpo}${dateRanges}`)
            res
                .json()
                .then(res => {
                    setAlertVisible(false)
                    setSubOrders(res)
                    setIsLoading(false)
                    if(res.length === 0){
                        setAlertVisible(true)
                    }
                })
                .catch(err => console.log(err))

        } else {
            setAlertVisible(true)
            setIsLoading(false)
        }
    }

    const handleSelectChange = (e) => {
        setQuery({
            ...query,
            [e.target.name]: e.target.value
        })
    } 

    return <React.Fragment>
    <form onSubmit={handleLaunchQuery}>
        <div className="row">
            <div className="col s4">
                <div className="form-group">
                    <label>Hospital</label>
                    <select 
                        className="form-control" 
                        name="hospital"
                        onChange={handleSelectChange}
                    >
                        <option disabled selected defaultValue=""> SELECCIONA </option>
                        { hospitals.map((hospital, index) => (
                            <option 
                                key={index}
                                value={hospital.id}
                            >
                                {hospital.name}
                            </option>
                        )) }
                    </select>
                </div>
            </div>
            <div className="col s4">
                <div className="form-group">
                    <label>Doctor</label>
                    <select 
                        className="form-control" 
                        name="doctor"
                        onChange={handleSelectChange}
                    >
                        <option disabled selected defaultValue=""> SELECCIONA </option>
                        { doctors.map((doctor, index) => (
                            <option 
                                key={index}
                                value={doctor.id}
                            >
                                {doctor.name}
                            </option>
                        )) }
                    </select>
                </div>
            </div>

            <div className="col s4">
                <div className="form-group">
                    <label>Folio GNP</label>
                    <input 
                        className="form-control" 
                        name="invoice"
                        onChange={handleSelectChange}
                    />
                </div>
            </div>

            <div className="col s4">
                <div className="form-group">
                    <label>Folio CPO</label>
                    <input 
                        className="form-control" 
                        name="cpo"
                        onChange={handleSelectChange}
                    />
                </div>
            </div>
            
            <div className="col s4 space-t">
                <button 
                    className="btn btn-primary"
                    type="submit"
                    >
                    <i className="fas fa-search"></i>
                </button>
            </div>
        </div>

        <div className="row">
            <div className="col s3">
                <label className="space-r">Periodo del:</label>
                <DatePicker
                    locale="es"
                    className="form-control space-r"
                    selected={rangeStart}
                    onChange={date => {
                        setRangeStart(date)
                        setRanges(true)
                    }}
                    // minDate={minDate}
                    dateFormat="dd/MM/yyyy"
                    popperPlacement="right-start"
                />
                
                <label className="space-l">al:</label>
                <DatePicker
                    locale="es"
                    className="form-control space-l"
                    selected={rangeEnd}
                    onChange={date => setRangeEnd(date)}
                    // minDate={minDate}
                    dateFormat="dd/MM/yyyy"
                    popperPlacement="right-start"
                />
                
                
            </div>
        </div>

    </form>
                
    <Loader 
        visibility={isLoading}
    />
    <Alert
        manageable={false}
        visible={alertVisible}
        error={manageError}
    />

    <table className="table table-bordered">
        <thead>
        <tr>
            <th scope="col">Ticket CPO</th>
            <th scope="col">Tipo de Procedimiento</th>
            <th scope="col">Asegurado</th>
            <th scope="col">Médico Tratante</th>
            <th scope="col">Hospital</th>
            <th scope="col">Estado</th>
            <th scope="col">Procedimiento</th>
            <th scope="col">Opciones</th>
        </tr>
        </thead>
        <tbody>
            { subOrders.map((subOrder, index) => (
                <tr key={index}>
                    <th scope="row">{ subOrder.cpo }</th>
                    <td>{ subOrder.order.order_type }</td>
                    <td>{ subOrder.order.insured }</td>
                    <td>{ subOrder.order.doctor.name }</td>
                    <td>{ subOrder.order.hospital.name }</td>
                    <td>{ subOrder.order.hospital.state }</td>
                    <td>{ subOrder.order.procedure }</td>
                    <td>
                        <a href={`operational_consulting_providers/${subOrder.slug}`}>Ir</a>
                    </td>
                </tr>
            )) }
        </tbody>
</table>
</React.Fragment>
}

export default OperationalConsultingProvider