import React, {
  useState,
  useEffect
} from 'react'
import Binnacle from '../uicomponents/binnacle'
import OrderResume from '../uicomponents/order-resume'
import DocumentsContainer from '../uicomponents/documents_container'
import useHttp from '../hooks/useHttp'
import { 
  current_date_time,
} from '../functionalities/dates'
import useFormatter from '../hooks/useFormatter'
import useValidations from '../hooks/useValidations'
import Notifications, { notify } from '../modules/Notifications'
import useWordSeparator from '../hooks/useWordSeparator'
import jsPDF from 'jspdf'
import 'jspdf-autotable';
import PDFHeader from '../../../public/images/pdf-header'

const NewSupplies = ({sub_order, order, username, module, authenticity_token}) => {
  const { generateString } = useWordSeparator()
  const { formatMoney } = useFormatter()
  const { get, put } = useHttp()
  const {
    validateIfCommentExist,
    validateDocuments
  } = useValidations()
  // Documents Component
  const [selectDocument, setSelectDocument] = useState(null)
  const [uploadBtn, setUploadBtn] = useState('visible')
  const [loader, setLoader] = useState('hidden')
  const [files, setFiles] = useState([])
  const [newFiles, setNewFiles] = useState([])
  // Comments Component
  const [comment, setComment] = useState('')
  const [comments, setComments] = useState([])
  // Resume Component
  const [departureDate, setDepartureDate] = useState( 
    order.departure_date ?  new Date(order.departure_date) : undefined
    )
  const [resumeForm, setResumeForm] = useState({
      departure_date: order.departure_date ? order.departure_date.split(' ').join('T').substring(0, 16) : '',     
  })
  //
  const [orderData, setOrderData] = useState('')
  const [hospital, setHospital] = useState('')
  const [doctor, setDoctor] = useState('')
  const [subOrderGenerics, setSubOrderGenerics] = useState([])
  const [subOrder, setSubOrder] = useState({})
  const [providers, setProviders] = useState([])
  const [providerForm, setProviderForm] = useState({
      'provider_id': '',
      'responsable': '',
      'contact_number': '',
      'provider_negotiation_type': '',
      'affiliate_code': ''
  })
  const [negotiatedAmountForm, setNegotiatedAmountForm] = useState({
      negotiated_amount: 0
  })
  const [costs, setCosts] = useState([])
  const [total, setTotal] = useState(0)

  const handleComment= e => {
      setComment(e.target.value)
  }

  const handleProviderFormChange = (e) => {
      setProviderForm({
          ...providerForm,
          [e.target.name]: e.target.value
      })
  }

  const handleNegotiatedAmountChange = (e) => {
      setNegotiatedAmountForm({
        ...negotiatedAmountForm,
        [e.target.name]: e.target.value
      })
  }

  const handleResumeChange = (e) => {
      setResumeForm({
        ...resumeForm,
        [e.target.name]: e.target.value
      })
  }

  useEffect(() => {
      const fetchOrder = async () => {
          let res = await get(`material_negotiations/${sub_order.slug}`)
          res
            .json()
            .then(res => {
                console.log(res)
                  setPendingProviders(res)
                  setSubOrder(res)
                  validateGenerics(res.sub_order_generics)
                  setComments([
                      ...res.comments,
                      ...res.order.order_comments
                    ])
                  setOrderData(res.order)
                  setHospital(res.order.hospital)
                  setDoctor(res.order.doctor)
                  setTotal(res.total > 0 ? res.total : parseInt(res.to_pay))
                  setFiles([
                      ...res.documents,
                      ...res.order.order_documents
                    ])
              })
            .catch(err => console.log(err))
      }
      fetchOrder()
  }, [])

  const validateGenerics = (generics) => {
      let full_generics = []

      generics.map((generic) => {
          if(generic.sub_order_generals.length > 0)
              full_generics.push(generic)
      })

      setSubOrderGenerics(full_generics)
  }

  const handleCostChange = (e) => {
      let ids = e.target.name.split("-")
      setCosts({
          ...costs,
          [e.target.name]: {
              sub_order_general_id: ids[1],
              cost: e.target.value
          }
      })
  }

  const getGenericCost = (generalId) => {
      let cost = 0;
      subOrderGenerics.map((generic) => {
          generic.sub_order_generals.map((general) => {
              if(general.id == generalId){
                  cost = general.cost
              }
          })
      })

      return cost
  }

  const setPendingProviders = (sub_order) => {
      if(sub_order.provider){
          setProviders([
              ...providers,
              sub_order.provider
          ])
          setProviderForm({
              ...providerForm,
              ['responsable']: sub_order.provider.responsable,
              ['contact_number']: sub_order.provider.contact_number,
              ['contact_person']: sub_order.provider.contact_person,
              ['provider_negotiation_type']: sub_order.provider.provider_negotiation_type,
              ['affiliate_code']: sub_order.provider.affiliate_code
          })
      }
  }

  const handleSelectDocumentChange = (e) => {
      setSelectDocument(e.target.value)
  }
  const handlePDFGenerator = () => {
    let genericsList = []

    subOrderGenerics.map((generic) => {
      generic.sub_order_generals.map((generals) => {
        if(generals.authorized != "FUERA DE COBERTURA"){
            genericsList.push([
                generateString(generic.generic, 1),
                generateString(generals.provider_material.description),
                generals.provider_material.brand,
                generals.quantity,
                `$${formatMoney(generals.cost)}`
            ]) 
        }
      })
    })

    let newTotal = total - (subOrder.negotiated_amount ? subOrder.negotiated_amount : 0)
    genericsList.push([generateString('Monto Negociado (Descuento)',1), '', '', '', `$${formatMoney(subOrder.negotiated_amount ? subOrder.negotiated_amount : 0)}`])
    genericsList.push(['Total', '', '', '', `$${formatMoney(newTotal)}`])

    var doc = new jsPDF()
    doc.setFontSize(12)
    let today = new Date();
    var img = new Image()
    img.src = PDFHeader
    doc.addImage(img, 'JPEG', 15, 5, 250, 20)


    doc.setFontType("bold");
    doc.setFontSize(10)
    doc.text(170,10, `Fecha de impresión`)
    doc.setFontType("normal");
    doc.text(180,15,`${today.getDate()}-${today.getMonth() + 1}-${today.getFullYear()}`)

    doc.setFontSize(16)
    doc.setFontType("bold");
    doc.text(125, 30, 'Visado Entrega - Proveedor')
    doc.setFontSize(10)
    doc.text(150,35, `Ticket: ${orderData.cpo}`)

    doc.setFontSize(18)
    doc.setFontType("bold");
    doc.text(20, 45, `${providers[0].name}`)

    doc.setFontSize(12)
    doc.setFontType("bold");
    doc.text(20,55, `Asegurado:`)
    doc.setFontType("normal");
    doc.text(45,55, `${orderData.insured}`)

    doc.setFontType("bold");
    doc.text(20,60, `Folio GNP:`)
    doc.setFontType("normal");
    doc.text(45,60, `${orderData.gnp_invoice}`)

    doc.setFontType("bold");
    doc.text(20,65, `Reclamación:`)
    doc.setFontType("normal");
    doc.text(55,65, `${orderData.claim}`)

    doc.setFontType("bold");
    doc.text(20,70, `Médico Tratante:`)
    doc.setFontType("normal");
    doc.text(55,70, `${orderData.doctor.name}`)

    doc.setFontType("bold");
    doc.text(20,75, `Hospital:`)
    doc.setFontType("normal");
    doc.text(45,75, `${orderData.hospital.name}`)
    
    doc.setFontType("bold");
    doc.text(20,80, `Padecimiento:`)
    doc.setFontType("normal");
    doc.text(55,80, `${orderData.suffering}`)

    doc.setFontType("bold");
    doc.text(20,85, `Procedimiento:`)
    doc.setFontType("normal");
    var splitProcedure = doc.splitTextToSize(`${orderData.procedure}`, 150)
    doc.text(55,85, splitProcedure)

    doc.setFillColor('ED7D32');
    doc.rect(20, 100, 180, 1, "F");

    doc.setFontType("bold");
    doc.setFontSize(16)
    doc.text(20,108, `Insumos Entregados`)

    doc.setFontType("normal");

    let wantedTableWidth = 150;
    let pageWidth = doc.internal.pageSize.width;
    let margin = (pageWidth - wantedTableWidth) / 2;

    doc.autoTable({
        theme: 'striped',
        startY: 115,
        headStyles: {
          fillColor: [0, 32, 96],
          cellWidth: 30,
          fontSize: 8,
        },
        styles: {
          halign: 'center',
          cellWidth: 'wrap',
          cellPadding: {top: 4, bottom: 4},
          fontSize: 8,
        },
        head: [['Genérico', 'Descripción', 'Marca', 'Cantidad', 'Subtotal']],
        margin: {left: margin, right: margin},
        body: [...genericsList]
      })

    doc.save('solicitud.pdf')
  }

  const handleSendOrder = async (partial) => {
      providerForm['provider_id'] = providers[0].id
  
      let data = {
          message: partial ? 'Solicitud enviada a proveedor' : 'Guardado Exitoso',
          sub_order:{
              status: partial ? 'finished' : 'new_supplies',
              negotiated_amount: negotiatedAmountForm.negotiated_amount
          },
          provider: {
              ...providerForm
          },
          cost_list: {
              ...costs
          },
          authenticity_token
      }
      if(comment){
          data['comment'] = {
              comment,
              user: username,
              module: module,
              date: current_date_time()
          }
      }
      if(newFiles.length > 0){
          let documents = []
          newFiles.map((file) => {
              documents.push({
                  document_type: file.document_type ? file.document_type : '',
                  preview: file.preview ? file.preview : '',
                  stored_at: file.stored_at ? file.stored_at : '',
                  user: username,
                  module: module
              })
          })
          data['documents'] = documents
      }
    if(partial){
      if(validateIfCommentExist(comment)){
        let validation = validateDocuments(files, ["NOTA DE CRÉDITO", "EVIDENCIA DE NEGOCIACIÓN"])
        if(validation.errors){
            notify(validation.errors,'danger')
          } else {
            sendRequest(data)
          }
      } else {
        notify('Recuerda capturar información en la bitacora','danger')
      }
    } else {
      sendRequest(data)
    }  
  }
  
  const sendRequest = async (data) => {
    let res = await put(`new_supplies/${sub_order.slug}`, data)
    res
      .json()
      .then(data => window.location.href = `/`)
      .catch(err => err)
  }

  return <React.Fragment>
      <OrderResume
        departureDate={departureDate}
          order={orderData}
          hospital={hospital}
          doctor={doctor}
          title={"Modulo Negociación Nuevos Insumos"}
          invisibles={[
              "deductible",
              "medical_fees",
              "surgical_event_date",
              "surgery_date"
          ]}
          form={resumeForm}
          handleChange={handleResumeChange}
      />
        <button 
            className="btn btn-sm btn-primary space-r" 
            onClick={(e) => {
                e.preventDefault()
                window.open(`${sub_order.letter}`)
            }}
        >
        <i className="fas fa-envelope-open-text"></i>
        </button>
        CARTA DE AUTORIZACION   
        <br/><br/>
        <div>
            <button className="btn btn-sm btn-primary space-r" onClick={handlePDFGenerator}>
                <i className="fas fa-print"></i>
            </button>
            VISADO 
        </div>
      <div>
          <div className="form-group">
          <label>Proveedor</label>
              <select disabled className="form-control">
                  <option disabled>Selecciona</option>
                  { providers.map((provider) => (
                      <option disabled selected value>{provider.name}</option>
                  )) }
              </select>
          </div>
          <div class="form-group">
              <label>Representante</label>
              <input 
                  type="text" 
                  class="form-control"
                  name="responsable"
                  value={providerForm.contact_person}
                  disabled
              />
          </div>
          <div class="form-group">
              <label>Contacto</label>
              <input 
                  type="text" 
                  class="form-control"
                  name="contact_number"
                  value={providerForm.contact_number}
                  disabled
              />
          </div>
          <div className="form-group">
              <label>Tipo de Negociación</label>
              <select 
                  className="form-control"
                  name="provider_negotiation_type"
                  value={providerForm.provider_negotiation_type}
                  onChange={handleProviderFormChange} 
              >
                  <option>Selecciona</option>
                  <option>Insumo Nuevo</option>
                  <option>Proveedor Nuevo</option>
                  <option>Solicitud Extemporánea</option>
              </select>
          </div>
      </div>
      <br></br>

      {subOrderGenerics.map((generic) => (
          <React.Fragment>
              
              <table class="table table-bordered">
              <thead>
                  <tr>
                      <th style={{"width": "25%"}}>Descripcion Generica</th>
                      <th style={{"width": "5%"}}>Cantidad</th>
                      <th style={{"width": "10%"}}>Codigo</th>
                      <th style={{"width": "20%"}}>Descripcion</th>
                      <th style={{"width": "5%"}}>Costo</th>
                      <th style={{"width": "10%"}}>Costo Negociado</th>
                      <th style={{"width": "5%"}}>Utilizado Proveedor</th>
                      <th style={{"width": "5%"}}>Utilizado Supervision Hospitalaria</th>
                      <th style={{"width": "5%"}}>Autorizado Supervision Hospitalaria</th>
                  </tr>
              </thead>
              <tbody>
              { generic.sub_order_generals.map((general) => (
                  <tr>
                      <th>{generic.generic}</th>
                      <td>{general.quantity}</td>
                      <td>{general.provider_material.general_material.code}</td>
                      <td>{general.provider_material.general_material.description}</td>
                      <td>${formatMoney(general.cost)}</td>
                      <td>
                          <div class="form-group">
                              <input 
                                  disabled={!general.is_new}
                                  class="form-control"
                                  type="number"
                                  min="1"
                                  step="1"
                                  name={`${subOrder.id}-${general.id}`}
                                  placeholder={general.negotiated_amount}
                                  value={`${subOrder.id}-${general.id}` in costs ? costs[`${subOrder.id}-${general.id}`].cost : ''}
                                    onChange={(e) => {
                                    const re = /\d+(\.\d{1,2})?/
                                    if (re.test(e.target.value) && e.target.value > 0) {
                                      handleCostChange(e)
                                    }
                                  }}
                              />
                          </div>
                      </td>
                      <td>{general.provider_used}</td>
                      <td>{general.used}</td>
                      <td>{general.authorized}</td>
                  </tr>
                 )   ) }
              </tbody>
              </table>
          </React.Fragment>
      ))}

      
      <br/><br/>
      <DocumentsContainer 
          selectDocument={selectDocument}
          handleSelectDocumentChange={handleSelectDocumentChange}
          uploadBtn={uploadBtn}
          loader={loader}
          setLoader={setLoader}
          setFiles={setFiles}
          newFiles={newFiles}
          setNewFiles={setNewFiles}
          files={files}
          username={username}
          options={["EVIDENCIA DE NEGOCIACIÓN"]}
          authenticity_token={authenticity_token}
          module={module}
      />
      <br/><br/>
      <Binnacle 
          comment={comment}
          handleComment={handleComment}
          comments={comments}
          username={username}
          module={module}
      />

  <button 
      onClick={() => {handleSendOrder(true)}}
      type="button" 
      className="btn btn-primary float-right  space-r"
  >
      Enviar a Proveedor
  </button>
  <button 
      onClick={() => {handleSendOrder(false)}}
      type="button" 
      className="btn btn-success float-right  space-r"
  >
      <i className="fas fa-save"></i>
</button>
  <Notifications/>
  </React.Fragment>
}

export default NewSupplies