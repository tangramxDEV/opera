import React, {
    useEffect,
    useState
} from 'react'
import Binnacle from '../uicomponents/binnacle'
import OrderResume from '../uicomponents/order-resume'
import { current_date_time } from '../functionalities/dates'
import Notifications, { notify } from '../modules/Notifications'
import useHttp from '../hooks/useHttp'
import useOrderStatus from '../hooks/useOrderStatus'
import useOrderCodedValidations from '../hooks/useOrderCodedValidations'
import useButtonLoader from '../hooks/useButtonLoader'
import useValidations from '../hooks/useValidations'
import DocumentsContainer from '../uicomponents/documents_container'
import useFormatter from '../hooks/useFormatter'

const IntelligenceCenterOrder = ({order, sub_order, authenticity_token, username, module}) => {
    const { formatMoney } = useFormatter()
    const { setSending, validateSending } = useButtonLoader()
    const { get, put } = useHttp()    
    const { showStatus } = useOrderStatus()
    const { codedValidation } = useOrderCodedValidations()
    const { 
      validateInvoiceExists,
      validateIfCommentExist
    } = useValidations()

    const [orderData, setOrderData] = useState('')
    const [hospital, setHospital] = useState('')
    const [doctor, setDoctor] = useState('')
    const [invoice, setInvoice] = useState(false)
    // Documents Component
    const [selectDocument, setSelectDocument] = useState(null)
    const [uploadBtn, setUploadBtn] = useState('visible')
    const [loader, setLoader] = useState('hidden')
    const [files, setFiles] = useState([])
    const [newFiles, setNewFiles] = useState([])
    // Comments Component
    const [comment, setComment] = useState('')
    const [comments, setComments] = useState([])
    // Resume Component
    const [resumeForm, setResumeForm] = useState({
      departure_date: order.departure_date ? order.departure_date.split(' ').join('T').substring(0, 16) : '',
    })
    //
    const [subOrder, setSubOrder] = useState({provider: {},sub_order_generics:[] })
    const [invoiceForm, setInvoiceForm] = useState({
      invoice: ''
    })
    const [departureDate, setDepartureDate] = useState( 
      order.departure_date ?  new Date(order.departure_date) : undefined
    )
    const [surgicalDate, setSurgicalDate] = useState( 
        order.surgical_event_date ?  new Date(order.surgical_event_date) : new Date()
    )
    const handleResumeChange = (e) => {
        setResumeForm({
          ...resumeForm,
          [e.target.name]: e.target.value
        })
      }

    const handleComment= e => {
        setComment(e.target.value)
    }

    const sendOrder = async () => {
      setSending(true)
        let data = {
          message: 'Solicitud enviada a Dictamen',
          sub_order: {
            status: 'payment_provider'
          },
          authenticity_token
        }
        if(comment){
          data['comment'] = {
              comment,
              user: username,
              module: module,
              date: current_date_time()
          }
        }
        if(newFiles[0]){
          let documents = []
          newFiles.map((file) => {
              documents.push({
                  document_type: file.document_type ? file.document_type : '',
                  preview: file.preview ? file.preview : '',
                  stored_at: file.stored_at ? file.stored_at : '',
                  user: username,
                  module: module
              })
          })
          data['documents'] = documents
        }

        let res = await put(`intelligence_centers/${sub_order.slug}`, data)
        res
          .json()
          .then(data => window.location.href = `/`)
          .catch(err => err)
    }

    const handleSendOrder = async () => {
      if(validateInvoiceExists(subOrder)){
        if(validateIfCommentExist(comment)){
          sendOrder()
        } else {
          notify('Recuerda capturar información en la bitacora','danger')
        }
      } else {
        notify('Debes registrar los folios antes de continuar','danger')
      }
        
    }
    
    useEffect(() => {
      const fetchSubOrder = async () => {
        let res = await get(`intelligence_centers/${sub_order.slug}`)
        res
          .json()
          .then(data => {
            setSubOrder(data)
            ExistInvoiceIntoSubOrder(data)
            setComments(data.comments)
            setOrderData(data.order)
            setHospital(data.order.hospital)
            setDoctor(data.order.doctor)
            setFiles([
              ...data.documents,
              ...data.order.order_documents,
            ])
          })
          .catch(err => console.log(err))
      }
        fetchSubOrder()
    }, [])

  const ExistInvoiceIntoSubOrder = (subOrder) => {
    setInvoice(subOrder.invoice != null)
  }

  const handleSelectDocumentChange = (e) => {
    setSelectDocument(e.target.value)
  }

  const validateInvoiceExist = () => {
    if(invoiceForm.invoice != undefined){
      notify('Debes ingresar el folio','danger')
      return false
    } else {
      return true
    }
  }
  const handleSendInvoice = async () => {
    if(validateInvoiceExist()){
      let data = {
        sub_order: {
          invoice: invoiceForm
        },
        authenticity_token
      }
      setInvoiceForm({invoice: ''})

      let res = await put(`sub_orders/${subOrder.slug}`, data)
      res
        .json()
        .then(data => {
          let newSubOrder = subOrder
          newSubOrder['invoice'] = data.sub_order.invoice

          
          setSubOrder(newSubOrder);
          setInvoice(true)
          validateInvoice()
        })
        .catch(err => err)
    }
  }

  const validateInvoice = () => {
    if(invoice){
      return <p>
        FOLIO: {subOrder.invoice} 
      </p>
    }else {
      if(subOrder.coded){
        return <div className="form-row">
          <div className="col">
            <input 
              type="text" 
              className="form-control"
              placeholder="Folio"
              name="invoide"
              maxLength={32}
              value={invoiceForm.invoice}
              onChange={(e) => {
                setInvoiceForm(e.target.value)
              }}
            />
          </div>
          <div className="col">
            <button 
              className="btn btn-primary"
              onClick={() => {handleSendInvoice()}}
            >
              <i className="fas fa-save"></i>
            </button>
          </div>
          <br/><br/>
        </div>
      }
    }
  }
    return <React.Fragment>

        <OrderResume
          departureDate={departureDate}
          surgicalDate={surgicalDate}
          title={"Centro de Inteligencia"}
          order={orderData}
          hospital={hospital}
          doctor={doctor}
          subOrder={subOrder}
          provider={subOrder.provider}
          legends={true}

          invisibles={[
            "deductible",
            "medical_fees",
            "surgical_event_date",
            "surgery_date"
          ]}
          form={resumeForm}
          handleChange={handleResumeChange}
        />

        <br/><br/>
    
      <React.Fragment>
        <div className="card">
          <div className="card-body">
              <h5 className="card-title no-margin">
                  {subOrder.provider.name}
                  { codedValidation(subOrder.coded) }
                  </h5>
                  <br/>
                  { validateInvoice() }
            <div className="table-responsive">
              <table className="table table-bordered">
                  <thead>
                      <tr>
                      <th scope="col">Cantidad</th>
                      <th scope="col">Codigo</th>
                      <th scope="col">Descripción</th>
                      <th scope="col">Marca</th>
                      <th scope="col">Costo</th>
                      <th scope="col">Usado Proveedor</th>
                      <th scope="col">Autorizado Supervision Hospitalaria</th>
                      <th scope="col">Utilizado Supervision Hospitalaria</th>
                      <th scope="col">Observaciones</th>
                      </tr>
                  </thead>
                  <tbody>
                  { subOrder.sub_order_generics.map((generics) => (
                      generics.sub_order_generals.map((generals, index) => (
                          <tr key={index}>
                              <td scope="col">{generals.quantity}</td>
                              <td scope="col">
                                { generals.provider_material.general_material.code }
                              </td>
                              <td scope="col">
                              { generals.provider_material.general_material.description }
                              </td>
                              <td scope="col">{generals.provider_material.brand}</td>
                              <td scope="col">${formatMoney(generals.cost)}</td>
                              <td scope="col">{generals.provider_used}</td>
                              <td scope="col">
                                  {generals.autdorized}
                              </td>
                              <td scope="col">
                                  {generals.used}
                              </td>
                              <td scope="col">
                                  {generals.obvservations}
                              </td>
                          </tr>
                      ))
                  )) }
                  </tbody>
              </table>
            </div>
          </div>
        </div>
        <br/>
      </React.Fragment>
      <br/><br/>
      <DocumentsContainer 
          selectDocument={selectDocument}
          handleSelectDocumentChange={handleSelectDocumentChange}
          uploadBtn={uploadBtn}
          loader={loader}
          setLoader={setLoader}
          setFiles={setFiles}
          newFiles={newFiles}
          setNewFiles={setNewFiles}
          files={files}
          username={username}
          options={["HOJA DE REMISION SELLADA", "FACTURA DE DESCUENTO"]}
          authenticity_token={authenticity_token}
          module={module}
      />
      <br/><br/>
        <Binnacle 
            comment={comment}
            handleComment={handleComment}
            comments={comments}
            username={username}
            module={module}
        />
        
        { validateSending('Enviar a Dictamen', handleSendOrder) }


    <Notifications/>
    </React.Fragment>
}

export default IntelligenceCenterOrder