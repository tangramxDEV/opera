import React, {
    useState,
    useEffect
} from 'react'
import Binnacle from '../uicomponents/binnacle'
import DocumentsContainer from '../uicomponents/documents_container'
import OrderResume from '../uicomponents/order-resume'
import useHttp from '../hooks/useHttp'
import useFormatter from '../hooks/useFormatter'
import useLegends from '../hooks/useLegends'

const OperationalResult = ({order, documents, hospital, doctor}) => {
    const { buildLegend } = useLegends()
    const { formatMoney } = useFormatter()
    const { get, post, put } = useHttp()

    const [orderData, setOrderData] = useState({doctor: {}, hospital: {}})
    // Comments Component
    const [comment, setComment] = useState('')
    const [comments, setComments] = useState([])
    // Documents Component
    const [selectDocument, setSelectDocument] = useState(null)
    const [uploadBtn, setUploadBtn] = useState('visible')
    const [loader, setLoader] = useState('hidden')
    const [files, setFiles] = useState([])
    const [newFiles, setNewFiles] = useState([])
    // Resume Component
    const [departureDate, setDepartureDate] = useState( 
        order.departure_date ?  new Date(order.departure_date) : undefined
    )
    const [surgicalDate, setSurgicalDate] = useState( 
        order.surgical_event_date ?  new Date(order.surgical_event_date) : undefined
    )
    const [resumeForm, setResumeForm] = useState({
        coinsurance_percentage: order.coinsurance_percentage,
        coinsurance_money: order.coinsurance_money,
        deductible: order.deductible,
        medical_fees: order.medical_fees,
        departure_probable_date: order.departure_probable_date,
        // departure_date: order.departure_date ? order.departure_date.split(' ').join('T').substring(0, 16) : '',
        episode: order.episode,
        hospital_expense: order.hospital_expense,
        hospital_coinsurance: order.hospital_coinsurance,
        surgical_event_date: order.surgical_event_date
    })
    const [subOrders, setSubOrders] = useState([])

    const handleResumeChange = (e) => {
        setResumeForm({
          ...resumeForm,
          [e.target.name]: e.target.value
        })
    }

    const handleSelectDocumentChange = (e) => {
        setSelectDocument(e.target.value)
    }

    const handleComment= e => {
        setComment(e.target.value)
    }

    useEffect(() => {
      const fetchSubOrder = async () => {
        let res = await get(`orders/${order.slug}/sub_orders`)
        res
          .json()
          .then(data => mergeSubOrderExtras(data))
          .catch(err => err)
    }
        fetchSubOrder()
    }, [])

    const mergeSubOrderExtras = (subOrders) => {
        // console.log(documents)
        setOrderData(order)
        
        // console.log(subOrders)
        // let mergedComments = []
        // let mergedDocuments = [...documents]
        setSubOrders(subOrders)

        // subOrders.map((subOrder) => {
        //     subOrder.documents.push(documents)
            // mergedComments.push(...subOrder.comments)
            // mergedDocuments.push(...subOrder.documents)
        // })

        // setComments(mergedComments)
        // setFiles(mergedDocuments)
    }

    const showLetter = (subOrder) => {
        if(subOrder.letter){
            return <p>
                CARTA DE AUTORIZACIÓN 
                <a
                    className="space-l"
                    onClick={(e) => {
                    e.preventDefault()
                    window.open(`${subOrder.letter}`)
                    }}
                    href="#"
                >
                    <i className="fas fa-eye"></i>
                </a>
            </p>
        }
    }

    return <React.Fragment> 
    <OrderResume
        surgicalDate={surgicalDate}
        departureDate={departureDate}
        title={"Consulta Operativa"}
        module={'Consulta Operativa'}
        order={orderData}
        hospital={hospital}
        doctor={doctor}
        invisibles={[
            "entry_probable_date",
            "claim",
            "departure_probable_date",
            "insured",
        ]}
        handleChange={handleResumeChange}
        form={resumeForm}
    />
    <br/><br/>
    
    { subOrders.map((subOrder, index) => {
        console.log(subOrder.documents)
        return <React.Fragment key={index}>
            <hr/>
            <h4>{subOrder.provider.name}</h4>
            { showLetter(subOrder) }
            <h5>
                COASEGURO: 
                <span className="badge badge-primary">
                    { buildLegend(subOrder.provider, subOrder.order, subOrder, subOrder.order.hospital) }
                </span>
            </h5>
            <table className="table table-bordered">
                <thead>
                    <tr>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Codigo</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Marca</th>
                    <th scope="col">Costo</th>
                    <th scope="col">Utilizado Proveedor</th>
                    <th scope="col">Utilizado Supervision Hospitalaria</th>
                    <th scope="col">Autorizado Supervision Hospitalaria</th>
                    <th scope="col">Observaciones</th>
                    </tr>
                </thead>
                <tbody>
                { subOrder.sub_order_generics.map((generics) => (
                    generics.sub_order_generals.map((generals, index) => (
                        <tr key={index}>
                            <th scope="col">{generals.quantity}</th>
                            <th scope="col">
                              { generals.provider_material.general_material.code }
                            </th>
                            <th scope="col">
                            { generals.provider_material.general_material.description }
                            </th>
                            <th scope="col">{generals.provider_material.brand}</th>
                            <th scope="col">${formatMoney(generals.cost)}</th>
                            <th scope="col">{generals.provider_used}</th>
                            <th>
                                {generals.used}
                            </th>
                            <th scope="col">
                                {generals.authorized}
                            </th>
                            <th scope="col">
                                 {generals.obvservations}    
                            </th>
                        </tr>
                    ))
                )) }
                
                </tbody>
            </table>

        <DocumentsContainer 
            selectDocument={selectDocument}
            handleSelectDocumentChange={handleSelectDocumentChange}
            uploadBtn={uploadBtn}
            loader={loader}
            setLoader={setLoader}
            setFiles={setFiles}
            setNewFiles={setNewFiles}
            files={[...subOrder.documents,...documents]}
            username={null}
            options={[]}
            authenticity_token={null}
            only_view={true}
            module={module}
        />
        <br/><br/>
        <Binnacle 
            comment={null}
            handleComment={null}
            comments={subOrder.comments}
            username={null}
            module={null}
            only_view={true}
        />
        </React.Fragment>
    }) }

    </React.Fragment>
    
}

export default OperationalResult