import React, {
    useEffect,
    useState
} from 'react'
import { URL } from '../settings/configs'

const Dictums = () => {
    const [orders, setOrders] = useState([])

    useEffect(() => {
        const fetchOrders = async () => {
            try {
                let res = await fetch(`${URL}/orders?status=dictamen`)
                let data = await res.json()
                setOrders(data)
                console.log(data)
            } catch (error) {
                console.log(error)
            }
        }
        fetchOrders()
    }, [])

    return <React.Fragment>
    <table className="table table-bordered">
    <thead>
      <tr>
        <th scope="col">Ticket CPO</th>
        <th scope="col">Tipo de Programación</th>
        <th scope="col">Asegurado</th>
        <th scope="col">Folio GNP</th>
        <th scope="col">Reclamacion</th>
        <th scope="col">Fecha de evento quirurgico</th>
        <th scope="col">Procedimiento</th>
        <th scope="col">Opciones</th>
      </tr>
    </thead>
    <tbody>
    { orders.map((order) => (
        <tr>
          <th>CPOO71900001*</th>
          <td>Programacion de Cirugia*</td>
          <td>{ order.insured }</td>
          <td>{ order.gnp_invoice }</td>
          <td>{ order.claim }</td>
          <td>{ order.surgery_date }</td>
          <td>@mdo</td>
          <td>
            <a href={`/dictums/${order.id}`}>Ir</a>
          </td>
        </tr>
    )) }
    </tbody>
  </table>
    </React.Fragment>
}

export default Dictums