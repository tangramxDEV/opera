import React, {
    useState,
    useEffect
} from 'react'
import Binnacle from '../uicomponents/binnacle'
import OrderResume from '../uicomponents/order-resume'
import DocumentsContainer from '../uicomponents/documents_container'
import useHttp from '../hooks/useHttp'
import { 
    current_date_time,
} from '../functionalities/dates'
import useFormatter from '../hooks/useFormatter'
import useValidations from '../hooks/useValidations'
import Notifications, { notify } from '../modules/Notifications'

const MaterialNegotiation = ({sub_order, order, username, module, authenticity_token}) => {
    const { formatMoney } = useFormatter()
    const { get, put } = useHttp()
    // Documents Component
    const [selectDocument, setSelectDocument] = useState(null)
    const [uploadBtn, setUploadBtn] = useState('visible')
    const [loader, setLoader] = useState('hidden')
    const [files, setFiles] = useState([])
    const [newFiles, setNewFiles] = useState([])
    // Comments Component
    const [comment, setComment] = useState('')
    const [comments, setComments] = useState([])
    // Resume Component
    const [departureDate, setDepartureDate] = useState( 
        order.departure_date ?  new Date(order.departure_date) : undefined
    )
    const [resumeForm, setResumeForm] = useState({
        departure_date: order.departure_date ? order.departure_date.split(' ').join('T').substring(0, 16) : '',        
    })
    //
    const [orderData, setOrderData] = useState('')
    const [hospital, setHospital] = useState('')
    const [doctor, setDoctor] = useState('')
    const [subOrderGenerics, setSubOrderGenerics] = useState([])
    const [subOrder, setSubOrder] = useState({})
    const [providers, setProviders] = useState([])
    const [providerForm, setProviderForm] = useState({
        'provider_id': '',
        'responsable': '',
        'contact_number': '',
        'provider_negotiation_type': '',
        'affiliate_code': ''
    })
    const { 
        validateIfCommentExist
    } = useValidations()
    const [total, setTotal] = useState(0)

    const handleComment= e => {
        setComment(e.target.value)
    }

    const handleResumeChange = (e) => {
        setResumeForm({
          ...resumeForm,
          [e.target.name]: e.target.value
        })
    }

    useEffect(() => {
        const fetchOrder = async () => {
            let res = await get(`material_negotiations/${sub_order.slug}`)
            res
              .json()
              .then(res => {
                    setPendingProviders(res)
                    setSubOrder(res)
                    validateGenerics(res.sub_order_generics)
                    setComments(res.comments)
                    setOrderData(res.order)
                    setHospital(res.order.hospital)
                    setDoctor(res.order.doctor)
                    setTotal(res.total)
                    setFiles(res.documents)
                })
              .catch(err => console.log(err))
        }
        fetchOrder()
    }, [])

    const validateGenerics = (generics) => {
        let full_generics = []

        generics.map((generic) => {
            if(generic.sub_order_generals.length > 0)
                full_generics.push(generic)
        })

        setSubOrderGenerics(full_generics)
    }

    const setPendingProviders = (sub_order) => {
        if(sub_order.provider){
            setProviders([
                ...providers,
                sub_order.provider
            ])
            setProviderForm({
                ...providerForm,
                ['responsable']: sub_order.provider.responsable,
                ['contact_number']: sub_order.provider.contact_number,
                ['contact_person']: sub_order.provider.contact_person,
                ['provider_negotiation_type']: sub_order.provider.provider_negotiation_type,
                ['affiliate_code']: sub_order.provider.affiliate_code
            })
        }
    }

    const handleSelectDocumentChange = (e) => {
        setSelectDocument(e.target.value)
    }

    const handleSendOrder = async (partial) => {
        providerForm['provider_id'] = providers[0].id
    
        let data = {
            message: partial ? 'Solicitud enviada correctamente' : 'Guardado Exitoso',
            sub_order:{
                status: partial ? 'case_managment' : 'negotiable-matter',
                coded: partial,
            },
            authenticity_token
        }
        if(comment){
            data['comment'] = {
                comment,
                user: username,
                module: module,
                date: current_date_time()
            }
        }
        if(newFiles.length > 0){
            let documents = []
            newFiles.map((file) => {
                documents.push({
                    document_type: file.document_type ? file.document_type : '',
                    preview: file.preview ? file.preview : '',
                    stored_at: file.stored_at ? file.stored_at : '',
                    user: username,
                    module: module
                })
            })
            data['documents'] = documents
        }
        
        if(validateIfCommentExist(comment)){
            let res = await put(`material_negotiations/${sub_order.slug}`, data)
            res
              .json()
              .then(data => window.location.href = `/`)
              .catch(err => err)
        } else {
            notify('Recuerda capturar información en la bitacora','danger')
        }
        
    }

    return <React.Fragment>
        <OrderResume
            departureDate={departureDate}
            order={orderData}
            hospital={hospital}
            doctor={doctor}
            title={"Modulo de Negociación"}
            invisibles={[
                "deductible",
                "medical_fees",
                "surgical_event_date",
                "surgery_date"
            ]}
            form={resumeForm}
            handleChange={handleResumeChange}
        />

        <div>
            <div className="form-group">
            <label>Proveedor</label>
                <select disabled className="form-control">
                    <option disabled>Selecciona</option>
                    { providers.map((provider) => (
                        <option disabled selected value>{provider.name}</option>
                    )) }
                </select>
            </div>
            <div class="form-group">
                <label>Representante</label>
                <input 
                    type="text" 
                    class="form-control"
                    name="responsable"
                    value={providerForm.contact_person}
                    disabled
                />
            </div>
            <div class="form-group">
                <label>Contacto</label>
                <input 
                    type="text" 
                    class="form-control"
                    name="contact_number"
                    value={providerForm.contact_number}
                    disabled
                />
            </div>
        </div>
        <br></br>

        {subOrderGenerics.map((generic) => (
            <React.Fragment>
                <div className="table-responsive">
                <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style={{"width": "35%"}}>Descripcion Generica</th>
                        <th style={{"width": "5%"}}>Cantidad</th>
                        <th style={{"width": "15%"}}>Codigo</th>
                        <th style={{"width": "35%"}}>Descripcion</th>
                        <th style={{"width": "5%"}}>Costo</th>
                        <th style={{"width": "5%"}}>Autorizado Dictamen Medico</th>
                    </tr>
                </thead>
                <tbody>
                { generic.sub_order_generals.map((general) => (
                    <tr>
                        <th>{generic.generic}</th>
                        <td>{general.quantity}</td>
                        <td>{general.provider_material.general_material.code}</td>
                        <td>{general.provider_material.general_material.description}</td>
                        <td>${formatMoney(general.cost)}</td>
                        <td>{generic.authorized}</td>
                    </tr>
                )) }
                </tbody>
                </table>
                </div>
            </React.Fragment>
        ))}

        
        <br/><br/>
        <DocumentsContainer 
            selectDocument={selectDocument}
            handleSelectDocumentChange={handleSelectDocumentChange}
            uploadBtn={uploadBtn}
            loader={loader}
            setLoader={setLoader}
            setFiles={setFiles}
            newFiles={newFiles}
            setNewFiles={setNewFiles}
            files={files}
            username={username}
            options={["HOJA DE REMISION SELLADA", "FACTURA DE DESCUENTO"]}
            authenticity_token={authenticity_token}
            module={module}
        />
        <br/><br/>
        <Binnacle 
            comment={comment}
            handleComment={handleComment}
            comments={comments}
            username={username}
            module={module}
        />

    <button 
        onClick={() => {handleSendOrder(true)}}
        type="button" 
        className="btn btn-primary float-right  space-r"
    >
        Enviar a administración de casos
    </button>
    <button 
        onClick={() => {handleSendOrder(false)}}
        type="button" 
        className="btn btn-success float-right  space-r"
    >
        <i className="fas fa-save"></i>
  </button>
    <Notifications/>
    </React.Fragment>
}

export default MaterialNegotiation