import React, {
    useEffect,
    useState
} from 'react'
import Binnacle from '../uicomponents/binnacle'
import DocumentsContainer from '../uicomponents/documents_container'
import OrderResume from '../uicomponents/order-resume'
import Notifications, { notify } from '../modules/Notifications'
import { current_date_time } from '../functionalities/dates'
import useHttp from '../hooks/useHttp'
import useOrderStatus from '../hooks/useOrderStatus'
import useValidations from '../hooks/useValidations'
import useButtonLoader from '../hooks/useButtonLoader'
import { useForm, FormContext } from "react-hook-form"
import useFormatter from '../hooks/useFormatter'

const HospitalSupervisionOrder = ({sub_order, order, authenticity_token, username, module}) => {
    const { formatMoney } = useFormatter()
    const methods = useForm()
    const { register, handleSubmit } = methods
    const { setSending, validateSending } = useButtonLoader()
    const { get, put } = useHttp()
    const { showStatus } = useOrderStatus()
    const { 
        validateHospitalSupervisionFields,
        validateSubOrderCoded,
        validateIfCommentExist,
        validateDocuments
    } = useValidations()

    const [observations, setObservations] = useState([])
    const [authorizations, setAuthorizations] = useState([])
    const [useds, setUseds] = useState([])
    const [subOrder, setSubOrder] = useState({provider: {}, sub_order_generics: []})
    const [orderData, setOrderData] = useState('')
    const [hospital, setHospital] = useState('')
    const [doctor, setDoctor] = useState('')
    const [providers, setProviders] = useState([])
    // Comments Component
    const [comment, setComment] = useState('')
    const [comments, setComments] = useState([])
    // Documents Component
    const [selectDocument, setSelectDocument] = useState(null)
    const [uploadBtn, setUploadBtn] = useState('visible')
    const [loader, setLoader] = useState('hidden')
    const [files, setFiles] = useState([])
    const [newFiles, setNewFiles] = useState([])

    // Resume Component
    const [departureDate, setDepartureDate] = useState( 
        order.departure_date ?  new Date(order.departure_date) : undefined
    )
    const [surgicalDate, setSurgicalDate] = useState( 
        order.surgical_event_date ?  new Date(order.surgical_event_date) : undefined
    )

    const [resumeForm, setResumeForm] = useState({
        coinsurance_percentage: order.coinsurance_percentage,
        coinsurance_money: order.coinsurance_money,
        deductible: order.deductible,
        medical_fees: order.medical_fees,
        departure_probable_date: order.departure_probable_date,
        // departure_date: order.departure_date ? order.departure_date.split(' ').join('T').substring(0, 16) : '',
        episode: order.episode,
        hospital_expense: order.hospital_expense,
        hospital_coinsurance: order.hospital_coinsurance,
        // surgical_event_date: order.surgical_event_date
    })

    const handleSelectDocumentChange = (e) => {
        setSelectDocument(e.target.value)
    }

    const handleComment= e => {
        setComment(e.target.value)
    }
    
    const codedValidation = (coded) => {
        if(coded) {
            return <div className="float-right">
                <div style={{'color':'green'}}>
                    <h5>
                        <i className="fas fa-check-circle"></i>
                         Solicitud Codificada
                    </h5>
                </div>
            </div>
            } else {
            return <div className="float-right">
                <div style={{'color':'red'}}>
                    <h5>
                        <i className="fas fa-exclamation-circle"></i>
                         Solicitud No Codificada
                    </h5>
                </div>
            </div>
        }
    }
    const validateObservations = () => {
        let rows = 0
        let coded = 0

        
        subOrder.sub_order_generics.map((generics) => {
            generics.sub_order_generals.map((generals) => {
                if(generals.obvservations != null || generals.authorized != null || generals.used != null){
                    coded = coded + 1
                }
                    rows = rows + 1
            })
        })
        
        if((coded + Object.keys(observations).length) >= rows){
            return true
        } else {
            return false
        }
    }
    const handleSendOrder = (partial) => {
        if(!partial){
            sendOrder(partial)
        } else {
            const { validation, errors } = validateHospitalSupervisionFields(order, resumeForm, departureDate)
            
            if(validation){
                if(validateSubOrderCoded(subOrder)){
                    if(validateObservations(subOrder)){
                        sendOrder(partial)
                    } else {
                        setSending(false)    
                        notify('Parece que no ingresaste las observaciones','danger')
                    }
                } else {
                    setSending(false)
                    notify('Parece que los proveedores aún no codifican','danger')
                }
            }
            else {
                setSending(false)
                notify(errors,'danger')
            }
        }
    }
    
    const sendOrder = async (partial) => {
        setSending(true)
        let supervision = []
        
        subOrder.sub_order_generics.map((generics) => {
            generics.sub_order_generals.map((generals, index) => {
                supervision.push({
                    general_material_id: generals.id,
                    observation: `${subOrder.id}-${generals.id}` in observations ? observations[`${subOrder.id}-${generals.id}`].observation : generals.obvservations,
                    authorization: `${subOrder.id}-${generals.id}` in authorizations ? authorizations[`${subOrder.id}-${generals.id}`].authorization : generals.authorized,
                    used: `${subOrder.id}-${generals.id}` in useds ? useds[`${subOrder.id}-${generals.id}`].used : generals.used
                })
            })
        })
        // Floats
        order['deductible'] = resumeForm.deductible ? Number.parseFloat(resumeForm.deductible.replace(/,/g, '')) : ''
        order['coinsurance_money'] = resumeForm.coinsurance_money ? Number.parseFloat(resumeForm.coinsurance_money.replace(/,/g, '')) : ''
        order['medical_fees'] = resumeForm.medical_fees ? Number.parseFloat(resumeForm.medical_fees.replace(/,/g, '')) : ''
        order['hospital_expense'] = resumeForm.hospital_expense ? Number.parseFloat(resumeForm.hospital_expense.replace(/,/g, '')) : ''
        //
        order['coinsurance_percentage'] = resumeForm.coinsurance_percentage
        order['departure_date'] = departureDate
        order['episode'] = resumeForm.episode
        order['hospital_coinsurance'] = resumeForm.hospital_coinsurance
        
        let data = {
            message: partial ? 'Solicitud enviada a CDI' : 'Guardado exitoso',
            order: {
                ...order
            },
            sub_order: {
                status: partial ? 'cdi' : 'hospital_supervision'
            },
            supervision,
            authenticity_token
        }
        
        if(comment){
            data['comment'] = {
                comment,
                user: username,
                module: module,
                date: current_date_time()
            }
        }

        if(newFiles.length > 0){
            let documents = []
            newFiles.map((file) => {
                documents.push({
                    document_type: file.document_type ? file.document_type : '',
                    preview: file.preview ? file.preview : '',
                    stored_at: file.stored_at ? file.stored_at : '',
                    user: username,
                    module: module
                })
            })
            data['documents'] = documents
        }
            
        if(validateIfCommentExist(comment)){
            partial ? validateSupervisionDataRegistered(supervision, data) : sendRequest(data)

        } else {
            setSending(false)    
            notify('Recuerda capturar información en la bitacora','danger')
        }

}

const handleDepartureDate = (date) => {
    setDepartureDate(date)
}

const validateSupervisionDataRegistered = (supervisions, data) => {
    let validation = validateDocuments(files, ["SABANA QUIRURGICA", "NOTA POSTQUIRURGICA"])
    
    if(validation.errors){
      setSending(false)    
      notify(validation.errors,'danger')
    } else {
        setSending(false)    
      let totalGenerics = 0
      let observationRegistereds = 0
      let usedRegistereds = 0
      let authorizedRegistereds = 0
  
      subOrder.sub_order_generics.map((generics) => {
          generics.sub_order_generals.map((generals, index) => {
              totalGenerics = totalGenerics + 1
              if(generals.obvservations != null){ observationRegistereds = observationRegistereds + 1 }
              if(generals.authorized != null){ authorizedRegistereds = authorizedRegistereds + 1 }
              if(generals.used != null){ usedRegistereds = usedRegistereds + 1 }
          })
      })
  
      if (totalGenerics == observationRegistereds && 
          totalGenerics == usedRegistereds &&
          totalGenerics == authorizedRegistereds) {
              sendRequest(data)
          }
      else {
          supervisions.map((supervision) => {
              if(supervision.observation != null){ observationRegistereds = observationRegistereds + 1 }
              if(supervision.authorization != null){ authorizedRegistereds = authorizedRegistereds + 1 }
              if(supervision.used != null){ usedRegistereds = usedRegistereds + 1 }
          })
  
          if (totalGenerics == observationRegistereds && 
              totalGenerics == usedRegistereds &&
              totalGenerics == authorizedRegistereds) {
                  sendRequest(data)
              }
          else {
              setSending(false)    
              notify('Parece que no ingresaste la información correspondiente a supervision hospitalaria','danger')
          }
      }
    }
}

const sendRequest = async (data) => {
    let res = await put(`hospital_supervisions/${sub_order.slug}`, data)
        res
        .json()
        .then(data => window.location.href = `/`)
        .catch(err => err)
}
    
    useEffect(() => {
        const fetchSubOrder = async () => {
            let res = await get(`hospital_supervisions/${sub_order.slug}`)
            res 
                .json()
                .then(data => {
                    setSubOrder(data)
                    setComments(data.comments)
                    setFiles([
                        ...data.documents,
                        ...data.order.order_documents,
                    ])
                    setOrderData(data.order)
                    setHospital(data.order.hospital)
                    setDoctor(data.order.doctor)
                })
                .catch(err => console.log(err))
        }
        fetchSubOrder()
    }, [])

    const handleUsedChange = (e) => {
        let ids = e.target.name.split("-")
        setUseds({
            ...useds,
            [e.target.name]: {
                used: e.target.value,
                general_material_id: ids[1]
            }
        })
    }
    const handleSelectChange = (e) => {
        let ids = e.target.name.split("-")
        setAuthorizations({
            ...authorizations,
            [e.target.name]: {
                authorization: e.target.value,
                general_material_id: ids[1]
            }
        })
    }

    const handleChange = (e) => {
        let ids = e.target.name.split("-")
        setObservations({
            ...observations,
            [e.target.name]: {
                observation: e.target.value,
                general_material_id: ids[1]
            }
        })
    }
    const generateValue = (general) => {
        if(observations[`${subOrder.id}-${general.id}`]){
            return observations[`${subOrder.id}-${general.id}`].observation
        } else {
            return general.observations
        }
    }

    const handleResumeChange = (e) => {
        setResumeForm({
          ...resumeForm,
          [e.target.name]: e.target.value
        })
    }

    return <React.Fragment>
    <FormContext {...methods} >
    <OrderResume
        surgicalDate={surgicalDate}
        departureDate={departureDate}
        handleDepartureDate={handleDepartureDate}
        order={orderData}
        hospital={hospital}
        doctor={doctor}
        title={"Supervision Hospitalaria"}
        expense={[
            "hospital_expense",
            "medical_fees",
            "deductible",
            "hospital_coinsurance",
            "coinsurance_money",
            "coinsurance_percentage",
        ]}
        invisibles={[
            "entry_probable_date",
            "claim",
            "departure_probable_date",
            "insured"
        ]}
        editable={[
            "wf_invoice",
            "evo_invoice",
            "deductible",
            "medical_fees",
            "episode",
            "hospital_expense",

            "hospital_coinsurance",
            "coinsurance_money",
            "coinsurance_percentage",
            
            "departure_date",
            "medical_fees"
        ]}
        validable={[
            "episode",
        ]}
        handleChange={handleResumeChange}
        form={resumeForm}
        module={'HospitalSupervision'}
    />
    <br/><br/>
        <React.Fragment>
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title no-margin">
                        {subOrder.provider.name}
                        { codedValidation(subOrder.coded) }
                    </h5>
                    { showStatus(subOrder.status) }
                    
                    <div className="table-responsive">
                        <table className="table table-bordered">
                            <thead>
                                <tr>
                                <th scope="col">Cantidad</th>
                                <th scope="col">Descripción</th>
                                <th scope="col">Codigo</th>
                                <th scope="col">Marca</th>
                                <th scope="col">Costo</th>
                                <th scope="col">Utilizado Supervision Hospitalaria</th>
                                <th scope="col">Autorizado Supervision Hospitalaria</th>
                                <th scope="col">Observaciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            { subOrder.sub_order_generics.map((generics) => (
                                generics.sub_order_generals.map((generals, index) => (
                                    <tr key={index}>
                                        <th scope="col">{generals.quantity}</th>
                                        <th scope="col">
                                        { generals.provider_material.general_material.description }
                                        </th>
                                        <th scope="col">
                                        { generals.provider_material.general_material.code }
                                        </th>
                                        <th scope="col">{generals.provider_material.brand}</th>
                                        <th scope="col">${formatMoney(generals.cost)}</th>
                                        <th>
                                            <div className="form-group">
                                                <select 
                                                    
                                                    className="form-control" 
                                                    onChange={handleUsedChange}
                                                    name={`${subOrder.id}-${generals.id}`}
                                                >
                                                    <option disabled selected value> {generals.used || 'SELECCIONA'} </option>
                                                    <option>SI</option>
                                                    <option>NO</option>
                                                </select>
                                            </div>
                                        </th>
                                        <th scope="col">
                                            <div className="form-group">
                                                <select 
                                                    className="form-control" 
                                                    id="exampleFormControlSelect1"
                                                    onChange={handleSelectChange}
                                                    name={`${subOrder.id}-${generals.id}`}
                                                >
                                                    <option disabled selected value> {generals.authorized || 'SELECCIONA'} </option>
                                                    <option>SI</option>
                                                    <option>FUERA DE COBERTURA</option>
                                                    <option>SOBREUTILIZACION</option>
                                                    <option>INSUMO SIN JUSTIFICACION</option>
                                                </select>
                                            </div>
                                        </th>
                                        <th scope="col">
                                            <div className="form-group">
                                                <textarea 
                                                    name={`${subOrder.id}-${generals.id}`}
                                                    type="text" 
                                                    className="form-control"
                                                    onChange={handleChange}
                                                    placeholder={generals.obvservations}
                                                />
                                            </div>
                                        </th>
                                    </tr>
                                ))
                            )) }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br/>   
        </React.Fragment>
    
    <br/>
    <DocumentsContainer 
        selectDocument={selectDocument}
        handleSelectDocumentChange={handleSelectDocumentChange}
        uploadBtn={uploadBtn}
        loader={loader}
        setLoader={setLoader}
        setFiles={setFiles}
        newFiles={newFiles}
        setNewFiles={setNewFiles}
        files={files}
        username={username}
        options={["SABANA QUIRURGICA", "NOTA POSTQUIRURGICA"]}
        authenticity_token={authenticity_token}
        module={module}
    />
    <br/><br/>
    <Binnacle 
        comment={comment}
        handleComment={handleComment}
        comments={comments}
        username={username}
        module={module}
    />

    { validateSending('Enviar a CDI', handleSendOrder) }

    <button 
        onClick={() => {handleSendOrder(false)}}
        type="button" 
        className="btn btn-success float-right  space-r"
    >
        <i className="fas fa-save"></i>
    </button>

    <Notifications/>
    </FormContext>
    </React.Fragment>
}

export default React.memo(HospitalSupervisionOrder)