import React, {
    useState,
    useEffect
} from 'react'
import { URL } from '../settings/configs'
import Alert from '../uicomponents/alert'
import OrdersList from '../uicomponents/orders-list'
import Loader from '../uicomponents/loader'
import useHttp from '../hooks/useHttp'
import DatePicker, { registerLocale } from 'react-datepicker'
import es from "date-fns/locale/es"
registerLocale("es", es)

const OperationalConsulting = () => {
    const FIRST_DATE = new Date()
    const { get } = useHttp()
    // Loader
    const [isLoading, setIsLoading] = useState(false)
    //Datepicker Component
    const [startDate, setStartDate] = useState(new Date())
    const [ranges, setRanges] = useState(false)
    const [rangeStart, setRangeStart] = useState(FIRST_DATE)
    const [rangeEnd, setRangeEnd] = useState(FIRST_DATE)
    //
    const [orders, setOrders] = useState([])
    const [hospitals, setHospitals] = useState([])
    const [doctors, setDoctors] = useState([])
    const [query, setQuery] = useState({
        doctor: '',
        hospital: '',
        insured: '',
        claim: '',
        invoice: '',
        cpo: ''
    })
    const [manageError] = useState('No se encontro informacion')
    const [alertVisible, setAlertVisible] = useState(false)
    const [minDate, setMinDate] = useState(FIRST_DATE)

    // GNP-1920
    // REC-1920
    useEffect(() => {
        const fetchLastOrders = async () => {
            setIsLoading(true)
            let res = await get(`/operational_consulting/last_ten`)
            res
              .json()
              .then(res => {
                    setOrders(res)
                    setIsLoading(false)
                })
              .catch(err => console.log(err))
        }
        const fetchHospitals = async () => {
            let res = await get(`/hospitals`)
            res
              .json()
              .then(res => setHospitals(res))
              .catch(err => console.log(err))
        }
        const fetchDoctors = async () => {
            let res = await get(`/doctors`)
            res
              .json()
              .then(res => setDoctors(res))
              .catch(err => console.log(err))
        }
        fetchLastOrders()
        fetchDoctors()
        fetchHospitals()
    }, [])

    const handleLaunchQuery = async (e) => {
        e.preventDefault()
        if (query.doctor && query.hospital){
            setIsLoading(true)
            var dateRanges = ''
            if(ranges){
                dateRanges = `&start=${rangeStart}&end=${rangeEnd}`
            }

            let res = await get(`/operational_consultings?doctor=${query.doctor}&hospital=${query.hospital}&invoice=${query.invoice}&insured=${query.insured}&claim=${query.claim}&cpo=${query.cpo}${dateRanges}`)
            res
                .json()
                .then(res => {
                    setAlertVisible(false)
                    setOrders(res)
                    setIsLoading(false)
                    if(res.length === 0){
                        setAlertVisible(true)
                    }
                })
                .catch(err => console.log(err))
        } else {
            setAlertVisible(true)
            setIsLoading(false)
        }
    }

    const handleSelectChange = (e) => {
        setQuery({
            ...query,
            [e.target.name]: e.target.value
        })
    } 

    return <React.Fragment>
    <form onSubmit={handleLaunchQuery}>
        <div className="row">
            <div className="col s4">
                <div className="form-group">
                    <label>Hospital</label>
                    <select 
                        className="form-control" 
                        name="hospital"
                        onChange={handleSelectChange}
                    >
                        <option disabled selected defaultValue=""> SELECCIONA </option>
                        { hospitals.map((hospital, index) => (
                            <option 
                                key={index}
                                value={hospital.id}
                            >
                                {hospital.name}
                            </option>
                        )) }
                    </select>
                </div>
            </div>
            <div className="col s4">
                <div className="form-group">
                    <label>Doctor</label>
                    <select 
                        className="form-control" 
                        name="doctor"
                        onChange={handleSelectChange}
                    >
                        <option disabled selected defaultValue=""> SELECCIONA </option>
                        { doctors.map((doctor, index) => (
                            <option 
                                key={index}
                                value={doctor.id}
                            >
                                {doctor.name}
                            </option>
                        )) }
                    </select>
                </div>
            </div>
            <div className="col s4">
                <div className="form-group">
                    <label>Folio GNP</label>
                    <input 
                        className="form-control" 
                        name="invoice"
                        onChange={handleSelectChange}
                    />
                </div>
            </div>
            <div className="col s4">
                <div className="form-group">
                    <label>Asegurado</label>
                    <input 
                        className="form-control" 
                        name="insured"
                        onChange={handleSelectChange}
                    />
                </div>
            </div>
            <div className="col s4">
                <div className="form-group">
                    <label>Reclamación</label>
                    <input 
                        className="form-control" 
                        name="claim"
                        onChange={handleSelectChange}
                    />
                </div>
            </div>
            <div className="col s4">
                <div className="form-group">
                    <label>Ticket CPO</label>
                    <input 
                        className="form-control" 
                        name="cpo"
                        onChange={handleSelectChange}
                    />
                </div>
            </div>

            <div className="col s4 space-t">
                <button 
                    className="btn btn-primary"
                    type="submit"
                >
                    <i class="fas fa-search"></i>
                </button>
            </div>
        </div>
        <div className="row">
            <div className="col s3">
                <label className="space-r">Periodo del:</label>
                <DatePicker
                    locale="es"
                    className="form-control space-r"
                    selected={rangeStart}
                    onChange={date => {
                        setRangeStart(date)
                        setRanges(true)
                    }}
                    // minDate={minDate}
                    dateFormat="dd/MM/yyyy"
                    popperPlacement="right-start"
                />
                
                <label className="space-l">al:</label>
                <DatePicker
                    locale="es"
                    className="form-control space-l"
                    selected={rangeEnd}
                    onChange={date => setRangeEnd(date)}
                    // minDate={minDate}
                    dateFormat="dd/MM/yyyy"
                    popperPlacement="right-start"
                />
                
                
            </div>
        </div>

    </form>
    <Loader 
        visibility={isLoading}
    />
    <Alert
        manageable={false}
        visible={alertVisible}
        error={manageError}
    />
    <OrdersList 
        orders={orders}
        route={`/operational_consultings`}
    />
</React.Fragment>
}

export default OperationalConsulting