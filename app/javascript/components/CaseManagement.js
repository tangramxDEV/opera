import React, {
    useState,
    useEffect
} from 'react'
import { URL } from '../settings/configs'
import Binnacle from '../uicomponents/binnacle'
import DocumentsContainer from '../uicomponents/documents_container'
import OrderResume from '../uicomponents/order-resume'
import { current_date_time } from '../functionalities/dates'
import Notifications, { notify } from '../modules/Notifications'
import useHttp from '../hooks/useHttp'
import useValidations from '../hooks/useValidations'
import useFormatter from '../hooks/useFormatter'

const CaseManagement = ({order, sub_order, authenticity_token, username, module}) => {
    const { formatMoney } = useFormatter()
    const { 
        validateHospitalSupervisionFields,
        validateIfCommentExist,
        validateDocuments
    } = useValidations()

    const { get, put } = useHttp()    
    const [observations, setObservations] = useState([])
    const [authorizations, setAuthorizations] = useState([])
    const [useds, setUseds] = useState([])
    const [orderData, setOrderData] = useState('')
    const [hospital, setHospital] = useState('')
    const [doctor, setDoctor] = useState('')
    const [subOrder, setSubOrder] = useState({
        provider: '',
        sub_order_generics: []
    })
    // Comments Component
    const [comment, setComment] = useState('')
    const [comments, setComments] = useState([])
    // Documents Component
    const [selectDocument, setSelectDocument] = useState(null)
    const [uploadBtn, setUploadBtn] = useState('visible')
    const [loader, setLoader] = useState('hidden')
    const [files, setFiles] = useState([])
    const [newFiles, setNewFiles] = useState([])
    // Resume Component
    const [departureDate, setDepartureDate] = useState( 
        order.departure_date ?  new Date(order.departure_date) : undefined
    )
    const [resumeForm, setResumeForm] = useState({
        wf_invoice: order.wf_invoice,
        evo_invoice: order.evo_invoice,
        coinsurance_percentage: order.coinsurance_percentage,
        coinsurance_money: order.coinsurance_money,
        deductible: order.deductible,
        medical_fees: order.medical_fees,
        departure_probable_date: order.departure_probable_date,
        // departure_date: order.departure_date ? order.departure_date.split(' ').join('T').substring(0, 16) : '',
        episode: order.episode,
        hospital_expense: order.hospital_expense,
        hospital_coinsurance: order.hospital_coinsurance,
        surgical_event_date: order.surgical_event_date
    })
    const handleSelectDocumentChange = (e) => {
        setSelectDocument(e.target.value)
    }
    const handleComment = e => {
        setComment(e.target.value)
    }
    const handleSelectChange = (e) => {
        let ids = e.target.name.split("-")
        setAuthorizations({
            ...authorizations,
            [e.target.name]: {
                authorization: e.target.value,
                general_material_id: ids[1]
            }
        })
    }
    const handleUsedChange = (e) => {
        let ids = e.target.name.split("-")
        setUseds({
            ...useds,
            [e.target.name]: {
                used: e.target.value,
                general_material_id: ids[1]
            }
        })
    }
    const handleChange = (e) => {
        let ids = e.target.name.split("-")
        setObservations({
            ...observations,
            [e.target.name]: {
                observation: e.target.value,
                general_material_id: ids[1]
            }
        })
    }
    useEffect(() => {
        const fetchSubOrder = async () => {
            let res = await get(`case_managements/${sub_order.slug}`)
            res
              .json()
              .then(res => {
                setSubOrder(res)
                setComments(res.comments)
                setFiles([
                    ...res.documents,
                    ...res.order.order_documents,
                ])
                setOrderData(res.order)
                setHospital(res.order.hospital)
                setDoctor(res.order.doctor)
              })
              .catch(err => console.log(err))
        }

        fetchSubOrder()
    }, [])

    const handleResumeChange = (e) => {
        setResumeForm({
          ...resumeForm,
          [e.target.name]: e.target.value
        })
    
    }

    const validateNegotiatedAmout = () => {
        if(parseInt(subOrder.negotiated_amount) > 0)
            return <h3>El monto negociado fue de ${subOrder.negotiated_amount}</h3>
    }

    const validateObservations = () => {
        let rows = 0
        let coded = 0

        
        subOrder.sub_order_generics.map((generics) => {
            generics.sub_order_generals.map((generals) => {
                if(generals.obvservations != null){
                    coded = coded + 1
                }

                rows = rows + 1
            })
        })
        

        if(coded == rows){
            return true
        } else {
            return Object.keys(observations).length == rows
        }
    }

    const handleSendOrder = (partial) => {
        const { validation, errors } = validateHospitalSupervisionFields(order, resumeForm, departureDate)

        if(validation){
            if(validateObservations(subOrder)){
                sendOrder(partial)
            } else {
                // setSending(false)    
                notify('Parece que no ingresaste las observaciones','danger')
            }
        }
        else {
            // setSending(false)
            notify(errors,'danger')
        }
    }
    const handleDepartureDate = (date) => {
        setDepartureDate(date)
    }
    const sendOrder = async (partial) => {
        let supervision = []

        subOrder.sub_order_generics.map((generics) => {
            generics.sub_order_generals.map((generals, index) => {
                supervision.push({
                    general_material_id: generals.id,
                    observation: `${subOrder.id}-${generals.id}` in observations ? observations[`${subOrder.id}-${generals.id}`].observation : generals.obvservations,
                    authorization: `${subOrder.id}-${generals.id}` in authorizations ? authorizations[`${subOrder.id}-${generals.id}`].authorization : generals.authorized,
                    used: `${subOrder.id}-${generals.id}` in useds ? useds[`${subOrder.id}-${generals.id}`].used : generals.used
                })
            })
        })
        
        
        order['deductible'] = resumeForm.deductible ? Number.parseFloat(resumeForm.deductible.replace(/,/g, '')) : ''
        order['coinsurance_money'] = resumeForm.coinsurance_money ? Number.parseFloat(resumeForm.coinsurance_money.replace(/,/g, '')) : ''
        order['medical_fees'] = resumeForm.medical_fees ? Number.parseFloat(resumeForm.medical_fees.replace(/,/g, '')) : ''
        order['hospital_expense'] = resumeForm.hospital_expense ? Number.parseFloat(resumeForm.hospital_expense.replace(/,/g, '')) : ''
        
        order['wf_invoice'] = resumeForm.wf_invoice
        order['evo_invoice'] = resumeForm.evo_invoice
        order['departure_date'] = departureDate
        order['episode'] = resumeForm.episode
        order['coinsurance_percentage'] = resumeForm.coinsurance_percentage
        order['hospital_coinsurance'] = resumeForm.hospital_coinsurance

        let data = {
            message: partial ? 'Solicitud enviada a centro de inteligencia' : 'Guardado Exitoso',
            order: {
                ...order
            },
            sub_order: {
                status: partial ? 'cdi' : 'case_managment',
            },
            supervision,
            authenticity_token
        }

        if(comment){
            data['comment'] = {
                comment,
                user: username,
                module: module,
                date: current_date_time()
            }
        }
        
        if(newFiles.length > 0){
            let documents = []
            newFiles.map((file) => {
                documents.push({
                    document_type: file.document_type ? file.document_type : '',
                    preview: file.preview ? file.preview : '',
                    stored_at: file.stored_at ? file.stored_at : '',
                    user: username,
                    module: module
                })
            })
            data['documents'] = documents
        }

        partial ? validateSupervisionDataRegistered(supervision, data) : sendRequest(data)
    }

    const validateSupervisionDataRegistered = (supervisions, data) => {
      let validation = validateDocuments(files, ["SABANA QUIRURGICA", "NOTA POSTQUIRURGICA"])
    
      if(validation.errors){
        notify(validation.errors,'danger')
      } else {
        let totalGenerics = 0
        let observationRegistereds = 0
        let usedRegistereds = 0
        let authorizedRegistereds = 0
    
        subOrder.sub_order_generics.map((generics) => {
            generics.sub_order_generals.map((generals, index) => {
                totalGenerics = totalGenerics + 1
                if(generals.obvservations != null){ observationRegistereds = observationRegistereds + 1 }
                if(generals.authorized != null){ authorizedRegistereds = authorizedRegistereds + 1 }
                if(generals.used != null){ usedRegistereds = usedRegistereds + 1 }
            })
        })
    
        if (totalGenerics == observationRegistereds && 
            totalGenerics == usedRegistereds &&
            totalGenerics == authorizedRegistereds) {
                sendRequest(data)
            }
        else {
            supervisions.map((supervision) => {
                if(supervision.observation != null){ observationRegistereds = observationRegistereds + 1 }
                if(supervision.authorization != null){ authorizedRegistereds = authorizedRegistereds + 1 }
                if(supervision.used != null){ usedRegistereds = usedRegistereds + 1 }
            })
    
            if (totalGenerics == observationRegistereds && 
                totalGenerics == usedRegistereds &&
                totalGenerics == authorizedRegistereds) {
                    sendRequest(data)
                }
            else {
                notify('Parece que no ingresaste la información correspondiente a supervision hospitalaria','danger')
            }
        }
      }
    }

    const sendRequest = async (data) => {
        if(validateIfCommentExist(comment)){
            if(order.departureDate || departureDate){
                let res = await put(`case_managements/${subOrder.slug}`, data)
                res
                  .json()
                  .then(data => window.location.href = `/`)
                  .catch(err => err)
            } else {

                notify('La fecha de alta es un campo obligatorio','danger')
            }
        } else {
            notify('Recuerda capturar información en la bitacora','danger')
        }
    }

    return <React.Fragment>
        <OrderResume
            departureDate={departureDate}
            handleDepartureDate={handleDepartureDate}
            order={orderData}
            hospital={hospital}
            doctor={doctor}
            title={"Administración de casos"}
            expense={[
                "hospital_expense",
                "medical_fees",
                "deductible",
                "hospital_coinsurance",
                "coinsurance_money",
                "coinsurance_percentage",
            ]}
            invisibles={[
                "entry_probable_date",
                "claim",
                "entry_probable_date",
                "departure_probable_date",
                "surgical_event_date",
                "insured",
                "surgery_date"
            ]}
            editable={[
                "wf_invoice",
                "evo_invoice",
                "coinsurance_percentage",
                "coinsurance_money",
                "deductible",
                "medical_fees",
                "departure_date",
                "episode",
                "hospital_expense",
                "hospital_coinsurance"
            ]}
            handleChange={handleResumeChange}
            form={resumeForm}
            module={'HospitalSupervision'}
        />
        <br/><br/>
        
            <h4>{subOrder.provider.name}</h4>
            <div className="table-responsive">
                <table className="table table-bordered">
                    <thead>
                        <tr>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Codigo</th>
                        <th scope="col">Descripción Generica</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Costo</th>
                        <th scope="col">Utilizado Proveedor</th>
                        <th scope="col">Utilizado Supervision Hospitalaria</th>
                        <th scope="col">Autorizado Supervision Hospitalaria</th>
                        <th scope="col">Observaciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    { subOrder.sub_order_generics.map((generics) => (
                        generics.sub_order_generals.map((generals, index) => (
                            <tr key={index}>
                                <th scope="col">{generals.quantity}</th>
                                <th scope="col">
                                { generals.provider_material.general_material.code }
                                </th>
                                <th scope="col">
                                    {generics.generic}
                                </th>
                                <th scope="col">
                                { generals.provider_material.general_material.description }
                                </th>
                                <th scope="col">${formatMoney(generals.cost)}</th>
                                <th scope="col">{generals.provider_used}</th>
                                <th>
                                    <div className="form-group">
                                        <select 
                                            className="form-control" 
                                            onChange={handleUsedChange}
                                            name={`${subOrder.id}-${generals.id}`}
                                        >
                                            <option disabled selected value> {generals.used || 'SELECCIONA'} </option>
                                            <option>SI</option>
                                            <option>NO</option>
                                        </select>
                                    </div>
                                </th>
                                <th scope="col">
                                <div className="form-group">
                                    <select 
                                        className="form-control" 
                                        id="exampleFormControlSelect1"
                                        onChange={handleSelectChange}
                                        name={`${subOrder.id}-${generals.id}`}
                                    >
                                        <option disabled selected value> {generals.authorized || 'SELECCIONA'} </option>
                                        <option>SI</option>
                                        <option>FUERA DE COBERTURA</option>
                                        <option>SOBREUTILIZACION</option>
                                        <option>INSUMO SIN JUSTIFICACION</option>
                                    </select>
                                </div>
                                </th>
                                <th scope="col">
                                    <div className="form-group">
                                        <textarea 
                                            name={`${subOrder.id}-${generals.id}`}
                                            type="text" 
                                            className="form-control"
                                            onChange={handleChange}
                                            placeholder={generals.obvservations}
                                        />
                                    </div>
                                </th>
                            </tr>
                        ))
                    )) }
                    </tbody>
                </table>
            </div>
            
            {validateNegotiatedAmout()}
           
        <br/><br/>
        <DocumentsContainer 
            selectDocument={selectDocument}
            handleSelectDocumentChange={handleSelectDocumentChange}
            uploadBtn={uploadBtn}
            loader={loader}
            setLoader={setLoader}
            setFiles={setFiles}
            newFiles={newFiles}
            setNewFiles={setNewFiles}
            files={files}
            username={username}
            options={["SABANA QUIRURGICA", "NOTA POSTQUIRURGICA"]}
            authenticity_token={authenticity_token}
            module={module}
        />
        <br/><br/>
        <Binnacle 
            comment={comment}
            handleComment={handleComment}
            comments={comments}
            username={username}
            module={module}
        />
        <button 
            onClick={() => handleSendOrder(true)}
            className="btn btn-primary float-right"
        >
            Enviar a CDI
        </button>
        <button 
            onClick={() => {handleSendOrder(false)}}
            type="button" 
            className="btn btn-success float-right  space-r"
        >
            <i className="fas fa-save"></i>
        </button>
        <Notifications/>
    </React.Fragment>
}

export default React.memo(CaseManagement)