import React, {
    useState,
    useEffect
} from 'react'
import useHttp from '../hooks/useHttp'
import useOrderStatus from '../hooks/useOrderStatus'
import OrderResume from '../uicomponents/order-resume'
import useFormatter from '../hooks/useFormatter'

const Overuse = ({sub_order, order, authenticity_token}) => {
    const { formatMoney } = useFormatter()
    const { get, put } = useHttp()
    const [subOrder, setSubOrder] = useState({
        order: { hospital: {}, doctor: {}}, provider: {}, sub_order_generics: [] 
    })
    const [subOrders, setSubOrders] = useState([])
    const { showStatus } = useOrderStatus()

    useEffect(() => {
        const fetchOveruse = async () => {
          let res = await get(`overuse/${sub_order.slug}`)
            res
              .json()
              .then(data => {
                console.log(data)
                setSubOrder(data)
                // setSubOrders(data.sub_order_generics)
              })
              .catch(err => err)
        }
        fetchOveruse()
    }, [])
    const [departureDate, setDepartureDate] = useState( 
        order.departure_date ?  new Date(order.departure_date) : undefined
    )
    const [resumeForm, setResumeForm] = useState({
        coinsurance_percentage: order.coinsurance_percentage,
        coinsurance_money: order.coinsurance_money,
        deductible: order.deductible,
        medical_fees: order.medical_fees,
        departure_probable_date: order.departure_probable_date,
        departure_date: order.departure_date ? order.departure_date.split(' ').join('T').substring(0, 16) : '',
        episode: order.episode,
        hospital_expense: order.hospital_expense,
        hospital_coinsurance: order.hospital_coinsurance,
        surgical_event_date: order.surgical_event_date
    })

    const handleResumeChange = (e) => {
        setResumeForm({
          ...resumeForm,
          [e.target.name]: e.target.value
        })
    }

    const handleSendOrder = async () => {
        let data = {
            authenticity_token
        }

        let res = await put(`overuse/${sub_order.slug}`, data)
        res
        .json()
        .then(data => window.location.href = `/`)
        .catch(err => err)
    }
    const codedValidation = (coded) => {
        if(coded) {
            return <div className="float-right">
                <div style={{'color':'green'}}>
                    <h5>
                        <i class="fas fa-check-circle"></i>
                         Solicitud Codificada
                    </h5>
                </div>
            </div>
            } else {
            return <div className="float-right">
                <div style={{'color':'red'}}>
                    <h5>
                        <i className="fas fa-exclamation-circle"></i>
                         Solicitud No Codificada
                    </h5>
                </div>
            </div>
        }
    }

    return <React.Fragment>
       <OrderResume
            departureDate={departureDate}
            order={subOrder.order}
            hospital={subOrder.order.hospital}
            doctor={subOrder.order.doctor}
            title={"Sobreutilización"}
            invisibles={[
                "gnp_invoice",
                "entry_probable_date",
                "claim",
                "departure_probable_date",
                "insured",
                "surgery_date"
            ]}
            handleChange={handleResumeChange}
            form={resumeForm}
        />

    
    
        <div className="card">
            <div className="card-body">
                <h5 className="card-title no-margin">
                    {subOrder.provider.name}
                    { codedValidation(subOrder.coded) }
                </h5>
                { showStatus(subOrder.status) }
                
                <div className="table-responsive">
                    <table className="table table-bordered">
                        <thead>
                            <tr>
                            <th scope="col">Cantidad</th>
                            <th scope="col">Codigo</th>
                            <th scope="col">Descripción</th>
                            <th scope="col">Marca</th>
                            <th scope="col">Costo</th>
                            <th scope="col">Utilizado Supervision Hospitalaria</th>
                            <th scope="col">Autorizado Supervision Hospitalaria</th>
                            <th scope="col">Observaciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        { subOrder.sub_order_generics.map((generics) => (
                            generics.sub_order_generals.map((generals, index) => (
                                <tr key={index}>
                                    <th scope="col">{generals.quantity}</th>
                                    <th scope="col">
                                    { generals.provider_material.general_material.code }
                                    </th>
                                    <th scope="col">
                                    { generals.provider_material.general_material.description }
                                    </th>
                                    <th scope="col">{generals.provider_material.brand}</th>
                                    <th scope="col">${formatMoney(generals.cost)}</th>
                                    <th scope="col">{generals.used}</th>
                                    <th scope="col">{generals.authorized}</th>
                                    <th scope="col">{generals.obvservations}</th>
                                </tr>
                            ))
                        )) }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <br/><br/><br/>
        <button 
            onClick={() => {handleSendOrder()}}
            type="button" 
            className="btn btn-success float-right  space-r"
        >
         Revisado   
        </button>
        <br/><br/><br/>
    </React.Fragment>
}

export default Overuse