import React, { 
  useState, 
  useEffect 
} from 'react'
import { URL } from '../settings/configs'
import Binnacle from '../uicomponents/binnacle'
import DocumentsContainer from '../uicomponents/documents_container'
import OrderResume from '../uicomponents/order-resume'
import MaterialModal from '../uicomponents/material-modal'
import AlertDanger from '../uicomponents/alert-danger'
import useSearch from '../hooks/useSearch'
import jsPDF from 'jspdf'
import 'jspdf-autotable';
import PDFHeader from '../../../public/images/pdf-header'
import { 
  current_date_time,
  add_days_from_YMD,
  YMD_to_DMY_format
 } from '../functionalities/dates'
 import useHttp from '../hooks/useHttp'
 import useValidations from '../hooks/useValidations'
 import useFormatter from '../hooks/useFormatter'
 import Notifications, { notify } from '../modules/Notifications'
 import useButtonLoader from '../hooks/useButtonLoader'
 import useWordSeparator from '../hooks/useWordSeparator'
 
const Materials = ({authenticity_token, sub_order, order, provider, username, module}) => {
  const { formatMoney } = useFormatter()
  const { setSending, validateSending } = useButtonLoader()
  const { 
    validateIfSurgicalEvent, 
    validateIfCommentExist,
    validateDocuments
  } = useValidations()
  const { get, post, put } = useHttp()
  const { generateString } = useWordSeparator()
  // Documents Component
  const [selectDocument, setSelectDocument] = useState(null)
  const [uploadBtn, setUploadBtn] = useState('visible')
  const [loader, setLoader] = useState('hidden')
  const [files, setFiles] = useState([])
  const [newFiles, setNewFiles] = useState([])
  // Comments Component
  const [comment, setComment] = useState('')
  const [comments, setComments] = useState([])
  // Order Resume
  const [subOrderData, setSubOrderData] = useState({})
  const [orderData, setOrderData] = useState('')
  const [hospital, setHospital] = useState('')
  const [doctor, setDoctor] = useState('')
  // Hook Searcher
  const [generalMaterials, setGeneralMaterials] = useState([])
  const { query, setQuery, filteredElements } = useSearch(generalMaterials, ['brand', 'description'])
  //
  const [aggregateMaterials, setAggregateMaterials] = useState([])
  const [generic, setGeneric] = useState('')
  const [subOrderGenericId, setSubOrderGenericId] = useState('')
  const [genericWithMaterials, setGenericWithMaterials] = useState('hidden')
  const [generics, setGenerics] = useState([])
  const [negotiableMatter, setNegotiableMatter] = useState('hospital_supervision')

  const [surgicalDate, setSurgicalDate] = useState( 
    order.surgical_event_date ?  new Date(order.surgical_event_date) : undefined
  )

  const handleSurgicalDate = (date) => {
    console.log(date)
    setSurgicalDate(date)
  }

  const [form, setForm] = useState({
    surgical_event_date: order.surgical_event_date,
  })
  const [materialForm, setMaterialForm] = useState({
    description: '',
    code: '',
    price: '',
    brand: ''
  })
  let total = 0
  const [modal, setModal] = useState(false)
  const toggle = () => setModal(!modal)

    useEffect(() => {
      const fetchMaterials = async () => {
        let res = await get(`provider_materials?provider_id=${provider.id}`)
          res
            .json()
            .then(data => setGeneralMaterials(data))
            .catch(err => err)
      }
        fetchSubOrder()
        fetchMaterials()
    }, [])

  const fetchSubOrder = async () => {
    let res = await get(`sub_orders/${sub_order.slug}`)
      res
        .json()
        .then(data => {
          setSubOrderData(data)
          setFiles([
            ...data.documents,
            ...data.order.order_documents,
          ])
          setComments(data.comments)
          setOrderData(data.order)
          setHospital(data.order.hospital)
          setDoctor(data.order.doctor)
          setGenerics(data.sub_order_generics)
        })
        .catch(err => err)
  }

    const handleComment= e => {
      setComment(e.target.value)
    }
    const handleSelectDocumentChange = (e) => {
      setSelectDocument(e.target.value)
    }

  const genericsHasGenerals = (materials) => {
    let addedGenerics = []
    
    generics.map((generic) => {
      if(generic.generic !== "otro adicional"){
        if(materials.filter(e => parseInt(e.sub_order_generic_id) === generic.id).length > 0 || generic.sub_order_generals.length){
          addedGenerics.push(true)
        }
      }
    })

    if(addedGenerics.length >= (generics.length - 1)){
      return true
    } else {
      return false
    }
    
  }

  const sendOrder = async(partial) => {
    setSending(true)
    
    let materials = []
    aggregateMaterials.map((material) => {
      materials.push(material)
    })

    let data = {
      message: partial ? 'Solicitud enviada correctamente' : 'Guardado exitoso',
      sub_order:{
        status: partial ? negotiableMatter : 'hospital_supervision',
        coded: partial ? true : false
      },
      materials: [
        ...materials
      ],
      order: {
        ...order,
      },
      authenticity_token
    }
    if(comment){
      data['comment'] = {
          comment,
          user: username,
          module: module,
          date: current_date_time()
      }
    }


    if(surgicalDate){
      data['order'] = {
        surgical_event_date: surgicalDate,
        departure_probable_date: surgicalDate
      }
    }
    
    if(newFiles.length > 0){
      let documents = []
      newFiles.map((file) => {
          documents.push({
              document_type: file.document_type ? file.document_type : '',
              preview: file.preview ? file.preview : '',
              stored_at: file.stored_at ? file.stored_at : '',
              user: username,
              module: module
          })
      })
      data['documents'] = documents
  }

    if(partial){
      if(validateIfCommentExist(comment)){
        if(genericsHasGenerals(materials)){
          if(surgicalDate != null){
            let validation = validateDocuments(files, ["HOJA DE REMISION SELLADA"])
    
            if(validation.errors){
              setSending(false)
              notify(validation.errors,'danger')
            } else {
              setGenericWithMaterials('hidden')
              sendRequest(data)
            }
          } else {
            setSending(false)
            notify('La fecha de evento quirurgico es obligatoria','danger') 
          }
        } else {
          setSending(false)
          setGenericWithMaterials('visible')
       }
      }
      else {
        setSending(false)
        notify('Recuerda capturar información en la bitacora','danger')
      }
    } else {
      sendRequest(data)
    }
  }

  const sendRequest = async (data) => {
    let res = await put(`hospital_supervision_provider_orders/${sub_order.slug}`, data)
    res
      .json()
      .then(data => window.location.href = `/`)
      .catch(err => {
        setSending(false)
        console.log(err)
      })
  }

  const handleSendOrder = async (partial) => {
    if(validateIfSurgicalEvent(order, surgicalDate)){
      sendOrder(partial)
    } else {
      notify('La fecha del evento quirurgico es obligatoria','danger')
    }
}

  const handleAddMaterial = (material, is_new = false) => {
    if(subOrderGenericId != ''){
      let date = new Date()
      let currentTime = date.getTime()

      setAggregateMaterials(aggregateMaterials => [
        ...aggregateMaterials,
        {
          ...material,
          sub_order_generic_id: subOrderGenericId,
          quantity: 1,
          used: 'SI',
          generic: generic,
          is_new: is_new,
          added: currentTime
        }
      ])
      calculateTotal()
    } else {
      notify('Selecciona Generico a surtir','danger')
      
    }
  }

  const handleResumeChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const handleMaterialChange = (e) => {
    setMaterialForm({
      ...materialForm,
      [e.target.name]: e.target.value
    })
  } 

  const handleMaterialSubmit = async () => {
    let data = {
      provider_material: {
        ...materialForm
      },
      authenticity_token
    }

    let res = await post(`provider_materials`, data)
    res
      .json()
      .then(data => {
        setNegotiableMatter('negotiable-matter')
        handleAddMaterial(data, true)
        setMaterialForm({
          description: '',
          code: '',
          price: '',
          brand: ''
        })
        toggle()
      })
      .catch(err => err)
  }

  const handleRemoveMaterial = (material) => {
    setAggregateMaterials(aggregateMaterials.filter((currentMaterial) => {
     return currentMaterial.added !== material.added
    }))

    calculateTotal()
  }
  
  const handlePDFGenerator = () => {
    let genericsList = []

    generics.map((generic) => {
      generic.sub_order_generals.map((general) => {
        genericsList.push([
          generateString(generic.generic, 1),
          generateString(general.provider_material.description),
          general.provider_material.brand,
          general.quantity
        ]) 
      })
    })

    var doc = new jsPDF()
    let today = new Date();
    var img = new Image()
    let pageInfo = doc.internal.getCurrentPageInfo()


    img.src = PDFHeader
    doc.addImage(img, 'JPEG', 15, 5, 250, 20)
    
    doc.setFontType("bold");
    doc.setFontSize(10)
    doc.text(170,10, `Fecha de impresión`)
    doc.setFontType("normal");
    doc.text(180,15,`${today.getDate()}-${today.getMonth() + 1}-${today.getFullYear()}`)
    
    
    doc.setFontSize(16)
    doc.setFontType("bold");
    doc.text(140, 30, 'Visado - Proveedor')
    doc.setFontSize(10)
    doc.text(150,35, `Ticket: ${orderData.cpo}`)
    // doc.setFontType("normal");

    doc.setFontSize(18)
    doc.setFontType("bold");
    doc.text(20, 45, `${provider.name}`)

    doc.setFontSize(12)
    doc.setFontType("bold");
    doc.text(20,55, `Asegurado:`)
    doc.setFontType("normal");
    doc.text(45,55, `${orderData.insured}`)

    doc.setFontType("bold");
    doc.text(20,60, `Folio GNP:`)
    doc.setFontType("normal");
    doc.text(45,60, `${orderData.gnp_invoice}`)

    doc.setFontType("bold");
    doc.text(20,65, `Reclamación:`)
    doc.setFontType("normal");
    doc.text(55,65, `${orderData.claim}`)

    doc.setFontType("bold");
    doc.text(20,70, `Médico Tratante:`)
    doc.setFontType("normal");
    doc.text(55,70, `${orderData.doctor.name}`)

    doc.setFontType("bold");
    doc.text(20,75, `Hospital:`)
    doc.setFontType("normal");
    doc.text(45,75, `${orderData.hospital.name}`)
    
    doc.setFontType("bold");
    doc.text(20,80, `Padecimiento:`)
    doc.setFontType("normal");
    doc.text(55,80, `${orderData.suffering}`)

    doc.setFontType("bold");
    doc.text(20,85, `Procedimiento:`)
    doc.setFontType("normal");
    var splitProcedure = doc.splitTextToSize(`${orderData.procedure}`, 150)
    doc.text(55,85, splitProcedure)

    doc.setFillColor('ED7D32');
    doc.rect(20, 100, 180, 1, "F");
    // X - Y - X width - Y width

    doc.setFontType("bold");
    doc.setFontSize(16)
    doc.text(20,108, `Insumos Entregados`)

    doc.setFontType("normal");

    let wantedTableWidth = 120;
    let pageWidth = doc.internal.pageSize.width;
    let margin = (pageWidth - wantedTableWidth) / 2;

    doc.autoTable({
      theme: 'striped',
      startY: 115,
      headStyles: {
        fillColor: [0, 32, 96],
        cellWidth: 30,
        fontSize: 8
      },
      styles: {
        halign: 'center',
        cellWidth: 'wrap',
        cellPadding: {top: 4, bottom: 4},
        fontSize: 8
      },
      head: [['Genérico', 'Descripción', 'Marca', 'Cantidad']],
      margin: {left: margin, right: margin},
      body: [...genericsList]
    })
    doc.save('solicitud.pdf')
  }

  const handleManageUsed = (material, used) => {
    setAggregateMaterials(aggregateMaterials.map((currentMaterial) => {
      if(currentMaterial == material){
        currentMaterial.used = used
        return currentMaterial
      } else {
        return currentMaterial
      }
    }))
    calculateTotal()
  }

  const handleManageQuantity = (material, quantity) => {
    setAggregateMaterials(aggregateMaterials.map((currentMaterial) => {
      if(currentMaterial == material){
        currentMaterial.quantity = quantity
        return currentMaterial
      } else {
        return currentMaterial
      }
    }))
    calculateTotal()
  }

  const calculateTotal = () => {
    total = 0
    generics.map((generic) => {
      generic.sub_order_generals.map((general) => {
        if(general.provider_used == 'SI'){
          total = total + (parseInt(general.quantity) * Number.parseFloat(general.cost) )
        }
      })
    })
    aggregateMaterials.map((material) => {
      if(material.used == 'SI')
        total = total + (parseInt(material.quantity) * Number.parseFloat(material.price))
    })

    return formatMoney(total)
  }

  const handleRemoveCloudMaterial = (material) => {

    handleDeleteMaterial(material)
    calculateTotal()
  }

  const handleDeleteMaterial = async (material) => {
    
    let config = {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({authenticity_token})
    }

    await fetch(`${URL}/sub_order_generals/${material.id}`, config)
    await fetchSubOrder()
  }
    return <React.Fragment>
    
    <OrderResume
      surgicalDate={surgicalDate}
      handleSurgicalDate={handleSurgicalDate}
      order={orderData}
      hospital={hospital}
      doctor={doctor}
      subOrder={subOrderData}
      title={"Programación de cirugia"}
      invisibles={[
        "deductible",
        "medical_fees",
        "coinsurance_percentage",
        "coinsurance_money",
        "departure_date",
        "departure_probable_date",
        "order_type",
        "icd",
        "cpt",
        "second_cpt",
        "episode",
        "hospital_expense"
      ]}
      editable={["surgical_event_date"]}
      form={form}
      handleChange={handleResumeChange}
      provider={provider}
      legends={true}
    />


    <div>
      <button className="btn btn-primary space-r" onClick={handlePDFGenerator}>
          <i className="fas fa-print"></i>
      </button>
      VISADO 
    </div>
  
    <br/><br/>
    {/* { validateAssorted() } */}
    <br/><br/>

    <div className="row">
      <div className="col-sm-6 form-group">
        <label htmlFor="exampleFormControlSelect1">Generico a surtir</label>
        <select 
          className="form-control" 
          id="exampleFormControlSelect1"
          name="generic_selected"
          onChange = {(e) => {
            setSubOrderGenericId(e.target.value.slice(0, e.target.value.lastIndexOf(',')))
            setGeneric(e.target.value.slice(e.target.value.lastIndexOf(',') + 1))
          }}
        >
        <option disabled selected value> -- selecciona una opción -- </option>
          {generics.map((generic, index) => (
            <option
              key={index}
              multiple={true}
              value={[generic.id, generic.generic]}
            >
            {generic.generic}
            </option>
          ))}
        </select>
      </div>

      <div className="col-sm-6 form-group">
        <label>Generales</label>
        <input 
          type="text" 
          className="form-control"
          placeholder="BUSCAR INSUMO"
          value={query}
          onChange={(e) => {
              setQuery(e.target.value)
          }}
        />
      </div>
    </div>
    

    <div style={{'maxHeight':'350px'}} className="pre-scrollable accordion">
      { filteredElements.map((material, index) => (
        <div className="card" key={index}>
          <div className="form-inline">
            <div className="form-group mx-sm-3 mb-2">
              <button className="btn btn-link" type="button">
                  {material.general_material.description}
              </button>
            </div>
            <button 
                type="button" 
                className="btn btn-outline-primary float-right ml-auto"
                onClick={()=>{handleAddMaterial(material)}}
            >
                <i className="fas fa-plus"></i>
            </button>
          </div>
        </div>      
      )) }
    </div>
    <MaterialModal
      form={materialForm}
      handleChange={handleMaterialChange}
      onSubmit={handleMaterialSubmit}
      toggle={toggle}
      modal={modal}
    />
    <br/><br/>
    <div className="table-responsive">
      <table className="table table-bordered">
            <thead>
              <tr>
                <th scope="col">Generico</th>
                <th scope="col">Descripcion</th>
                <th scope="col">Codigo</th>
                <th scope="col">Precio</th>
                <th scope="col">Marca</th>
                <th scope="col">Cantidad</th>
                <th scope="col">Utilizado Proveedor</th>
                <th scope="col">Utilizado Supervision Hospitalaria</th>
                <th scope="col">Autorizado Supervision Hospitalaria</th>
                <th slice="col">Acciones</th>
              </tr>
            </thead>
            <tbody>
            {   
              generics.map((generic) => (
                generic.sub_order_generals.map((general,index) => (
                  <tr>
                  <td>{generic.generic}</td>
                  <td>{general.provider_material.description}</td>
                  <td>{general.provider_material.code}</td>
                  <td>${formatMoney(general.cost)}</td>
                  <td>{general.provider_material.brand}</td>
                  <td>{general.quantity}</td>
                  <td>{general.provider_used}</td>
                  <td>{general.used}</td>
                  <td>{general.authorized}</td>
                  <td>                  
                  <center>
                    <button 
                      type="button" 
                      className="btn btn-danger"
                      onClick={() => handleRemoveCloudMaterial(general)}
                    >
                      <i className="fa fa-trash"></i>
                    </button>
                  </center>
                </td>
                </tr>    
                ))
              ))
            }
            {aggregateMaterials.map((material) => (
              <tr>
                <td>{material.generic}</td>
                <td>{material.description}</td>
                <td>{material.code}</td>
                <td>${formatMoney(material.price)}</td>
                <td>{material.brand}</td>
                <td>
                  <input 
                    className="form-control"
                    type="number"
                    placeholder="1"
                    min="1"
                    value={material.quantity}
                    onChange={(e) => {
                      handleManageQuantity(material, e.target.value)
                    }}
                  />
                </td>
                <td>
                    <select 
                      className="form-control"
                      onChange={(e) => {
                        handleManageUsed(material, e.target.value)
                      }}
                    >
                      <option selected >SI</option>
                      <option>NO</option>
                    </select>
                </td>
                <td></td>
                <td></td>
                <td>                  
                <center>
                  <button 
                    type="button" 
                    className="btn btn-danger"
                    onClick={() => handleRemoveMaterial(material)}
                  >
                    <i className="fa fa-trash"></i>
                  </button>
              </center>
              </td>
              </tr>
            ))}
            </tbody>
            </table>
    </div>

          <h3>Total: ${calculateTotal()}</h3>
    <br/><br/>
    
    <DocumentsContainer 
        selectDocument={selectDocument}
        handleSelectDocumentChange={handleSelectDocumentChange}
        uploadBtn={uploadBtn}
        loader={loader}
        setLoader={setLoader}
        setFiles={setFiles}
        newFiles={newFiles}
        setNewFiles={setNewFiles}
        files={files}
        username={username}
        options={["HOJA DE REMISION SELLADA"]}
        authenticity_token={authenticity_token}
        module={module}
        provider={true}
    />
    <br/><br/>
    <Binnacle 
        comment={comment}
        handleComment={handleComment}
        comments={comments}
        username={username}
        module={module}
    />

    <AlertDanger
      text="Verifica que cada generico contenga al menos 1 insumo"
      visibility={genericWithMaterials}
    />

    { validateSending('Enviar a Supervision Hospitalaria', handleSendOrder) }
    
    <button 
      onClick={() => {handleSendOrder(false)}}
      type="button" 
      className="btn btn-success float-right  space-r"
    >
      <i className="fas fa-save"></i>
    </button>
    <Notifications/>
    </React.Fragment>
}
export default Materials