import React,{
    useState,
    useEffect
} from 'react'
import DocumentsContainer from '../uicomponents/documents_container'
import Binnacle from '../uicomponents/binnacle'
import OrderResume from '../uicomponents/order-resume'
import { current_date_time } from '../functionalities/dates'
import useHttp from '../hooks/useHttp'
import useFormatter from '../hooks/useFormatter'
import useValidations from '../hooks/useValidations'
import Notifications, { notify } from '../modules/Notifications'

const Extemporaneous = ({sub_order, order, username, module, authenticity_token}) => {
  const { formatMoney } = useFormatter()
  const { get, put } = useHttp()    
  // Documents Component
  const [selectDocument, setSelectDocument] = useState(null)
  const [uploadBtn, setUploadBtn] = useState('visible')
  const [loader, setLoader] = useState('hidden')
  const [files, setFiles] = useState([])
  const [newFiles, setNewFiles] = useState([])
  // Comments Component
  const [comment, setComment] = useState('')
  const [comments, setComments] = useState([])
  // Resume Component
  const [departureDate, setDepartureDate] = useState( 
    order.departure_date ?  new Date(order.departure_date) : undefined
  )
  const [resumeForm, setResumeForm] = useState({
    departure_date: order.departure_date ? order.departure_date.split(' ').join('T').substring(0, 16) : '',
  })
  const [orderData, setOrderData] = useState('')
  const [hospital, setHospital] = useState('')
  const [doctor, setDoctor] = useState('')
  //
  const [subOrder, setSubOrder] = useState({
      provider: {},
      sub_order_generics: []
  })
  const [amount, setAmount] = useState(0)
  const [negotiatedAmount, setNegotiatedAmout] = useState(0)
  const { 
      validateIfCommentExist
  } = useValidations()

    const handleComment= e => {
      setComment(e.target.value)
    }

    const handleSelectDocumentChange = (e) => {
      setSelectDocument(e.target.value)
    }    

    const handleResumeChange = (e) => {
      setResumeForm({
        ...resumeForm,
        [e.target.name]: e.target.value
      })
    }

    useEffect(() => {
        const fetchOrder = async () => {
          let res = await get(`extemporaneous/${sub_order.slug}`)
            res
            .json()
            .then(data => {
              setSubOrder(data)
              setFiles(data.documents)
              setComments(data.comments)
              setOrderData(data.order)
              setHospital(data.order.hospital)
              setDoctor(data.order.doctor)
            })
            .catch(err => console.log(err))
        }
          fetchOrder()
    }, [])

    const calculateTotal = () => {
        let total = 0
        subOrder.sub_order_generics.map((generics) => {
          generics.sub_order_generals.map((general) => {
            if(general.provider_used == 'SI'){
              total = total + (parseInt(general.cost) * parseInt(general.quantity))
            }
          })
        })
        return total
    }

    const handleSendOrder = async () => {
      sub_order['status'] = 'case_managment'
      sub_order['coded'] = true
      sub_order['negotiated_amount'] = amount
      let data = {
        message: 'Solicitud enviada a administración de casos',
        sub_order: {
          ...sub_order
        },
        authenticity_token
      }
      if(comment){
        data['comment'] = {
            comment,
            user: username,
            module: module,
            date: current_date_time()
        }
      }
      if(newFiles.length > 0){
        let documents = []
        newFiles.map((file) => {
            documents.push({
                document_type: file.document_type ? file.document_type : '',
                preview: file.preview ? file.preview : '',
                stored_at: file.stored_at ? file.stored_at : '',
                user: username,
                module: module
            })
        })
        data['documents'] = documents
    }

    if(validateIfCommentExist(comment)){
      let res = await put(`extemporaneous/${sub_order.slug}`, data)
      res
        .json()
        .then(data => window.location.href = `/`)
        .catch(err => err)
    } else {
      notify('Recuerda capturar información en la bitacora','danger')
    }
  }

    return <React.Fragment>
    <OrderResume
      departureDate={departureDate}
      order={orderData}
      hospital={hospital}
      doctor={doctor}
      title={"Extemporanea"}
      invisibles={[
          "deductible",
          "medical_fees",
          "surgical_event_date",
          "surgery_date"
      ]}
      form={resumeForm}
      handleChange={handleResumeChange}
  />
  <br/>
    <h4>{subOrder.provider.name}</h4>
    <div className="table-responsive">
      <table className="table table-bordered">
          <thead>
              <tr>
                  <th scope="col">Cantidad</th>
                  <th scope="col">Codigo</th>
                  <th scope="col">Descripción</th>
                  <th scope="col">Marca</th>
                  <th scope="col">Utilizado Proveedor</th>
                  <th scope="col">Costo</th>
              </tr>
          </thead>
          <tbody>
          { subOrder.sub_order_generics.map((generics) => (
              generics.sub_order_generals.map((generals, index) => (
                  <tr key={index}>
                      <th scope="col">{generals.quantity}</th>
                      <th scope="col">
                        { generals.provider_material.general_material.code }
                      </th>
                      <th scope="col">
                      { generals.provider_material.general_material.description }
                      </th>
                      <th scope="col">{generals.provider_material.brand}</th>
                      <th scope="col">{generals.provider_used}</th>
                      <th scope="col">${formatMoney(generals.cost)}</th>
                  </tr>
              ))
          )) }
              <tr>
                <th colspan="5">Total</th>
                <td>${formatMoney(calculateTotal())}</td>
              </tr>
              <tr>
                <th colspan="5">Monto Negociado (Descuento)</th>
                <td>  
                  <input 
                    type="number"
                    className="form-control"
                    min={0}
                    placeholder="0"
                    onChange={(e)=>{
                      setAmount(e.target.value)
                      setNegotiatedAmout(e.target.value)
                    }}
                  />
                </td>
              </tr>
              <tr>
                <th colspan="5">Monto Autorizado</th>
                <td>${formatMoney(calculateTotal() - negotiatedAmount)}</td>
              </tr>    
          </tbody>
      </table>
    </div>

 
    <br/><br/>
  <DocumentsContainer 
    selectDocument={selectDocument}
    handleSelectDocumentChange={handleSelectDocumentChange}
    uploadBtn={uploadBtn}
    loader={loader}
    setLoader={setLoader}
    setFiles={setFiles}
    newFiles={newFiles}
    setNewFiles={setNewFiles}
    files={files}
    username={username}
    options={["FACTURA DESCUENTO", "NOTA DE CREDITO", "COMPROBANTE COBRO COASEGURO", "EVIDENCIA DE NEGOCIACIÓN"]}
    authenticity_token={authenticity_token}
    module={module}
  />
  <br/><br/>
  <Binnacle 
    comment={comment}
    handleComment={handleComment}
    comments={comments}
    username={username}
    module={module}
  />
    <button 
        onClick={() => {handleSendOrder()}}
        className="btn btn-primary float-right"
    >
    Enviar a Administración de casos
    </button>
    <Notifications/>
</React.Fragment>
}

export default Extemporaneous