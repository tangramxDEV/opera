import React,{
    useState,
    useEffect
} from 'react'
import { URL } from '../settings/configs'
import Binnacle from '../uicomponents/binnacle'
import DocumentsContainer from '../uicomponents/documents_container'
import OrderResume from '../uicomponents/order-resume'
import { current_date_time } from '../functionalities/dates'
import useOrderStatus from '../hooks/useOrderStatus'
import useHttp from '../hooks/useHttp'
import useValidations from '../hooks/useValidations'
import Notifications, { notify } from '../modules/Notifications'
import useButtonLoader from '../hooks/useButtonLoader'
import useFormatter from '../hooks/useFormatter'

const ProviderPaymentsOrder = ({order, sub_order, authenticity_token, username, module}) => {
    const { formatMoney } = useFormatter()
    const { setSending, validateSending } = useButtonLoader()
    const { showStatus } = useOrderStatus()
    const { get, put } = useHttp()    
    const { validateLetterExists,
      validateIfCommentExist
     } = useValidations()

    const [containNew, setContainNew] = useState(false)
    const [letter, setLetter] = useState(false)
    const [orderData, setOrderData] = useState('')
    const [hospital, setHospital] = useState('')
    const [doctor, setDoctor] = useState('')
    // Resume Component
    const [resumeForm, setResumeForm] = useState({
      departure_date: order.departure_date ? order.departure_date.split(' ').join('T').substring(0, 16) : '',
    })
    // Comments Component
    const [comment, setComment] = useState('')
    const [comments, setComments] = useState([])
    // Documents Component
    const [selectDocument, setSelectDocument] = useState(null)
    const [uploadBtn, setUploadBtn] = useState('visible')
    const [loader, setLoader] = useState('hidden')
    const [files, setFiles] = useState([])
    const [newFiles, setNewFiles] = useState([])
    //
    const [subOrder, setSubOrder] = useState({provider: {}, sub_order_generics:[]})
    // FileUploader Component
    const [currentFile, setCurrentFile] = useState('')
    const [departureDate, setDepartureDate] = useState( 
      order.departure_date ?  new Date(order.departure_date) : undefined
    ) 
    const handleSelectDocumentChange = (e) => {
        setSelectDocument(e.target.value)
    }

    const handleResumeChange = (e) => {
        setResumeForm({
          ...resumeForm,
          [e.target.name]: e.target.value
        })
    }

    const handleComment= e => {
        setComment(e.target.value)
    }

    useEffect(() => {
      const fetchSubOrder = async () => {
        let res = await get(`provider_payments/${sub_order.slug}`)
        res
          .json()
          .then(data => {
            setSubOrder(data)
            validateIfExistNew(data)
            existLetterIntoSubOrder(data)
            setComments(data.comments)
            setFiles([
              ...data.documents,
              ...data.order.order_documents,
            ])
            setOrderData(data.order)
            setHospital(data.order.hospital)
            setDoctor(data.order.doctor)
            // setSubOrders(data.sort(compareValues('to_pay', 'desc')))
          })
          .catch(err => console.log(err))
      }
          fetchSubOrder()
      }, [])

      const existLetterIntoSubOrder = (subOrder) => setLetter(subOrder.letter != null)

      const handleSendOrder = () => {
        if(validateLetterExists(subOrder)){
          if(validateIfCommentExist(comment)){
            sendOrder()
          } else {
            notify('Recuerda capturar información en la bitacora','danger')
          }
        } else {
          notify('Parece que no agregaste las cartas de autorización','danger')
        }
    }

    const validateIfExistNew = (data) => {
      data.sub_order_generics.map((generic) => {
        generic.sub_order_generals.map((general) => {
          if(general.is_new) {
            setContainNew(true)
          }
        })
      })
    }

    const sendOrder = async () => {
      setSending(true)
      
      let data = {
        message: containNew ? 'Solicitud enviada a Negociacion de Insumos' : 'Solicitud enviada a Proveedor',
        sub_order: {
          status: containNew ? 'new_supplies' : 'finished'
        },
        order: {
          status: 'finished'
        },
        authenticity_token
      }
      if(comment){
        data['comment'] = {
            comment,
            user: username,
            module: module,
            date: current_date_time()
        }
      }
      let res = await put(`provider_payments/${sub_order.slug}`, data)
      res
        .json()
        .then(data => window.location.href = `/`)
        .catch(err => err)
    }

    const onChange = (e) => {
      setCurrentFile(e.target.files[0])
    }

    const handleUploadFile = async (subOrder) => {
      const formData = new FormData()
      formData.append('file_uploader[file]', currentFile)
      formData.append('authenticity_token', authenticity_token)
      try {
          let config = {
            method: 'POST',
            body: formData,
            contentType: false,
            processData: false
          }
      
          await fetch(`${URL}/file_uploaders`, config).then(async (res) => {
              let data = await res.json()
              updateSubOrder(data.url, subOrder)
          })
        } catch (error) {
          console.log(error)
      }
  }

  const updateSubOrder = async (letter_url, subOrder) => {
    let data = {
      sub_order: {
        letter: letter_url
      },
      authenticity_token
    }

    let res = await put(`/sub_orders/${subOrder.slug}`, data)
    res
      .json()
      .then(data => {
        let newSubOrder = subOrder
        newSubOrder['letter'] = letter_url
        setSubOrder(newSubOrder);
        if(letter_url){
          setLetter(true)
        } else {
          setLetter(false)
        }
        validateLetter()
      })
      .catch(err => err)
  }

  const validateLetter = () => {
    if(letter){
      return <p>
      CARTA DE AUTORIZACIÓN 
      <a
      className="space-l"
      onClick={(e) => {
        e.preventDefault()
        window.open(`${subOrder.letter}`)
      }}
      href="#"
      >
        <i className="fas fa-eye"></i>
      </a>
      <button 
        type="button" 
        className="space-l btn btn-danger"
        onClick={() => {
          if (window.confirm(`Seguro que deseas eliminar la carta de autorización?`)) {
            updateSubOrder(null, subOrder)
          }
        }}
      >
        <i className="fa fa-trash"></i>
      </button>
      </p>
    }else {
      if(subOrder.coded){
        return <div className="row">
            <div className="col sm-4">
              <div className="form-group">
                <input 
                  type="file" 
                  className="space-t form-control-file" 
                  onChange={onChange}
                  accept=".pdf"
                />
              </div>
            </div>
            <div className="col sm-4 space-t-x">
              <button 
                className="btn btn-outline-primary"
                onClick={() => {handleUploadFile(subOrder)}}
                style={{visibility: uploadBtn}}
              >
                <i className="fas fa-save"></i>
              </button>
            </div>
          </div>
      }
    }
  }

  const validateNegotiation = () => {
    if(subOrder.negotiated_amount){
      return <h4>Monto Negociado: ${subOrder.negotiated_amount}</h4>
    }
  }
      return <React.Fragment>
        <OrderResume
            departureDate={departureDate}
            title={"Dictamen - Cartas de autorización"}
            order={orderData}
            hospital={hospital}
            doctor={doctor}
            subOrder={subOrder}
            provider={subOrder.provider}
            legends={true}
            invisibles={[
              "deductible",
              "medical_fees",
              "surgical_event_date",
              "surgery_date"
            ]}
            form={resumeForm}
            handleChange={handleResumeChange}
        />
        <br/><br/>
    
        <React.Fragment>
        <div className="card">
          <div className="card-body">
            <h5 className="card-title no-margin">
              {subOrder.provider.name}
              <div className="float-right">
                <h5>
                  Folio: {subOrder.invoice}
                </h5>
              </div>
            </h5>
            
            { validateLetter() }
            <div className="table-responsive">
              <table className="table table-bordered">
                  <thead>
                      <tr>
                      <th>Cantidad</th>
                      <th>Codigo</th>
                      <th>Descripción</th>
                      <th>Marca</th>
                      <th>Utilizado Proveedor</th>
                      <th>Utilizado Supervision Hospitalaria</th>
                      <th>Autorizado Supervision Hospitalaria</th>
                      <th>Observaciones</th>
                      <th>Costo</th>
                      </tr>
                  </thead>
                  <tbody>
                  { subOrder.sub_order_generics.map((generics) => (
                      generics.sub_order_generals.map((generals, index) => {
                          return <tr key={index}>
                              <td scope="col">{generals.quantity}</td>
                              <td scope="col">
                                { generals.provider_material.general_material.code }
                              </td>
                              <td scope="col">
                              { generals.provider_material.general_material.description }
                              </td>
                              <td scope="col">
                              { generals.provider_material.brand }
                              </td>
                              <td scope="col">{generals.provider_used}</td>
                              <td scope="col">
                              {generals.used}
                              </td>
                              <td scope="col">
                              {generals.autdorized}
                              </td>
                              <td scope="col">
                              {generals.obvservations}
                              </td>
                              <td>${formatMoney(generals.cost)}</td>
                          </tr>
                      })
                  )) }
                  <tr>
                    <td colSpan="8">Monto Negociado (Descuento)</td>
                    <td>${formatMoney(subOrder.negotiated_amount ? subOrder.negotiated_amount : 0)}</td>
                  </tr>
                  <tr>
                    <td colSpan="8"><strong>IMPORTE TOTAL CARTA AUTORIZACIÓN</strong></td>
                    <td>${formatMoney(subOrder.to_pay - (subOrder.negotiated_amount ? subOrder.negotiated_amount : 0))}</td>
                  </tr>
                  </tbody>
              </table>
            </div>
          </div>
        </div>
        <br/>
        {validateNegotiation()}
        </React.Fragment>
    
    <br/><br/>

        <DocumentsContainer 
            selectDocument={selectDocument}
            handleSelectDocumentChange={handleSelectDocumentChange}
            uploadBtn={uploadBtn}
            loader={loader}
            setLoader={setLoader}
            setFiles={setFiles}
            setNewFiles={setNewFiles}
            files={files}
            username={username}
            options={["SABANA QUIRURGICA", "NOTA POSTQUIRURGICA"]}
            authenticity_token={authenticity_token}
            only_view={true}
            module={module}
        />
        <br/><br/>
        <Binnacle 
            comment={comment}
            handleComment={handleComment}
            comments={comments}
            username={username}
            module={module}
        />

        { validateSending(containNew ? 'Enviar a Negociacion Insumos' : 'Enviar a Proveedor', handleSendOrder) }

        <Notifications/>
      </React.Fragment>
}

export default ProviderPaymentsOrder