import React,{
    useState,
    useEffect
} from 'react'
import Binnacle from '../uicomponents/binnacle'
import DocumentsContainer from '../uicomponents/documents_container'
import OrderResume from '../uicomponents/order-resume'
import useHttp from '../hooks/useHttp'
import useFormatter from '../hooks/useFormatter'

const OperationalResultProvider = ({sub_order, order, username}) => {
    const { formatMoney } = useFormatter()
    const { get } = useHttp()    
    const [orderData, setOrderData] = useState('')
    const [hospital, setHospital] = useState('')
    const [doctor, setDoctor] = useState('')
    // Comments Component
    const [comment, setComment] = useState('')
    const [comments, setComments] = useState([])
    // Documents Component
    const [selectDocument, setSelectDocument] = useState(null)
    const [uploadBtn, setUploadBtn] = useState('visible')
    const [loader, setLoader] = useState('hidden')
    const [files, setFiles] = useState([])
    const [newFiles, setNewFiles] = useState([])
    // Resume Component
    const [departureDate, setDepartureDate] = useState( 
        order.departure_date ?  new Date(order.departure_date) : undefined
    )
    const [surgicalDate, setSurgicalDate] = useState( 
        order.surgical_event_date ?  new Date(order.surgical_event_date) : undefined
    )
    const [resumeForm, setResumeForm] = useState({
        coinsurance_percentage: order.coinsurance_percentage,
        coinsurance_money: order.coinsurance_money,
        deductible: order.deductible,
        medical_fees: order.medical_fees,
        departure_probable_date: order.departure_probable_date,
        departure_date: order.departure_date ? order.departure_date.split(' ').join('T').substring(0, 16) : '',
        episode: order.episode,
        hospital_expense: order.hospital_expense,
        hospital_coinsurance: order.hospital_coinsurance,
        surgical_event_date: order.surgical_event_date
    })
    const [subOrder, setSubOrder] = useState({
        provider: {},
        sub_order_generics: []
    })

    const handleResumeChange = (e) => {
        setResumeForm({
          ...resumeForm,
          [e.target.name]: e.target.value
        })
    }

    const handleSelectDocumentChange = (e) => {
        setSelectDocument(e.target.value)
    }

    const handleComment= e => {
        setComment(e.target.value)
    }

    useEffect(() => {   
      const fetchSubOrder = async () => {
        let res = await get(`operational_consulting_providers/${sub_order.slug}`)
        res
          .json()
          .then(data => {
            setSubOrder(data)
            setComments(data.comments)
            setFiles(data.documents)
            setOrderData(data.order)
            setHospital(data.order.hospital)
            setDoctor(data.order.doctor)
            })
          .catch(err => console.log(err))
    }
        fetchSubOrder()
    }, [])

    return <React.Fragment> 
        <OrderResume
            surgicalDate={surgicalDate}
            departureDate={departureDate}
            title={"Consulta Operativa"}
            module={'Consulta Operativa'}
            
            subOrder={subOrder}
            order={orderData}
            hospital={hospital}
            provider={subOrder.provider}
            doctor={doctor}
            
            legends={true}
            
            form={resumeForm}
            invisibles={[
                "wf_invoice",
                "evo_invoice",
                "gnp_invoice",
                "entry_probable_date",
                "claim",
                "departure_probable_date",
                "insured",
                "surgery_date"
            ]}
            handleChange={handleResumeChange}
        />
        
        <table className="table table-bordered">
            <thead>
                <tr>
                <th scope="col">Cantidad</th>
                <th scope="col">Codigo</th>
                <th scope="col">Descripción</th>
                <th scope="col">Marca</th>
                <th scope="col">Costo</th>
                <th scope="col">Utilizado Proveedor</th>
                <th scope="col">Utilizado Supervision Hospitalaria</th>
                <th scope="col">Autorizado Supervision Hospitalaria</th>
                <th scope="col">Observaciones</th>
                </tr>
            </thead>
            <tbody>
            { subOrder.sub_order_generics.map((generics) => (
                generics.sub_order_generals.map((generals, index) => (
                    <tr key={index}>
                        <th scope="col">{generals.quantity}</th>
                        <th scope="col">
                            { generals.provider_material.general_material.code }
                        </th>
                        <th scope="col">
                        { generals.provider_material.general_material.description }
                        </th>
                        <th scope="col">{generals.provider_material.brand}</th>
                        <th scope="col">{formatMoney(generals.cost)}</th>
                        <th scope="col">{generals.provider_used}</th>
                        <th>
                            {generals.used}
                        </th>
                        <th scope="col">
                            {generals.authorized}
                        </th>
                        <th scope="col">
                                {generals.obvservations}    
                        </th>
                    </tr>
                ))
            )) }
            
            </tbody>
        </table>

        <DocumentsContainer 
            selectDocument={selectDocument}
            handleSelectDocumentChange={handleSelectDocumentChange}
            uploadBtn={uploadBtn}
            loader={loader}
            setLoader={setLoader}
            setFiles={setFiles}
            setNewFiles={setNewFiles}
            files={files}
            username={username}
            options={[]}
            authenticity_token={null}
            only_view={true}
            module={module}
            provider={true}
        />
        <br/><br/>
        <Binnacle 
            comment={comment}
            handleComment={handleComment}
            comments={comments}
            username={username}
            module={null}
            only_view={true}
            provider={false}
        />
    </React.Fragment>
}

export default OperationalResultProvider