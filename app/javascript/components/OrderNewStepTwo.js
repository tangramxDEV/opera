import React, { useState, useEffect } from 'react'
import Binnacle from '../uicomponents/binnacle'
import DocumentsContainer from '../uicomponents/documents_container'
import { URL } from '../settings/configs'
import ICDModal from '../uicomponents/icd-modal'
import CPTModal from '../uicomponents/cpt-modal'
import AlertDanger from '../uicomponents/alert-danger'
import DatePicker, { registerLocale } from 'react-datepicker'
import { current_date_time } from '../functionalities/dates'
import Autosuggest from 'react-autosuggest'
import useHttp from '../hooks/useHttp'
import useSuggest from '../hooks/useSuggest'
import { useForm } from 'react-hook-form'
import ButtonLoader from '../uicomponents/button-loader'
import useValidations from '../hooks/useValidations'
import Notifications, { notify } from '../modules/Notifications'
import es from "date-fns/locale/es"
registerLocale("es", es)

const OrderNewStepTwo = ({authenticity_token, order, hospital, doctor, username, module}) => {
    const [sending, setSending] = useState(false)
    const { register, handleSubmit, errors } = useForm()
    const { get, post, put } = useHttp()
    const { getSuggestions, getSuggestionValue, renderSuggestion } = useSuggest('description')
    const { 
        validateIfCommentExist
    } = useValidations()
    //Datepicker Component
    const [startDate, setStartDate] = useState(new Date())
    const [minDate, setMinDate] = useState(new Date())
    // Comments Component
    const [comment, setComment] = useState('')
    // Documents Component
    const [selectDocument, setSelectDocument] = useState(null)
    const [uploadBtn, setUploadBtn] = useState('visible')
    const [loader, setLoader] = useState('hidden')
    const [files, setFiles] = useState([])
    const [newFiles, setNewFiles] = useState([])
    //
    const [subOrders, setSubOrders] = useState([])
    const [materials, setMaterials] = useState([])
    const [providers, setProviders] = useState([])
    const [icds, setIcds] = useState([])
    const [cpts, setCpts] = useState([])
    const [materialForm, setMaterialForm] = useState({
        'generic': '',
        'provider': '',
        'quantity': 1,
        'motive': '',
        'authorized': ''
    })
    const [form, setForm] = useState({
      entry_probable_date: '',
      departure_probable_date: '',
      claim: '',
      gnp_invoice: '',
      evo_invoice: '',
      wf_invoice: '',
      insured_name: '',
      insured_last_name: '',
      insured_mother_last_name: '',
      icd: '',
      cpt: '',
      second_cpt: '',
      finished: true
    })
    const [ICDForm, setICDForm] = useState({
        description: '',
        code: '',
    })
    const [CPTForm, setCPTForm] = useState({
        description: '',
        code: '',
    })
    const [alertMissingMaterials, setAlertMissingMaterials] = useState('hidden')
    // ICD Suggestions
    const [suggestions, setSuggestions] = useState([])
    const [value, setValue] = useState('')
    // CPT Suggestions
    const [cptSuggestions, setCptSuggestions] = useState([])
    const [cptValue, setCptValue] = useState('')
    // Second CPT Suggestions
    const [secondCptSuggestions, setSecondCptSuggestions] = useState([])
    const [secondCptValue, setSecondCptValue] = useState('')

    // ICD Suggest Engine
    const onChange = (event, { newValue }) => {
        let newForm = form
        newForm['icd'] = newValue
        setValue(newValue)
    }
    
    // Autosuggest will call this function every time you need to update suggestions.
    // You already implemented this logic above, so just use it.
    const onSuggestionsFetchRequested = ({ value }) => {
        setSuggestions(getSuggestions(value, icds))
    }
    
    // Autosuggest will call this function every time you need to clear suggestions.
    const onSuggestionsClearRequested = () => {
        setSuggestions([])
    }

    // CPT Suggest Engine
    const onCptChange = (event, { newValue }) => {
        let newForm = form
        newForm['cpt'] = newValue
        setCptValue(newValue)
    }

    const onCptSuggestionsFetchRequested = ({ value }) => {
        setCptSuggestions(getSuggestions(value, cpts))
    }

    const onCptSuggestionsClearRequested = () => {
        setCptSuggestions([])
    }

    // Second CPT Suggest Engine
    const onSecondCptChange = (event, { newValue }) => {
        let newForm = form
        newForm['second_cpt'] = newValue
        setSecondCptValue(newValue)
    }

    const onSecondCptSuggestionsFetchRequested = ({ value }) => {
        setSecondCptSuggestions(getSuggestions(value, cpts))
    }

    const onSecondCptSuggestionsClearRequested = () => {
        setSecondCptSuggestions([])
    }

    useEffect(() => {
        const fetchProviders = async () => {
            let res = await get(`/providers?selected=${order.selected_providers}`)
            res
                .json()
                .then(data => setProviders(data))
                .catch(err => console.log(err))
        }
        const fetchIcds = async () => {
            let res = await get(`/icds?suffering=${order.suffering}`)
            res
                .json()
                .then(data => setIcds(data))
                .catch(err => console.log(err))
        }
        const fetchCpts = async () => {
            let res = await get(`/cpts?suffering=${order.suffering}`)
            res
                .json()
                .then(data => setCpts(data))
                .catch(err => console.log(err))
        }

        const setMinDateToDatePicker = () => {
            if(order.order_type == "REPORTE HOSPITALARIO"){
                var date = new Date();
                date.setDate(date.getDate() - 5)
                setMinDate(date)
            }
            
        }
        fetchIcds()
        fetchCpts()
        fetchProviders()
        setMinDateToDatePicker()
    }, [])

    const handleMaterialChange = e => {
        setMaterialForm({
            ...materialForm,
            [e.target.name]: e.target.value
        })
    }
    const handleChange = e => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        })
    }
    const handleComment= e => {
        setComment(e.target.value)
    }
    const handleSelectDocumentChange = (e) => {
        setSelectDocument(e.target.value)
    }
    
    const addDays = (date, days = 1) => {
        let arrayDate = date.split("-") // [0] Year [1] Month [2] Day
        let result = new Date(arrayDate[0], arrayDate[1], arrayDate[2])
        result.setDate(result.getDate() + days)
        
        return `${result.getDate()}-${result.getMonth()}-${result.getFullYear()}`
    }
    const providerHasMaterials = (selectedMaterials) => {
        let providerMaterials = []
        let addedMaterials = []

        selectedMaterials.map((material) => {
            if(material.materials.length > 0){
                providerMaterials.push(material)
            }
        })
        providers.map((provider) => {
            if(providerMaterials.filter(e => e.provider == provider.id).length > 0){
                addedMaterials.push(true)
            }
        })

        if(addedMaterials.length == providers.length){
            return true
          } else {
            return false
          }
    }
    const handleSendOrder = async () => {
        setSending(true)
        let subOrdersMatrix = []
        order.selected_providers.map((provider) => {
            subOrdersMatrix.push({provider: provider, materials: []})
        })

        subOrdersMatrix.map((providerOrder) => {
            subOrders.map((subOrder) => {
                let providerId = subOrder.provider.slice(0, subOrder.provider.lastIndexOf(',')).toString()
                if(providerOrder.provider === providerId){
                    providerOrder.materials.push(subOrder)
                }
            })
        })

        form['departure_probable_date'] = addDays(`${startDate.getFullYear()}-${startDate.getMonth() + 1}-${startDate.getDate()}`)
        if(order.order_type == 'PROGRAMACIÓN DE CIRUGÍA'){
            form['entry_probable_date'] = `${startDate.getDate()}-${startDate.getMonth() + 1}-${startDate.getFullYear()}`
        } else {
            form['surgery_date'] = `${startDate.getDate()}-${startDate.getMonth() + 1}-${startDate.getFullYear()}`
        }
        let data = {
            order: {
              ...form,
              status: 'hospital_supervision'
            },
            order_comment: {
                comment,
                user: username,
                module: module,
                date: current_date_time()
            },
            sub_orders: [
                ...subOrdersMatrix
            ],
            
            authenticity_token
        }

        if(newFiles.length > 0){
            let documents = []
            newFiles.map((file) => {
                documents.push({
                    document_type: file.document_type ? file.document_type : '',
                    preview: file.preview ? file.preview : '',
                    stored_at: file.stored_at ? file.stored_at : '',
                    user: username,
                    module: module
                })
            })
            data['documents'] = documents
        }
        
        if(providerHasMaterials(subOrdersMatrix)){
            setAlertMissingMaterials('hidden')
            if (form.icd && form.cpt){
                if(validateIfCommentExist(comment)){
                    let res = await put(`orders/${order.slug}`, data)
                    res
                      .json()
                      .then(data => window.location.href = `/`)
                      .catch(err => err)
                } else {
                    setSending(false)
                    notify('Recuerda capturar información en la bitacora','danger') 
                }
            } else {
                setSending(false)
                notify('Recuerda capturar el ICD y CPT','danger') 
            }

        } else {
            setSending(false)
            setAlertMissingMaterials('visible')
        }
    }

    const handleAddMaterial = () => {
        let date = new Date()
        let currentTime = date.getTime()
        // let provider = materialForm.provider.slice(0, materialForm.provider.lastIndexOf(','))        
        
        let subOrder = {
            provider: materialForm.provider,
            quantity: materialForm.quantity,
            authorized: materialForm.authorized,
            motive: materialForm.motive,
            generic: materialForm.generic,
            created_at: currentTime
        }
        materialForm.created_at = currentTime
        
        setSubOrders([
            ...subOrders,
            subOrder
        ])
        setMaterials([
            ...materials,
            subOrder
        ])
    }

    const handleRemoveMaterial = (material) => {
        setSubOrders(subOrders.filter((currentSubOrder) => {
            return currentSubOrder !== material
        }))
        setMaterials(materials.filter((currentMaterial) => {
            return currentMaterial !== material
        }))
    }

    const handleICDChange = (e) => {
        setICDForm({
            ...ICDForm,
            [e.target.name]: e.target.value
        })
    } 

    const handleICDSubmit = async () => {
        let data = {
            icd: {
              description: `${ICDForm.code} - ${ICDForm.description}`
            },
            authenticity_token
          }

          let res = await post(`/icds`, data)
          res
            .json()
            .then(data => {
                setIcds([
                    ...icds,
                    data.icd
                ])
                setICDForm({
                  'description': '',
                  'code': ''
                })
            })
            .catch(err => err)
    }
    const handleCPTChange = (e) => {
        setCPTForm({
            ...CPTForm,
            [e.target.name]: e.target.value
        })
    } 

    const handleCPTSubmit = async () => {
        let data = {
            cpt: {
                description: `${CPTForm.code} - ${CPTForm.description}`
            },
            authenticity_token
          }
          try {
            let config = {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body: JSON.stringify(data)
            }
        
            await fetch(`${URL}/cpts`, config).then(async (res) => {
              if(res.status == '200'){
                let data = await res.json()
                setCpts([
                    ...cpts,
                    data.cpt
                ])
                setCPTForm({
                    'description': '',
                    'code': ''
                })
              }
            })
          } catch (error) {
            console.log(error)
          }
    }

    const validateOrderType = () => {
        if(order.order_type != 'REPORTE HOSPITALARIO'){
            return <React.Fragment>
                <h3 className="text-center">Insumos Ortopédicos</h3>
                <table className="table table-bordered">
                    <thead>
                        <tr>
                        <th scope="col">Insumo</th>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Proveedor</th>
                        <th scope="col">Autorizado</th>
                        <th scope="col">Motivo</th>
                        <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">
                        <div className="form-group">
                            <input 
                                type="text" 
                                className="form-control"
                                onChange={handleMaterialChange}
                                name="generic"
                                value={materialForm.generic}
                            />
                        </div>
                        </th>
                        <td>
                            <div className="form-group">
                                <input 
                                
                                    min="1"
                                    type="number" 
                                    className="form-control"
                                    onChange={handleMaterialChange}
                                    name="quantity"
                                    value={materialForm.quantity}
                                />
                            </div>
                        </td>
                        <td>
                            <div className="form-group">
                                <select 
                                    className="form-control" 
                                    id="exampleFormControlSelect1"
                                    onChange={handleMaterialChange}
                                    name="provider"
                                    value={materialForm.provider}
                                >
                                    <option disabled selected value=""> -- selecciona una opción -- </option>
                                    { providers.map((provider, index) => (
                                        <option
                                            key={index}
                                            multiple={true}
                                            value={[provider.id, provider.name]}
                                        >
                                            {provider.name}
                                        </option>    
                                    )) }
                                </select>
                            </div>
                        </td>
                        <td>
                            <div className="form-group">
                                <select 
                                    className="form-control"
                                    onChange={handleMaterialChange}
                                    name="authorized"
                                    value={materialForm.authorized}
                                >
                                    <option disabled selected value=""> -- selecciona una opción -- </option>
                                    <option>SI</option>
                                    <option>NO</option>
                                </select>
                            </div>
                        </td>
                        <td>
                            <div className="form-group">
                                <select 
                                    className="form-control"
                                    onChange={handleMaterialChange}
                                    name="motive"
                                    value={materialForm.motive}
                                >
                                    <option disabled selected value=""> -- selecciona una opción -- </option>
                                    <option></option>
                                    <option>Sujeto a Valoración</option>
                                    <option>Fuera de Cobertura</option>
                                </select>
                            </div>
                        </td>
                        <td>
                            <button 
                                type="button" 
                                className="btn btn-primary"
                                onClick={handleAddMaterial}
                            >
                                <i className="fa fa-plus"></i>
                            </button>
                        </td>
                        </tr>
                        { materials.map((material) => {
                            console.log(material)
                            return <tr>
                                <th scope="row">
                                    { material.generic.slice(material.generic.lastIndexOf(',') + 1) }
                                </th>
                                <th scope="row">
                                    { material.quantity }
                                </th>
                                <th scope="row">
                                    { material.provider.slice(material.provider.lastIndexOf(',') + 1) }
                                </th>
                                <th scope="row">
                                    { material.authorized }
                                </th>
                                <th scope="row">
                                    { material.motive }
                                </th>
                                <th scope="row">
                                <button 
                                    type="button" 
                                    className="btn btn-danger"
                                    onClick={() => handleRemoveMaterial(material)}
                                >
                                    <i className="fa fa-trash"></i>
                                </button>
                                </th>
                            </tr>
        }) }
                    </tbody>
                </table>
            </React.Fragment>
        }
    }
    const sufferingSelectors = () => {
        if(order.suffering === 'COLUMNA'){
            return <React.Fragment>
            <div className="form-inline">
                <label className="my-1 mr-2">Segmentación</label>
                <select className="custom-select my-12 mr-sm-4" id="inlineFormCustomSelectPref">
                    <option selected>Seleccionar</option>
                    <option value="C1">C1</option>
                    <option value="C2">C2</option>
                    <option value="C3">C3</option>
                    <option value="C4">C4</option>
                    <option value="C5">C5</option>
                    <option value="C6">C6</option>
                    <option value="C7">C7</option>
                    <option value="T1">T1</option>
                    <option value="T2">T2</option>
                    <option value="T3">T3</option>
                    <option value="T4">T4</option>
                    <option value="T5">T5</option>
                    <option value="T6">T6</option>
                    <option value="T7">T7</option>
                    <option value="T8">T8</option>
                    <option value="T9">T9</option>
                    <option value="T10">T10</option>
                    <option value="T11">T11</option>
                    <option value="T12">T12</option>
                    <option value="L1">L1</option>
                    <option value="L2">L2</option>
                    <option value="L3">L3</option>
                    <option value="L4">L4</option>
                    <option value="L5">L5</option>
                    <option value="S1">S1</option>
                </select>
                <label className="my-1 mr-2">A</label>
                <select className="custom-select my-12 mr-sm-4" id="inlineFormCustomSelectPref">
                    <option selected>Seleccionar</option>
                    <option value="C1">C1</option>
                    <option value="C2">C2</option>
                    <option value="C3">C3</option>
                    <option value="C4">C4</option>
                    <option value="C5">C5</option>
                    <option value="C6">C6</option>
                    <option value="C7">C7</option>
                    <option value="T1">T1</option>
                    <option value="T2">T2</option>
                    <option value="T3">T3</option>
                    <option value="T4">T4</option>
                    <option value="T5">T5</option>
                    <option value="T6">T6</option>
                    <option value="T7">T7</option>
                    <option value="T8">T8</option>
                    <option value="T9">T9</option>
                    <option value="T10">T10</option>
                    <option value="T11">T11</option>
                    <option value="T12">T12</option>
                    <option value="L1">L1</option>
                    <option value="L2">L2</option>
                    <option value="L3">L3</option>
                    <option value="L4">L4</option>
                    <option value="L5">L5</option>
                    <option value="S1">S1</option>
                </select>
            </div>
            
            </React.Fragment>
        }
    }

    const validateDatesByOrderType = () => {
        if(order.order_type == 'PROGRAMACIÓN DE CIRUGÍA'){
            return <div className="form-group">
            <label>Fecha probable de ingreso</label>
            <br/>
            <DatePicker
                locale="es"
                className="form-control"
                selected={startDate}
                onChange={date => setStartDate(date)}
                minDate={minDate}
                dateFormat="dd/MM/yyyy"
                popperPlacement="right-end"
            />
        </div>
        } else {
            return <div className="form-group">
            <label>Fecha de cirugia</label>
            <br/>
            <DatePicker
                locale="es"
                className="form-control"
                selected={startDate}
                onChange={date => setStartDate(date)}
                minDate={minDate}
                dateFormat="dd/MM/yyyy"
                popperPlacement="right-end"
            />
        </div>
        }
        
    }
    
    const validateSending = () => {
        if(sending){
            return <button className="btn btn-primary float-right">
                <ButtonLoader
                    visibility={true}
                />
            </button>
        } else {
            return <button 
                className="btn btn-primary float-right"
                onClick={handleSubmit(handleSendOrder)}
            >
            Enviar Solicitud
        </button>
        }
    }

    return <React.Fragment>
        <h3>Folio: { order.cpo }</h3>
        <div className="form-group">
            <label>Medico Tratante</label>
            <input disabled type="text" className="form-control" value={doctor.name}/>
        </div>
        <div className="form-group">
            <label>Hospital</label>
            <input disabled type="text" className="form-control" value={hospital.name}/>
        </div>
        
        { validateDatesByOrderType() }

        <div className="form-group">
            <label>Padecimiento</label>
            <input disabled type="text" className="form-control" value={order.suffering}/>
        </div>

        { sufferingSelectors() }
        
        <label>ICD</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <ICDModal
                    form={ICDForm}
                    handleChange={handleICDChange}
                    handleSubmit={handleICDSubmit}
                />
            </div>
            <Autosuggest 
                theme={{
                    input: 'form-control',
                    suggestionsList: 'list-group'
                }}
                suggestions={suggestions}
                onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                onSuggestionsClearRequested={onSuggestionsClearRequested}
                getSuggestionValue={getSuggestionValue}
                renderSuggestion={renderSuggestion}
                inputProps={{
                    placeholder: 'Ingresa el ICD',
                    value: value,
                    onChange: onChange
                }}
            />
      </div>

      <label>CPT</label>
      <div class="input-group">
          <div class="input-group-prepend">
              <CPTModal
                  form={CPTForm}
                  handleChange={handleCPTChange}
                  handleSubmit={handleCPTSubmit}
              />
          </div>
        <Autosuggest 
            theme={{
                input: 'form-control',
                suggestionsList: 'list-group'
            }}
            suggestions={cptSuggestions}
            onSuggestionsFetchRequested={onCptSuggestionsFetchRequested}
            onSuggestionsClearRequested={onCptSuggestionsClearRequested}
            getSuggestionValue={getSuggestionValue}
            renderSuggestion={renderSuggestion}
            inputProps={{
                placeholder: 'Ingresa el CPT',
                value: cptValue,
                onChange: onCptChange
            }}
        />
    </div>

    <label>CPT</label>
    <div class="input-group">
            <div class="input-group-prepend">
                <CPTModal
                    form={CPTForm}
                    handleChange={handleCPTChange}
                    handleSubmit={handleCPTSubmit}
                />
            </div>
            <Autosuggest 
            theme={{
                input: 'form-control',
                suggestionsList: 'list-group'
            }}
            suggestions={secondCptSuggestions}
            onSuggestionsFetchRequested={onSecondCptSuggestionsFetchRequested}
            onSuggestionsClearRequested={onSecondCptSuggestionsClearRequested}
            getSuggestionValue={getSuggestionValue}
            renderSuggestion={renderSuggestion}
            inputProps={{
                placeholder: 'Ingresa el CPT',
                value: secondCptValue,
                onChange: onSecondCptChange
            }}
        />
    </div>
        <hr/>

        <h4>Datos del tramite:</h4>
        <div className="form-group">
            <label>Reclamación</label>
            <input 
                type="text" 
                className={errors.claim ? "form-control is-invalid" : "form-control"}
                onChange={handleChange}
                value={form.claim}
                name="claim"
                maxLength="15"
                ref={register({required: true})}
            />
            {  errors.claim && <div class="invalid-feedback">
                La reclamación es necesaria.
                </div>
            }
        </div>
        <div className="form-group">
            <label>Folio GNP Hospitalización</label>
            <input 
                type="text" 
                className={errors.gnp_invoice ? "form-control is-invalid" : "form-control"}
                onChange={handleChange}
                value={form.gnp_invoice}
                name="gnp_invoice"
                maxLength="32"
                ref={register({required: true})}
            />
            {  errors.gnp_invoice && <div class="invalid-feedback">
                El Folio GNP es necesario.
                </div>
            }
        </div>
        <div className="form-group">
            <label>Folio EVO</label>
            <input 
                type="text" 
                className={"form-control"}
                onChange={handleChange}
                value={form.evo_invoice}
                name="evo_invoice"
                maxLength="32"
            />
        </div>
        <div className="form-group">
            <label>Folio WF</label>
            <input 
                type="text" 
                className={"form-control"}
                onChange={handleChange}
                value={form.wf_invoice}
                name="wf_invoice"
                maxLength="32"
                
            />
        </div>
        <div className="form-group">
            <label>Coaseguro(%)</label>
            <input 
                disabled 
                type="text" 
                className="form-control"
                value={order.coinsurance_percentage}
            />
        </div>
        <div className="form-group">
            <label>Coaseguro($)</label>
            <input 
                disabled 
                type="text" 
                className="form-control"
                value={order.coinsurance_money}
            />
        </div>
        <h4>Datos del asegurado:</h4>
        <div className="form-group">
            <label>Apellido Paterno</label>
            <input 
                type="text" 
                className={errors.insured_last_name ? "form-control is-invalid" : "form-control"}
                onChange={handleChange}
                value={form.insured_last_name}
                name="insured_last_name"
                ref={register({required: true})}
            />
            {  errors.insured_last_name && <div class="invalid-feedback">
            El apellido del asegurado es necesario.
            </div>
            }
        </div>
        <div className="form-group">
            <label>Apellido Materno</label>
            <input 
                type="text" 
                className={errors.insured_mother_last_name ? "form-control is-invalid" : "form-control"}
                onChange={handleChange}
                value={form.insured_mother_last_name}
                name="insured_mother_last_name"
                ref={register({required: true})}
            />
            {  errors.insured_mother_last_name && <div class="invalid-feedback">
                El apellido del asegurado es necesario.
                </div>
            }
        </div>
        <div className="form-group">
            <label>Nombre(s)</label>
            <input 
                type="text" 
                className={errors.insured_name ? "form-control is-invalid" : "form-control"}
                onChange={handleChange}
                value={form.insured_name}
                name="insured_name"
                ref={register({required: true})}
            />
            {  errors.insured_name && <div class="invalid-feedback">
                El nombre del asegurado es necesario.
                </div>
            }
        </div>
        <hr/>
        <br/>
        { validateOrderType() }
        <hr/>
        <br/><br/>
        <DocumentsContainer 
            selectDocument={selectDocument}
            handleSelectDocumentChange={handleSelectDocumentChange}
            uploadBtn={uploadBtn}
            loader={loader}
            setLoader={setLoader}
            setFiles={setFiles}
            newFiles={newFiles}
            setNewFiles={setNewFiles}
            files={files}
            username={username}
            options={[
                "FORMATO DE VALORACIÓN MÉDICA",
                "CARTA DE AUTORIZACIÓN"
            ]}
            authenticity_token={authenticity_token}
            module={module}
        />
        <br/><br/>
        <Binnacle 
            comment={comment}
            handleComment={handleComment}
            username={username}
            module={module}
        />

        <AlertDanger
            text="Verifica que cada proveedor contenga al menos 1 insumo"
            visibility={alertMissingMaterials}
        />

        { validateSending() }
        
    <Notifications/>
    </React.Fragment>
}

export default OrderNewStepTwo