import React, { 
    useState, 
    useEffect 
} from 'react'
import OrdersList from '../uicomponents/orders-list'
import DatePicker from 'react-datepicker'
import Autosuggest from 'react-autosuggest'
import useHttp from '../hooks/useHttp'
import useSuggest from '../hooks/useSuggest'
import Loader from '../uicomponents/loader'
import Alert from '../uicomponents/alert'

const ReportOrders = ({hospitals, doctors}) => {
    const { get } = useHttp()    
    const { getSuggestions, getSuggestionValue, renderSuggestion } = useSuggest('name')
    //Datepicker Component
    const [startDate, setStartDate] = useState(new Date())
    const [changeDate, setChangeDate] = useState(false)
    // Loader
    const [isLoading, setIsLoading] = useState(false)
    //
    const [orders, setOrders] = useState([])
    const [query, setQuery] = useState({
        doctor: '',
        hospital: '',
        insured: ''
    })
    // Doctor Suggestions
    const [doctorSuggestions, setDoctorSuggestions] = useState([])
    const [doctorValue, setDoctorValue] = useState('')
    // Hospital Suggestions
    const [hospitalSuggestions, setHospitalSuggestions] = useState([])
    const [hospitalValue, setHospitalValue] = useState('')
    
    const [manageError] = useState('Resultados no encontrados, validar información.')
    const [alertVisible, setAlertVisible] = useState(false)

    // Doctor Suggest Engine
    const onDoctorChange = (event, { newValue }) => {
        setQuery({
            ...query,
            'doctor': newValue
        })
        setDoctorValue(newValue)
    }

    const onChange = (e) => {
        setQuery({
            ...query,
            [e.target.name]: e.target.value
        })
    }
    const onDoctorSuggestionsFetchRequested = ({ value }) => {
        setDoctorSuggestions(getSuggestions(value, doctors))
    }

    const onDoctorSuggestionsClearRequested = () => {
        setDoctorSuggestions([])
    }
    
    // Hospital Suggest Engine
    const onHospitalChange = (event, { newValue }) => {
        setQuery({
            ...query,
            'hospital': newValue
        })
        setHospitalValue(newValue)
    }

    const onHospitalSuggestionsFetchRequested = ({ value }) => {
        setHospitalSuggestions(getSuggestions(value, hospitals))
    }

    const onHospitalSuggestionsClearRequested = () => {
        setHospitalSuggestions([])
    }

    const findInCollection = (collection, lost, atribute) => (
        collection.find(element => element[atribute] === lost)
    ) 

    const handleLaunchQuery = async (e) => {
        e.preventDefault()
        const hospital = findInCollection(hospitals, query.hospital, 'name')
        const doctor = findInCollection(doctors, query.doctor, 'name')
        
        if (doctor && hospital){
            setIsLoading(true)
            let res = await get(`/report_orders?doctor=${doctor.id}&hospital=${hospital.id}&surgery=${ changeDate ? startDate : ''}&type=REPORTE HOSPITALARIO&status=hospital_supervision&insured=${query.insured}`)
            res
                .json()
                .then(res => {
                    setAlertVisible(false)
                    setOrders(res)
                    setIsLoading(false)
                    if(res.length === 0){
                        setAlertVisible(true)
                    }
                })
                .catch(err => console.log(err))
        } else {
            setAlertVisible(true)
            setIsLoading(false)
        }
    }

    return <React.Fragment>
    <form onSubmit={handleLaunchQuery}>
        <div className="row">
            <div className="col s4">
                <div className="form-group">
                    <label>Hospital</label>
                    <Autosuggest 
                        theme={{
                            input: 'form-control',
                            suggestionsList: 'list-group'
                        }}
                        suggestions={hospitalSuggestions}
                        onSuggestionsFetchRequested={onHospitalSuggestionsFetchRequested}
                        onSuggestionsClearRequested={onHospitalSuggestionsClearRequested}
                        getSuggestionValue={getSuggestionValue}
                        renderSuggestion={renderSuggestion}
                        inputProps={{
                            placeholder: 'Hospital...',
                            value: hospitalValue,
                            onChange: onHospitalChange
                        }}
                    />
                </div>
            </div>
            <div className="col s4">
                <div className="form-group">
                    <label>Doctor</label>
                    <Autosuggest 
                        theme={{
                            input: 'form-control',
                            suggestionsList: 'list-group'
                        }}
                        suggestions={doctorSuggestions}
                        onSuggestionsFetchRequested={onDoctorSuggestionsFetchRequested}
                        onSuggestionsClearRequested={onDoctorSuggestionsClearRequested}
                        getSuggestionValue={getSuggestionValue}
                        renderSuggestion={renderSuggestion}
                        inputProps={{
                            placeholder: 'Doctor...',
                            value: doctorValue,
                            onChange: onDoctorChange
                        }}
                    />
                </div>
            </div>
            <div className="form-group">
                <label>Fecha de cirugia</label>
                <br/>
                <DatePicker
                    className="form-control"
                    dateFormat="dd/MM/yyyy"
                    selected={startDate}
                    onChange={date => {
                        setChangeDate(true)
                        setStartDate(date)
                    }}
                />
            </div>
            
            <div className="form-group">
                <label>Asegurado</label>
                <br/>
                <input
                    onChange={onChange}
                    name="insured"
                    value={query.insured}
                    className="form-control"
                    type="text"
                />
            </div>

            <div className="col s4 space-t">
                <button 
                    className="btn btn-primary"
                    type="submit"
                >
                    <i className="fas fa-search"></i>
                </button>
            </div>
        </div>

    </form>
    <Loader 
        visibility={isLoading}
    />
    <Alert
        manageable={false}
        visible={alertVisible}
        error={manageError}
    />
    <OrdersList 
        orders={orders}
        route={`/report_orders`}
    />
</React.Fragment>
}

export default ReportOrders