import React, {
    useEffect,
    useState
} from 'react'
import { URL } from '../settings/configs'
import formatMoney from '../extras/money_format'
import Binnacle from '../uicomponents/binnacle'
import DocumentsContainer from '../uicomponents/documents_container'

const DictumsOrder = ({order, authenticity_token, username, module}) => {
    const [deliveries, setDeliveries] = useState([])
    // Comments Component
    const [comment, setComment] = useState('')
    const [comments, setComments] = useState([])
    // Documents Component
    const [selectDocument, setSelectDocument] = useState(null)
    const [uploadBtn, setUploadBtn] = useState('visible')
    const [loader, setLoader] = useState('hidden')
    const [files, setFiles] = useState([])
    const [newFiles, setNewFiles] = useState([])
    //
    const [subOrders, setSubOrders] = useState([])    

    const handleSelectDocumentChange = (e) => {
        setSelectDocument(e.target.value)
    }
    const handleComment= e => {
        setComment(e.target.value)
    }

    const handleSendOrder = async () => {
        let today = new Date();
        let data = {
            order: {
              ...order,
              status: 'provider'
            },
            authenticity_token
        }

        if(comment){
          data['comment'] = {
              comment,
              user: username,
              module: module,
              date: current_date_time()
          }
        }

        if(newFiles[0]){
            data['document'] = {
                document_type: newFiles[0].document_type ? newFiles[0].document_type : '',
                preview: newFiles[0].preview ? newFiles[0].preview : '',
                stored_at: newFiles[0].stored_at ? newFiles[0].stored_at : '',
                user: username,
                module: module
            }
        }

        try {
            let config = {
              method: 'PUT',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              body: JSON.stringify(data)
            }
        
            await fetch(`${URL}/orders/${order.id}`, config).then(async (res) => {
              if(res.status == '200'){
                window.location.href = `/`
              }
            })
          } catch (error) {
            console.log(error)
          }
    }
    
    useEffect(() => {
        const fetchOrder = async () => {
          try {
              let res = await fetch(`${URL}/orders/${order.id}`)
              let data = await res.json()
              setDeliveries(data.deliveries)
              setComments(data.comments)
              setFiles(data.documents)
              console.log(data)
          } catch (error) {
              console.log(error)
          }
      }
      const fetchSubOrder = async () => {
        try {
            let res = await fetch(`${URL}/sub_orders/${order.id}`)
            let data = await res.json()
            setSubOrders(data)
            console.log(data)
        } catch (error) {
            console.log(error)
        }
      }
        fetchSubOrder()
        fetchOrder()
    }, [])

    return <React.Fragment>
    <br/><br/>
    { subOrders.map((subOrder) => (
        <React.Fragment>
            <h4>{subOrder.provider.name}</h4>
            { codedValidation(subOrder.coded) }
            <div class="form-row">
              <div class="col">
                <input 
                  type="text" 
                  className="form-control"
                  placeholder="Folio"
                  name="invoide"
                  value={invoiceForm.invoice}
                  onChange={(e) => {
                    setInvoiceForm(e.target.value)
                  }}
                />
              </div>
              <div class="col">
                <button 
                  className="btn btn-primary"
                  onClick={() => {handleSendInvoice(subOrder)}}
                >
                  <i class="fas fa-save"></i>
                </button>
              </div>
            </div>

            <table className="table table-bordered">
                <thead>
                    <tr>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Codigo</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Costo</th>
                    <th scope="col">Autorizado Supervision Hospitalaria</th>
                    <th scope="col">Observaciones</th>
                    </tr>
                </thead>
                <tbody>
                { subOrder.sub_order_generics.map((generics) => (
                    generics.sub_order_generals.map((generals) => (
                        <tr>
                            <th scope="col">{generals.quantity}</th>
                            <th scope="col">Codigo</th>
                            <th scope="col">Descripción</th>
                            <th scope="col">{generals.cost}</th>
                            <th scope="col">
                                {generals.authorized}
                            </th>
                            <th scope="col">
                                {generals.obvservations}
                            </th>
                        </tr>
                    ))
                )) }
                </tbody>
            </table>
        </React.Fragment>
    )) }
    <br/><br/>
    
        <DocumentsContainer 
            selectDocument={selectDocument}
            handleSelectDocumentChange={handleSelectDocumentChange}
            uploadBtn={uploadBtn}
            loader={loader}
            setLoader={setLoader}
            setFiles={setFiles}
            setNewFiles={setNewFiles}
            files={files}
            username={username}
            authenticity_token={authenticity_token}
            module={module}
        />
        <br/><br/>
        <Binnacle 
            comment={comment}
            handleComment={handleComment}
            comments={comments}
            username={username}
            module={module}
        />
        <button 
            onClick={handleSendOrder}
            className="btn btn-primary float-right"
        >
            Enviar a Proveedor
        </button>
    </React.Fragment>
}

export default DictumsOrder