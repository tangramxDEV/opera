import React, {
  useState,
  useEffect
} from 'react'
import { URL } from '../settings/configs'
import OrderResume from '../uicomponents/order-resume'
import DocumentsContainer from '../uicomponents/documents_container'
import Binnacle from '../uicomponents/binnacle'
import useSearch from '../hooks/useSearch'
import MaterialModal from '../uicomponents/material-modal'
import useHttp from '../hooks/useHttp'
import useFormatter from '../hooks/useFormatter'
import { current_date_time } from '../functionalities/dates'
import PDFHeader from '../../../public/images/pdf-header'
import jsPDF from 'jspdf'
import 'jspdf-autotable'
import Notifications, { notify } from '../modules/Notifications'
import useValidations from '../hooks/useValidations'
import useWordSeparator from '../hooks/useWordSeparator'

const ReportOrder = ({authenticity_token, order, provider, username, module}) => {
  const { formatMoney } = useFormatter()
  const { get, post, put } = useHttp()
  const { generateString } = useWordSeparator()
  const { 
    validateIfCommentExist,
    validateDocuments
  } = useValidations()
// Component
const [generalMaterials, setGeneralMaterials] = useState([])
const [quantity, setQuantity] = useState(1)
const [assortedMaterials, setAssortedMaterials] = useState([])
const [negotiableMatter, setNegotiableMatter] = useState('hospital_supervision')

// Order Resume
const [orderData, setOrderData] = useState('')
const [hospital, setHospital] = useState('')
const [doctor, setDoctor] = useState('')
const [resumeForm, setResumeForm] = useState({
  surgical_event_date: '',
})
// Documents Component
const [selectDocument, setSelectDocument] = useState(null)
const [uploadBtn, setUploadBtn] = useState('visible')
const [loader, setLoader] = useState('hidden')
const [files, setFiles] = useState([])
const [newFiles, setNewFiles] = useState([])
// Comments Component
const [comment, setComment] = useState('')
const [comments, setComments] = useState([])
// Hook Searcher
const { query, setQuery, filteredElements } = useSearch(generalMaterials, ['brand', 'description'])
const [generics, setGenerics] = useState([])
//Material Modal
const [materialForm, setMaterialForm] = useState({
  description: '',
  code: '',
  price: '',
  brand: ''
})
const [sended, setSended] = useState(false)
const [modal, setModal] = useState(false)
const toggle = () => setModal(!modal)

// Comments Component
const handleComment= e => {
  setComment(e.target.value)
}
// Documents Component
const handleSelectDocumentChange = (e) => {
  setSelectDocument(e.target.value)
}

const handleMaterialChange = (e) => {
  setMaterialForm({
    ...materialForm,
    [e.target.name]: e.target.value
  })
} 

const handleMaterialSubmit = async () => {
  console.log(materialForm)
  let data = {
    provider_material: {
      ...materialForm
    },
    authenticity_token
  }

  let res = await post(`provider_materials`, data)
  res
    .json()
    .then(data => {
      setNegotiableMatter('negotiable-matter')
      handleAddMaterial(data, true)
      setMaterialForm({
        description: '',
        code: '',
        price: '',
        brand: ''
      })
      toggle()
    })
    .catch(err => console.log(err))

}

const handleAddMaterial = (material, is_new = false) => {
  let date = new Date()
  let currentTime = date.getTime()
  setAssortedMaterials([
    ...assortedMaterials,
   {
     ...material,
     quantity: quantity,
     used: 'SI',
     is_new: is_new,
     added: currentTime
    }
  ])
}

const handleRemoveCloudMaterial = (material) => {
  handleDeleteMaterial(material)
  // calculateTotal()
}

const handleDeleteMaterial = async (material) => {
  
  let config = {
    method: 'DELETE',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({authenticity_token})
  }

  await fetch(`${URL}/sub_order_generals/${material.id}`, config)
  await fetchOrder()
}

useEffect(() => {
  const fetchMaterials = async () => {
      let res = await get(`provider_materials?provider_id=${provider.id}`)
      res 
          .json()
          .then(res => setGeneralMaterials(res))
          .catch(err => console.log(err))
  }

      fetchOrder()
      fetchMaterials()
  }, [])

  const fetchOrder = async () => {
    let res = await get(`orders/${order.slug}`)
    res 
      .json()
      .then(res => {
        setOrderData(res)
        setHospital(res.hospital)
        setDoctor(res.doctor)
        fetchSubOrders(res.slug, res.order_comments)
        })
      .catch(err => console.log(err))
  }

  const fetchSubOrders = async (orderId, order_comments) => {
    setComments(order_comments)
    let res = await get(`orders/${orderId}/sub_orders`)
    res 
      .json()
      .then(res => {
        let providerGenerics = []
        res.map((subOrder) => {
          if(subOrder.provider.id == provider.id){
            setComments(subOrder.comments)
            setFiles(subOrder.documents)
            if(subOrder.status != 'pending'){
              setSended(true)
            }
            providerGenerics.push(...subOrder.sub_order_generics)
          }
        })

        setGenerics(providerGenerics)
      })
      .catch(err => console.log(err))
  }

  const handleResumeChange = (e) => {
      setResumeForm({
        ...form,
        [e.target.name]: e.target.value
      })
  }

  const handleSendOrder = async (partial) => {
      let general_materials = []
      
      assortedMaterials.map((material) => {
        general_materials.push({ 
          material_id: material.id,
          quantity: material.quantity,
          cost: material.price,
          provider_used: material.used,
          is_new: material.is_new
        })
      })
  
      
      let data = {
        sub_order: {
          status: partial ? negotiableMatter : 'pending',
          coded: true
        },
        message: partial ? 'Solicitud Enviada a Supervision Hospitalaria' : 'Solicitud Guardada',
        materials: [
          ...general_materials
        ],
        order: {
          ...order,
        },
        authenticity_token
      }
      if(comment){
        data['comment'] = {
            comment,
            user: username,
            module: module,
            date: current_date_time()
        }
      }
      if(resumeForm.surgical_event_date){
        data['order'] = {
          surgical_event_date: resumeForm.surgical_event_date
        }
      }

      if(newFiles.length > 0){
        let documents = []
        newFiles.map((file) => {
            documents.push({
                document_type: file.document_type ? file.document_type : '',
                preview: file.preview ? file.preview : '',
                stored_at: file.stored_at ? file.stored_at : '',
                user: username,
            })
        })
        data['documents'] = documents
    }

    if (partial) {
      if(validateIfCommentExist(comment)){
        if(general_materials.length > 0 || calculateRegisteredGenerics() > 0){
          let validation = validateDocuments(files, ["HOJA DE REMISION SELLADA"])
    
          if(validation.errors){
            notify(validation.errors,'danger')
          } else {
            await sendRequest(data)
          }
        } else {
          notify('Selecciona al menos 1 insumo','danger')
        }
      } else {
        notify('Recuerda capturar información en la bitacora','danger')
      }
      
    } else {
      await sendRequest(data)
    }

  }

  const calculateRegisteredGenerics = () => {
    let counter = 0
    generics.map((generic) => {
      generic.sub_order_generals.map((general,index) => {
        counter = counter + 1
      })
    })

    return counter
  }
  const sendRequest = async (data) => {
    let res = await put(`report_orders/${order.slug}`, data)
    res
      .json()
      .then(data => window.location.href = `/`)
      .catch(err => err)
  } 

  const handleManageQuantity = (material, quantity) => {
    setAssortedMaterials(assortedMaterials.map((currentMaterial) => {
      if(material == currentMaterial){
        currentMaterial.quantity = quantity
        return currentMaterial
      } else {
        return currentMaterial
      }
    }))
  }

  const handleManageUsed = (material, used) => {
    setAssortedMaterials(assortedMaterials.map((currentMaterial) => {
      if(material == currentMaterial){
        currentMaterial.used = used
        return currentMaterial
      } else {
        return currentMaterial
      }
    }))
  }

  const handleRemoveMaterial = (material) => {
    setAssortedMaterials(assortedMaterials.filter((currentMaterial) => (
      currentMaterial !== material
    )))
  }

  const calculateTotal = () => {
    let total = 0
    generics.map((generic) => {
      generic.sub_order_generals.map((general) => {
        if(general.provider_used == 'SI'){
          total = total + (parseInt(general.quantity) * Number.parseFloat(general.cost) )
        }
      })
    })
    assortedMaterials.map((material) => {
      if(material.used == 'SI')
        total = total + (parseInt(material.quantity) * Number.parseFloat(material.price))
    })

    return formatMoney(total)
  }

  const handlePDFGenerator = () => { 
    let genericsList = []
    generics.map((generic) => {
      generic.sub_order_generals.map((general) => {
        genericsList.push([
          generateString(generic.generic, 1),
          generateString(general.provider_material.description),
          general.provider_material.brand,
          general.quantity
        ]) 
      })
    })

    var doc = new jsPDF()
    doc.setFontSize(12)
    let today = new Date();
    var img = new Image()
    img.src = PDFHeader
    doc.addImage(img, 'JPEG', 15, 5, 250, 20)


    doc.setFontType("bold");
    doc.setFontSize(10)
    doc.text(170,10, `Fecha de impresión`)
    doc.setFontType("normal");
    doc.text(180,15,`${today.getDate()}-${today.getMonth() + 1}-${today.getFullYear()}`)

    doc.setFontSize(16)
    doc.setFontType("bold");
    doc.text(140, 30, 'Visado - Proveedor')
    doc.setFontSize(10)
    doc.text(150,35, `Ticket: ${orderData.cpo}`)

    doc.setFontSize(18)
    doc.setFontType("bold");
    doc.text(20, 45, `${provider.name}`)

    doc.setFontSize(12)
    doc.setFontType("bold");
    doc.text(20,55, `Asegurado:`)
    doc.setFontType("normal");
    doc.text(45,55, `${orderData.insured}`)

    doc.setFontType("bold");
    doc.text(20,60, `Folio GNP:`)
    doc.setFontType("normal");
    doc.text(45,60, `${orderData.gnp_invoice}`)

    doc.setFontType("bold");
    doc.text(20,65, `Reclamación:`)
    doc.setFontType("normal");
    doc.text(55,65, `${orderData.claim}`)

    doc.setFontType("bold");
    doc.text(20,70, `Médico Tratante:`)
    doc.setFontType("normal");
    doc.text(55,70, `${orderData.doctor.name}`)

    doc.setFontType("bold");
    doc.text(20,75, `Hospital:`)
    doc.setFontType("normal");
    doc.text(45,75, `${orderData.hospital.name}`)
    
    doc.setFontType("bold");
    doc.text(20,80, `Padecimiento:`)
    doc.setFontType("normal");
    doc.text(55,80, `${orderData.suffering}`)

    doc.setFontType("bold");
    doc.text(20,85, `Procedimiento:`)
    doc.setFontType("normal");
    var splitProcedure = doc.splitTextToSize(`${orderData.procedure}`, 150)
    doc.text(55,85, splitProcedure)

    doc.setFillColor('ED7D32');
    doc.rect(20, 100, 180, 1, "F");

    doc.setFontType("bold");
    doc.setFontSize(16)
    doc.text(20,108, `Insumos Entregados`)

    doc.setFontType("normal");

    let wantedTableWidth = 120;
    let pageWidth = doc.internal.pageSize.width;
    let margin = (pageWidth - wantedTableWidth) / 2;

    doc.autoTable({
        theme: 'striped',
        startY: 115,
        headStyles: {
          fillColor: [0, 32, 96],
          cellWidth: 30,
          fontSize: 8,
        },
        styles: {
          halign: 'center',
          cellWidth: 'wrap',
          cellPadding: {top: 4, bottom: 4},
          fontSize: 8,
        },
        head: [['Genérico', 'Descripción', 'Marca', 'Cantidad']],
        margin: {left: margin, right: margin},
        body: [...genericsList]
      })

    doc.save('solicitud.pdf')
  }

  const showButtons = () => {
    if(!sended) {
      return <React.Fragment>
        <button 
            className="btn btn-primary float-right"
            onClick={() => {handleSendOrder(true)}}
        >
            Enviar a Supervision Hospitalaria
        </button>
  
        <button 
          onClick={() => {handleSendOrder(false)}}
          type="button" 
          className="btn btn-success float-right  space-r"
        >
          <i className="fas fa-save"></i>
        </button>
      </React.Fragment>
    } else {
      return <h5><span className="badge badge-primary float-right">Ya haz enviado esta solicitud</span></h5>
    }
  }
  return <React.Fragment>
      <OrderResume
          order={orderData}
          hospital={hospital}
          doctor={doctor}
          title={"Reporte Hospitalario"}
          invisibles={["deductible",
          "medical_fees",
          "coinsurance_percentage",
          "coinsurance_money",
          "departure_date",
          "departure_probable_date",
          "entry_probable_date",
          "surgical_event_date"
          ]}
          editable={[]}
          form={resumeForm}
          handleChange={handleResumeChange}
          provider={provider}
          // legends={true}
      />
  <br/>

    <div className="form-group">
      <input 
        type="text" 
        className="form-control"
        placeholder="BUSCAR INSUMO"
        value={query}
        onChange={(e) => {
            setQuery(e.target.value)
        }}
      />
    </div>

  <div style={{'maxHeight':'350px'}} className="pre-scrollable accordion">
  { filteredElements.map((material, index) => (
    <div className="card" key={index}>
      <div className="form-inline">
        <div className="form-group mx-sm-3 mb-2">
          <button className="btn btn-link" type="button">
              {material.general_material.description}
          </button>
        </div>
        <button 
            type="button" 
            className="btn btn-outline-primary float-right ml-auto"
            onClick={()=>{handleAddMaterial(material)}}
        >
            <i className="fas fa-plus"></i>
        </button>
      </div>
    </div>      
  )) }
</div>
<MaterialModal
  form={materialForm}
  handleChange={handleMaterialChange}
  onSubmit={handleMaterialSubmit}
  toggle={toggle}
  modal={modal}
/>
<br/><br/>
<React.Fragment>
  <button 
    className="btn btn-sm btn-primary space-r" 
    onClick={handlePDFGenerator}
  >
    <i className="fas fa-print"></i>
  </button>
  VISADO  
<br/><br/>
<table className="table table-bordered">
  <thead>
    <tr>
      <th scope="col">Descripcion</th>
      <th scope="col">Codigo</th>
      <th scope="col">Marca</th>
      <th scope="col">Costo</th>
      <th scope="col">Cantidad</th>
      <th scope="col">Utilizado Proveedor</th>
      <th scope="col">Utilizado Supervision Hospitalaria</th>
      <th scope="col">Autorizado Supervision Hospitalaria</th>
      <th slice="col">Acciones</th>
    </tr>
  </thead>
  <tbody>
    { generics.map((generic) => (
      generic.sub_order_generals.map((general,index) => (
        <tr>
        <td>{general.provider_material.description}</td>
        <td>{general.provider_material.code}</td>
        <td>{general.provider_material.brand}</td>
        <td>${formatMoney(general.cost)}</td>
        <td>{general.quantity}</td>
        <td>{general.provider_used}</td>
        <td>{general.used}</td>
        <td>{general.authorized}</td>
        <td>                  
        <center>
          <button 
            type="button" 
            className="btn btn-danger"
            onClick={() => handleRemoveCloudMaterial(general)}
            disabled={sended}
          >
            <i className="fa fa-trash"></i>
          </button>
        </center>
      </td>
      </tr>    
      ))
    )) }
    {assortedMaterials.map((material, index) => (
      <tr>
      <td>{material.description}</td>
      <td>{material.code}</td>
      <td>{material.brand}</td>
      <td>${formatMoney(material.price)}</td>
      <td>
        <input 
          className="form-control"
          type="number"
          placeholder="1"
          min="1"
          value={material.quantity}
          onChange={(e) => {
            handleManageQuantity(material, e.target.value)
          }}
        />
      </td>
      <td>
          <select 
            className="form-control"
            onChange={(e) => {
              handleManageUsed(material, e.target.value)
            }}
          >
            <option selected >SI</option>
            <option>NO</option>
          </select>
      </td>
      <td></td>
      <td></td>
      <td>                  
      <center>
        <button 
          type="button" 
          className="btn btn-danger"
          onClick={() => handleRemoveMaterial(material)}
        >
          <i className="fa fa-trash"></i>
        </button>
    </center>
    </td>
    </tr>
  ))}
  </tbody>
</table>

<h3>Total: ${calculateTotal()}</h3>

</React.Fragment>

  <DocumentsContainer 
      selectDocument={selectDocument}
      handleSelectDocumentChange={handleSelectDocumentChange}
      uploadBtn={uploadBtn}
      loader={loader}
      setLoader={setLoader}
      setFiles={setFiles}
      newFiles={newFiles}
      setNewFiles={setNewFiles}
      files={files}
      username={username}
      options={["HOJA DE REMISION SELLADA"]}
      authenticity_token={authenticity_token}
      module={module}
      provider={true}
  />
  <br/><br/>
  <Binnacle 
      comment={comment}
      handleComment={handleComment}
      comments={comments}
      username={username}
      module={module}
  />
  
  { showButtons() }
  <Notifications/>
  </React.Fragment>
}

export default ReportOrder