import React,{
    useState,
    useEffect
} from 'react'
// import { URL } from '../settings/configs'
import Binnacle from '../uicomponents/binnacle'
import DocumentsContainer from '../uicomponents/documents_container'
import OrderResume from '../uicomponents/order-resume'
import useHttp from '../hooks/useHttp'
import jsPDF from 'jspdf'
import 'jspdf-autotable';
import PDFHeader from '../../../public/images/pdf-header'
import useFormatter from '../hooks/useFormatter'
import useWordSeparator from '../hooks/useWordSeparator'

const QualityOrder = ({order, sub_order, provider, authenticity_token, username, module}) => {
    const { generateString } = useWordSeparator()
    const { formatMoney } = useFormatter()
    const { get } = useHttp()    
    const [orderData, setOrderData] = useState('')
    const [hospital, setHospital] = useState('')
    const [doctor, setDoctor] = useState('')

    // Resume Component
    const [resumeForm, setResumeForm] = useState({
        departure_date: order.departure_date ? order.departure_date.split(' ').join('T').substring(0, 16) : '',
    })
    // Comments Component
    const [comment, setComment] = useState('')
    const [comments, setComments] = useState([])
    // Documents Component
    const [selectDocument, setSelectDocument] = useState(null)
    const [uploadBtn, setUploadBtn] = useState('visible')
    const [loader, setLoader] = useState('hidden')
    const [files, setFiles] = useState([])
    const [newFiles, setNewFiles] = useState([])
    const [subOrder, setSubOrder] = useState({provider:{}, sub_order_generics:[]})
    let total = 0
    const [departureDate, setDepartureDate] = useState( 
        order.departure_date ?  new Date(order.departure_date) : undefined
    )
    const handleSelectDocumentChange = (e) => {
        setSelectDocument(e.target.value)
    }

    const handleResumeChange = (e) => {
        setResumeForm({
          ...resumeForm,
          [e.target.name]: e.target.value
        })
    }

    const handleComment= e => {
        setComment(e.target.value)
    }

    useEffect(() => {

        const fetchSubOrder = async () => {
            let res = await get(`qualities/${sub_order.slug}`)
                res
                .json()
                .then(data => {
                    setSubOrder(data)
                    setComments(data.comments)
                    setFiles([
                        ...data.documents,
                        ...data.order.order_documents,
                    ])
                    setOrderData(data.order)
                    setHospital(data.order.hospital)
                    setDoctor(data.order.doctor)
                })
                .catch(err => console.log(err))
        }
          fetchSubOrder()
      }, [])

      const codedValidation = (coded) => {
        if(coded) {
            return <div className="float-right">
                <div style={{'color':'green'}}>
                    <h4>
                        <i className="fas fa-lg fa-check-circle"></i>
                        Solicitud Codificada
                    </h4>
                </div>
            </div>
            } else {
            return <div className="float-right">
                <div style={{'color':'red'}}>
                    <h4>
                        <i className="fas fa-lg fa-exclamation-circle"></i>
                        Solicitud No Codificada
                    </h4>
                </div>
            </div>
        }
    }

    const calculateTotal = (general) => {
        if(general.provider_used == 'SI' && general.authorized != 'FUERA DE COBERTURA'){
          total = total + (Number.parseFloat(general.cost) * parseInt(general.quantity))
        }
    }

    const handlePDFGenerator = () => {

        let genericsList = []
        
        subOrder.sub_order_generics.map((generics) => {
            generics.sub_order_generals.map((generals, index) => {
                if(generals.authorized != "FUERA DE COBERTURA"){
                    genericsList.push([
                        generateString(generics.generic, 1),
                        generateString(generals.provider_material.description),
                        generals.provider_material.brand,
                        generals.quantity,
                        `$${formatMoney(generals.cost)}`
                    ]) 
                }
            })
        })
        
        let newTotal = total - (subOrder.negotiated_amount ? subOrder.negotiated_amount : 0)
        genericsList.push([generateString('Monto Negociado (Descuento)', 1), '', '', '', `$${formatMoney(subOrder.negotiated_amount ? subOrder.negotiated_amount : 0)}`])
        genericsList.push(['Total', '', '', '', `$${formatMoney(newTotal)}`])

        var doc = new jsPDF()
        doc.setFontSize(12)
        let today = new Date();
        var img = new Image()
        img.src = PDFHeader
        doc.addImage(img, 'JPEG', 15, 5, 250, 20)


        doc.setFontType("bold");
        doc.setFontSize(10)
        doc.text(170,10, `Fecha de impresión`)
        doc.setFontType("normal");
        doc.text(180,15,`${today.getDate()}-${today.getMonth() + 1}-${today.getFullYear()}`)

        doc.setFontSize(16)
        doc.setFontType("bold");
        doc.text(125, 30, 'Visado Calidad - Proveedor')
        doc.setFontSize(10)
        doc.text(150,35, `Ticket: ${orderData.cpo}`)

        doc.setFontSize(18)
        doc.setFontType("bold");
        doc.text(20, 45, `${provider.name}`)

        doc.setFontSize(12)
        doc.setFontType("bold");
        doc.text(20,55, `Asegurado:`)
        doc.setFontType("normal");
        doc.text(45,55, `${orderData.insured}`)

        doc.setFontType("bold");
        doc.text(20,60, `Folio GNP:`)
        doc.setFontType("normal");
        doc.text(45,60, `${orderData.gnp_invoice}`)

        doc.setFontType("bold");
        doc.text(20,65, `Reclamación:`)
        doc.setFontType("normal");
        doc.text(55,65, `${orderData.claim}`)

        doc.setFontType("bold");
        doc.text(20,70, `Médico Tratante:`)
        doc.setFontType("normal");
        doc.text(55,70, `${orderData.doctor.name}`)

        doc.setFontType("bold");
        doc.text(20,75, `Hospital:`)
        doc.setFontType("normal");
        doc.text(45,75, `${orderData.hospital.name}`)
        
        doc.setFontType("bold");
        doc.text(20,80, `Padecimiento:`)
        doc.setFontType("normal");
        doc.text(55,80, `${orderData.suffering}`)

        doc.setFontType("bold");
        doc.text(20,85, `Procedimiento:`)
        doc.setFontType("normal");
        var splitProcedure = doc.splitTextToSize(`${orderData.procedure}`, 150)
        doc.text(55,85, splitProcedure)

        doc.setFillColor('ED7D32');
        doc.rect(20, 100, 180, 1, "F");

        doc.setFontType("bold");
        doc.setFontSize(16)
        doc.text(20,108, `Insumos Entregados`)
    
        doc.setFontType("normal");
        let wantedTableWidth = 150;
        let pageWidth = doc.internal.pageSize.width;
        let margin = (pageWidth - wantedTableWidth) / 2;
        doc.autoTable({
            theme: 'striped',
            startY: 115,
            headStyles: {
              fillColor: [0, 32, 96],
              cellWidth: 'wrap',
              fontSize: 8
            },
            styles: {
              halign: 'center',
              cellWidth: 'wrap',
              cellPadding: {top: 4, bottom: 4},
              fontSize: 8,
            },
            head: [['Genérico', 'Descripción', 'Marca', 'Cantidad', 'Subtotal']],
            margin: {left: margin, right: margin},
            body: [...genericsList]
          })
        doc.save('solicitud.pdf')
    }

    return <React.Fragment>
        <OrderResume
            departureDate={departureDate}
            order={orderData}
            hospital={hospital}
            doctor={doctor}
            title={"Calidad"}
            invisibles={[
                "deductible",
                "medical_fees",
                "surgical_event_date",
                "order_type",
                "deductible",
                "medical_fees",
                "coinsurance_money",
                "icd",
                "cpt",
                "second_cpt",
                "episode",
                "hospital_expense",
                "surgery_date"
            ]}
            form={resumeForm}
            handleChange={handleResumeChange}

            provider={provider}
            subOrder={subOrder}
            legends={true}
        />
        <br/><br/>
        
        <React.Fragment>
            <h4>{subOrder.provider.name}</h4>
            
            <br/>
            <button 
                className="btn  btn-sm btn-primary space-r" 
                onClick={handlePDFGenerator}
            >
                <i className="fas fa-print"></i>
            </button>
            VISADO   
            <br/><br/>
            <button 
                className="btn  btn-sm btn-primary space-r" 
                onClick={(e) => {
                    e.preventDefault()
                    window.open(`${subOrder.letter}`)
                }}
            >
                <i className="fas fa-envelope-open-text"></i>
            </button>
            CARTA DE AUTORIZACION   
            <br/><br/>

            <p>
               FOLIO: {subOrder.invoice}
            </p>
            { codedValidation(subOrder.coded) }
            
            <div className="table-responsive">
                <table className="table table-bordered">
                    <thead>
                        <tr>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Codigo</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Marca</th>
                        <th scope="col">Utilizado Proveedor</th>
                        <th scope="col">Utilizado Supervision Hospitalaria</th>
                        <th scope="col">Autorizado Supervision Hospitalaria</th>
                        <th scope="col">Observaciones</th>
                        <th scope="col">Costo</th>
                        </tr>
                    </thead>
                    <tbody>
                    { subOrder.sub_order_generics.map((generics) => (
                        generics.sub_order_generals.map((generals, index) => {
                            { calculateTotal(generals) }
                            return <tr key={index}>
                                <th scope="col">{generals.quantity}</th>
                                <th scope="col">
                                { generals.provider_material.general_material.code }
                                </th>
                                <th scope="col">
                                { generals.provider_material.general_material.description }
                                </th>
                                <th scope="col">
                                { generals.provider_material.brand }
                                </th>
                                <th scope="col">{generals.provider_used}</th>
                                <th scope="col">
                                    {generals.used}
                                </th>
                                <th scope="col">
                                { generals.authorized }
                                </th>
                                <th scope="col">
                                { generals.obvservations }
                                </th>
                                <th scope="col">{formatMoney(generals.cost)}</th>
                            </tr>
                        })
                    )) }
                    <tr>
                        <th colSpan="8">Monto Negociado(Descuento)</th>
                        <td>${formatMoney(subOrder.negotiated_amount ? subOrder.negotiated_amount : 0)}</td>
                    </tr>
                    <tr>
                        <th colSpan="8">Total</th>
                        <td>${formatMoney(total - subOrder.negotiated_amount)}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </React.Fragment>
            
        <br/><br/>
        <DocumentsContainer 
            selectDocument={selectDocument}
            handleSelectDocumentChange={handleSelectDocumentChange}
            uploadBtn={uploadBtn}
            loader={loader}
            setLoader={setLoader}
            setFiles={setFiles}
            setNewFiles={setNewFiles}
            files={files}
            username={username}
            options={["CARTA DE AUTORIZACION"]}
            authenticity_token={authenticity_token}
            only_view={true}
            module={module}
            provider={true}
        />
        <br/><br/>
        <Binnacle 
            comment={comment}
            handleComment={handleComment}
            comments={comments}
            username={username}
            module={module}
            only_view={true}
        />
    </React.Fragment>
}

export default QualityOrder