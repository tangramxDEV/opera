import React, {
    useState,
    useEffect
} from 'react'
import useHttp from '../hooks/useHttp'

const UserHospitalCharge = ({user, authenticity_token}) => {
    const [hospitals, setHospitals] = useState([])
    const [hospitalSelecteds, setHospitalSelecteds] = useState([])
    const { get, post, destroy } = useHttp()    

    useEffect(() => {
        const fetHospitalCharges = async () => {
            let res = await get(`hospital_charges/${user.id}`)
            res
                .json()
                .then(data => {
                    setHospitalSelecteds(data.hospitals)
                    fetchHospitals(data.hospitals)
                })
                .catch(err => console.log(err))
        }

        const fetchHospitals = async (selectedHospitals) => {
            let res = await get('hospitals')
            res
            .json()
            .then(data => {
                setHospitals(arrayDiffByKey('id', selectedHospitals, data))
            })
            .catch(err => console.log(err))
        }

        fetHospitalCharges()
        

    }, [])

    const arrayDiffByKey = (key, ...arrays) => {
        return [].concat(...arrays.map( (arr, i) => {
            const others = arrays.slice(0);
            others.splice(i, 1);
            const unique = [...new Set([].concat(...others))];
            return arr.filter( x =>
                !unique.some(y => x[key] === y[key])
            );
        }));
    }

    const handleRemoveHospitalSelected = async (hospitalToRemove) => {
        await destroyHospitalCharge(hospitalToRemove)

        setHospitals([
            ...hospitals,
            hospitalToRemove
        ])
        setHospitalSelecteds(hospitalSelecteds.filter((hospital) => (
            hospital.id !== hospitalToRemove.id
        )))
    }

    const handleHospitalSelect = async (hospitalToRemove) => {
        await storeHospitalCharge(hospitalToRemove)

        setHospitals(hospitals.filter((hospital) => (
            hospital.id !== hospitalToRemove.id
        )))

        setHospitalSelecteds([
            ...hospitalSelecteds,
            hospitalToRemove
        ])
    }

    const destroyHospitalCharge = async (hospital) => {
        let data = {
            user: {
                user_id: user.id,
            },
            authenticity_token
        }

        let res = await destroy(`hospital_charges/${hospital.id}`, data)
        res
            .json()
            .then(data => console.log(data))
            .catch(err => err)
    }

    const storeHospitalCharge = async (hospital) => {
        let data = {
            hospital_charge: {
                user_id: user.id,
                hospital_id: hospital.id
            },
            authenticity_token
        }

        let res = await post(`hospital_charges`, data)
        res
          .json()
          .then(data => {
            console.log(data)
          })
          .catch(err => err)
    }

    return <React.Fragment>
    
        <div class="jumbotron">
            <h3 class="display-6">Asignación de hospitales</h3>
            <hr class="my-4" />
            <div className="row">
                <div className="col-6">
                    <h4 class="display-6">Existentes</h4>
                    <div class="list-group" style={{'maxHeight': '290px', 'overflow' : 'scroll'}}>
                        { hospitals.map((hospital) => <a 
                                onClick={() => handleHospitalSelect(hospital)}
                                class="list-group-item list-group-item-action"
                                style={{'cursor': 'pointer'}}
                            >
                                {hospital.name}
                            </a>
                        )}
                    </div>
                </div>
                <div className="col-6">
                    <h4 class="display-6">Seleccionados</h4>
                    <div class="list-group" style={{'maxHeight': '290px', 'overflow' : 'scroll'}}>
                        { hospitalSelecteds.map((hospital) => <a 
                                onClick={() => handleRemoveHospitalSelected(hospital)}
                                class="list-group-item list-group-item-action"
                                style={{'cursor': 'pointer'}}
                            >
                                {hospital.name}
                            </a>
                        )}
                    </div>
                </div>
            </div>
        </div>
        
    </React.Fragment>
}

export default UserHospitalCharge
