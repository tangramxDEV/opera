import React from 'react'

const useOrderCodedValidations = () => {
    let codeds = false

    const codedValidation = (coded) => {
        if(coded) {
            codeds = true
            return <div className="float-right">
                <div style={{'color':'green'}}>
                    <h5>
                        <i className="fas fa-check-circle"></i>
                         Solicitud Codificada
                    </h5>
                </div>
            </div>
            } else {
            return <div className="float-right">
                <div style={{'color':'red'}}>
                    <h5>
                        <i className="fas fa-exclamation-circle"></i>
                         Solicitud No Codificada
                    </h5>
                </div>
            </div>
        }
    }

    return { codedValidation, codeds }
}

export default useOrderCodedValidations