const useWordSeparator = () => {

    const generateString = (oldString, words = 3) => {
        const WORDS =  words
        let newString = ''
        let stringList = oldString.split(' ')

        stringList.map((word, index) => {
            if((index + 1) % WORDS == 0){
                newString = `${newString}${word}\n`
            } else {
                newString = `${newString}${word} `
            }
        })

        return newString
    }

    return { generateString }
}

export default useWordSeparator