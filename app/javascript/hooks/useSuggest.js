import React from 'react'

const useSuggest = (key) => {

    // Teach Autosuggest how to calculate suggestions for any given input value.
    const getSuggestions = (value, elements) => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;
      
        return inputLength === 0 ? [] : elements.filter(element =>
          element[`${key}`].toLowerCase().includes(inputValue)
        )
    }  

    // When suggestion is clicked, Autosuggest needs to populate the input
    // based on the clicked suggestion. Teach Autosuggest how to calculate the
    // input value for every given suggestion.
    const getSuggestionValue = suggestion => suggestion[`${key}`]

    // Use your imagination to render suggestions.
    const renderSuggestion = suggestion => (
        <li className="list-group-item">
            {suggestion[`${key}`]}
        </li>
    )

    return { getSuggestions, getSuggestionValue, renderSuggestion }
}

export default useSuggest