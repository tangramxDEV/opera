import { useState } from 'react'
import { DROP_BOX_ACCESS_TOKEN } from '../settings/configs'
import Dropbox from 'dropbox'

const useUploadFile = e => {
    const [ loader, setLoader ] = useState(true)
    const [files, setFiles] = useState([])
    const [newFiles, setNewFiles] = useState([])

    setLoader('visible')

    let date = Date.now();
    let dbx = new Dropbox.Dropbox({accessToken: DROP_BOX_ACCESS_TOKEN})
    let fileInput = document.getElementById('fileInput')
    let file = fileInput.files[0]

    dbx.filesUpload({path: `/opera/documents/${date}${file.name}`, contents: file})
    .then((res) => {
        let file = {
            stored_at: res.client_modified,
            preview: res.name,
            document_type: selectDocument
        }
        setLoader('hidden')
        setFiles([
            ...files,
            file
        ])
        setNewFiles([file])
        console.log(res);
      })
      .catch((error) => {
        setLoader('hidden')
        console.error(error);
      });

      return { loader, files, newFiles }
}

export default useUploadFile