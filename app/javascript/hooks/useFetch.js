import { 
    useState, 
    useEffect } from 'react'
import { URL } from '../settings/configs'

const useFetch = (url) => {
    const [data, setData] = useState([])
    const [error, setError] = useState(null)

    useEffect(() => {
        const fetchResource = async () => {
            try {
                let res = await fetch(`${URL}/${url}`)
                let data = await res.json()
                setData(data)
                // setLoading(false)
            } catch (error) {
                // setLoading(false)
                setError(error)
            }
        }
        fetchResource()
    }, [])

    return { data, error }
}

export default useFetch