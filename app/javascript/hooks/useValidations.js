
const useValidations = () => {

    const validateDocuments = (currents, expected) => {
        let expectedDocuments = expected.length
        let uploadedDocuments = 0
        currents.map((currentDoc) => {
            expected.map((expect) => {
                if(currentDoc.document_type === expect){
                    uploadedDocuments += 1
                }
            })
        })

        if(uploadedDocuments >= expectedDocuments){
            return true
        }
        return {
            errors: "Parece que no has ingresado los documentos necesarios para avanzar"
        }
    }

    const existAnyElementInArray = (collection, attribute) => {
        let pass = false
        collection.forEach(element => element[attribute] ? pass = true : null)

        return pass
    }
    const validateSubOrderCoded = subOrder => subOrder.coded

    const validateAnyOrderCoded = (subOrders) => {
        return existAnyElementInArray(subOrders, 'coded')
    }

    const validateLetterExists = (subOrder) => subOrder.letter ? true : false
    const validateAuthLetters = (subOrders) => {
        return existAnyElementInArray(subOrders, 'letter')
    }
    
    const validateIfSurgicalEvent = (order, surgical_event) => {
        return order.surgical_event_date !== null || surgical_event !== null
    }

    const validateHospitalSupervisionFields = (order, form, departureDate) => {
        let validator = []
        
        if(order.departure_date === null && !departureDate)
            validator.push('La fecha de alta es un campo obligatorio')
        if(order.episode === null && form.episode === null)
            validator.push('El episodio es un campo obligatorio')
        if(order.hospital_expense === null && form.hospital_expense === null)
            validator.push('El gasto hospitalario es un campo obligatorio')

        return { 
            validation: validator.length <= 0, 
            errors: validator 
        }
    }

    const validateInvoiceExists = (subOrder) => subOrder.invoice ? true : false

    const validateInvoiceByOrder = (subOrders) => {
        let invoices = 0

        subOrders.map((subOrder) => {
            if(subOrder.invoice !== null)
                invoices = invoices + 1
        })

        return invoices === subOrders.length
    }

    const validateIfCommentExist = (comment) => comment ? true : false

    return { 
        validateAuthLetters,
        validateAnyOrderCoded,
        validateSubOrderCoded,
        validateIfSurgicalEvent,
        validateHospitalSupervisionFields,
        validateInvoiceByOrder,
        validateInvoiceExists,
        validateLetterExists,
        validateIfCommentExist,
        validateDocuments
    }
}

export default useValidations