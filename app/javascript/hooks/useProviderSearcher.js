import { useState, useMemo } from 'react'

const useProviderSearcher = (providers) => {
    const [query, setQuery] = useState('')
    const [filteredProviders, setFilteredProviders] = useState(providers)
  
    useMemo(() => {
      const result = providers.filter(provider => {
      return `${provider.name}`
        .toLowerCase()
        .includes(query.toLowerCase())
    })
    setFilteredProviders(result)
    }, [providers, query])

    return { query, setQuery, filteredProviders }
}

export default useProviderSearcher