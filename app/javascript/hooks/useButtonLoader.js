import React,{
    useState
} from 'react'
import ButtonLoader from '../uicomponents/button-loader'

const useButtonLoader = () => {
    const [sending, setSending] = useState(false)


    const validateSending = (buttonText, handler) => {
        if(sending){
            return <button className="btn btn-primary float-right">
                <ButtonLoader
                    visibility={true}
                />
            </button>
        } else {
            return <button 
                onClick={() => {handler(true)}}
                className="btn btn-primary float-right"
            >
                { buttonText } 
            </button>
        }
    }

    return {sending, setSending, validateSending}
}

export default useButtonLoader