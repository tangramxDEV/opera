import { useState, useMemo } from 'react'

const useSearch = (elements, filteredBy = []) => {
    const [query, setQuery] = useState('')
    const [filteredElements, setFilteredElements] = useState(elements)
    useMemo(() => {
        const result = elements.filter(element => {
            var objectValues = ''
            filteredBy.map((filterValue) => {
                objectValues += element[filterValue]
            })
            return objectValues
            .toLowerCase()
            .includes(query.toLowerCase())
        })
        setFilteredElements(result)
    }, [elements, query])
    return { query, setQuery, filteredElements }
}

export default useSearch