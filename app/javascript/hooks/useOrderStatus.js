import React from 'react'

const useOrderStatus = () => {
    const translateStatus = (status) => {
        switch(status){
            case 'pending':
                return 'En bandeja del proveedor'
            case 'supplier_management':
                return 'En bandeja de negociacion | gestion de insumos'
            case 'coded':
                return 'Codificada por proveedor'
            case 'extemporaneous':
                return 'En bandeja de extemporaneos'
            case 'case_managment':
                return 'En bandeja de administracion de casos | Nuevo proveedor'
            case 'ok':
                return 'Nuevo proveedor negociado'
        }
    }

    const generateStatus = (status) => {
        // {translateStatus(status)}
        return <React.Fragment>
            <span className="badge badge-primary space-b">{translateStatus(status)}</span>
            <br/><br/>
        </React.Fragment>
    }

    const showStatus = (current_status) => {
        return generateStatus(current_status)
    }

    return { showStatus }
}

export default useOrderStatus