
const useLegends = () => {
    const buildLegend = (provider, order, subOrder, hospital) => {
        // The provider contain coinsurance?
        if(providerChargeCoinsurance(provider)){
            // The order contain coinsurance?
            if (existsCoinsurance(order)){
                let amount_coinsurance_hospital = (parseInt(order.hospital_expense) - parseInt(order.deductible)) * (parseInt(order.hospital_coinsurance) / 100 )
                let amout_coinsurance_medic = parseInt(order.medical_fees) * (parseInt(order.coinsurance_percentage) / 100 )
                let amount_coinsurance_provider = parseInt(order.coinsurance_money) - (amount_coinsurance_hospital + amout_coinsurance_medic)
                // If the subOrder its in hospital_supervision
                if(validateSubOrderStatus(subOrder)){
                    if(subOrder.coded || subOrder.coded == false){
                        return `Pendiente en valoración` 
                    }
                    if(order.hospital_expense == null || order.hospital_expense == 0){
                        return `Pendiente en Valoración - 
                        Existe un monto de coaseguro disponible, 
                        termine la codificación de insumos para efectuar el cálculo de asignación`
                    }
                    if(amount_coinsurance_provider >= 0 || amount_coinsurance_provider == null){
                        return `Existe un monto de coaseguro disponible, termine la codificación de insumos para efectuar el cálculo de asignación`
                    }
                    if(amount_coinsurance_provider < 0){
                        return 'Sin Cobro de coaseguro'
                    }
                } else {
                    // When the subOrder its in CDI or any other
                    if(validateIfHospitalChargeCoinsurance(hospital)){
                        if(subOrder.paid_coinsurance > 0){
                            return `Hospital Cobra Coaseguro, 
                            Recuperar con hospital $${subOrder.paid_coinsurance}`
                        } else {
                            if(order.money_available == null || order.money_available == 0){
                                return 'Sin Cobro de coaseguro'
                            } 
                        }
                    } else {
                        if(subOrder.paid_coinsurance == 0 || subOrder.paid_coinsurance == null ){
                            return `Sin Cobro de coaseguro`
                        } else {
                            return `Coaseguro $${subOrder.paid_coinsurance}`
                        }
                    }
                }
            } else {
                if(order.coinsurance_percentage == null || order.coinsurance_percentage == 0){
                    return 'Sin Cobro de coaseguro'
                } else {
                    return `Coaseguro %${order.coinsurance_percentage}`
                }
            }
            // return 'Pendiente en Valoración'
        } else {
            return 'Sin Cobro de coaseguro'
        }
    }

    // Validate that provider charge coinsurance
    const providerChargeCoinsurance = (provider) => {
        return provider.coinsurance
    }

    // Validate that the coinsurance greater than 0
    const existsCoinsurance = (order) => {
        if(parseInt(order.coinsurance_money) > 0){
            return true
        } else {
            return false
        }
    }

    // Validate What is the status of subOrder
    const validateSubOrderStatus = (subOrder, status = 'hospital_supervision') => {
        return (
            subOrder.status == 'extemporaneous' || 
            subOrder.status == 'negotiable-matter' || 
            subOrder.status == status || 
            subOrder.status == 'pending' ||
            subOrder.status == 'new_supplies' ||
            subOrder.status == 'case_managment'
        )
        // return subOrder.status == status
    }

    // Validate if the hospital charge the coinsurance
    const validateIfHospitalChargeCoinsurance = (hospital) => {
        return hospital.coinsurance
    }
    

    return { buildLegend }
}

export default useLegends