import { URL } from '../settings/configs'

const useHttp = () => {
    const formatUrl = (partialUrl) => {
        return `${URL}/${partialUrl}`
    }

    const buildConfig = (method, data) => {
        let config = {
            method: method,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        return config
    }

    const get = async (url) => {
        return await fetch(formatUrl(url))
    }

    const post = async (url, data) => {
        return await fetch(formatUrl(url), buildConfig('POST', data))
    }

    const put = async (url, data) => {
        return await fetch(formatUrl(url), buildConfig('PUT', data))
    }

    const destroy = async (url, data = {}) => {
        return await fetch(formatUrl(url), buildConfig('DELETE', data))
    }

    return { get, post, put, destroy }
}


export default useHttp