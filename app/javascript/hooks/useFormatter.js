const useFormatter = () => {

    const formatMoney = (quantity, minfractionDigits = 2, maxfractionDigits = 2) => {
        return new Intl.NumberFormat('en-US', { style: 'decimal', minimumFractionDigits: minfractionDigits, maximumFractionDigits: maxfractionDigits }).format(quantity)
    }

    return { formatMoney }
}

export default useFormatter