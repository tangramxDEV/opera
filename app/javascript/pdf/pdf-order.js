import React from 'react'
import {
    Page,
    Text,
    View,
    Document,
    StyleSheet,
    Image
} from '@react-pdf/renderer'
import PDFHeader from '../../../public/images/pdf-head'

export function PDFOrder(props){
    console.log("pdf props", props.data.cpo);
    return <Document>
    <Page>
    <Image src={PDFHeader} />
    <View>
        
        <Text>Ticket {props.data ? props.data.cpo : ":("}</Text>

    </View>
    </Page>
  </Document>

}

// export default PDFOrder