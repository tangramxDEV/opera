import React, {
    useState
} from 'react'
import styled from 'styled-components'
import ee from 'event-emitter'

const emitter = new ee()

export const notify = (msg, status) => {
    emitter.emit('notification', msg, status)
}

const Notifications = () => {
    const [top, setTop] = useState(-300)
    const [message, setMessage] = useState('')
    const [status, setStatus] = useState('')
    var timeOut = null
    
    const Container = styled.div`
        background-color: ${() => {
            if(status == 'success')
                return '#d4edda'
            if(status == 'danger')
                return '#f8d7da'
            }
        } ;    
        color: ${() => {
            if(status == 'success')
                return '#155724'
            if(status == 'danger')
                return '#721c24'
        }};  
        width: 80%;
        padding: .75rem 1.25rem;
        margin-bottom: 1rem;
        border: 1px solid transparent;
        border-radius: .25rem;
        position: fixed;
        top: ${props => props.top}px;
        z-index: 999;
        display: block;
        transition: top 0.5s ease;
    `
    
    emitter.on('notification', (msg, status) => {
        setMessage(msg)
        setStatus(status)
        onShow()
    })

    const onShow = () => {
        if(timeOut){
            clearTimeout(timeOut)
            setTop(-300)
            timeOut = setTimeout(() => {
                showNotification()
            }, 500)    
        } else {
            showNotification()
        }
    }

    const showNotification = () => {
        setTop(16)
        timeOut = setTimeout(() => {
            setTop(-300)
        }, 4000)
    }

    const isAnArray = (messages) => {
        if(Array.isArray(messages)){
            return <ul>
                {messages.map((message) => <li>{message}</li>)}
            </ul>
        } else {
            return messages
        }
    }
    return <React.Fragment>
        <Container top={top}>
            { isAnArray(message) }
        </Container>
    </React.Fragment>
}

export default Notifications