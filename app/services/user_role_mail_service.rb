# frozen_string_literal: true

class UserRoleMailService
  def self.get_mails(role)
    User.with_role(role.parameterize.underscore.to_sym)
  end
end
