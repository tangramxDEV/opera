# frozen_string_literal: true

class NotificationService
  def self.notify(order)
    if order.status == 'finished'
      order.sub_orders.map do |sub_order|
        # Email 6 - Notify to provider to new letter
        OrderMailer.with(sub_order: sub_order).notify_new_letter_to_provider.deliver_later
      end
    end
    # Email 3 Notify to provider any change
    order.sub_orders.map do |sub_order|
      OrderMailer.with(sub_order: sub_order).notify_change_to_provider.deliver_later
    end
  end
end
