# frozen_string_literal: true

class TotalSubOrderCalculatorService
  def self.calculate(sub_order)
    total = 0
    sub_order.sub_order_generics.each do |generic|
      generic.sub_order_generals.each do |general|
        total += general.cost.to_f if generic.authorized == 'SI'
      end
    end

    total.to_f
  end
end
